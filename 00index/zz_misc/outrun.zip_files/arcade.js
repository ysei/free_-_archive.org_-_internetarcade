$(document).ready(function() {
	function synthesizeEvent(type, keyCode) {
	        var e = new Event(type);
	        e.keyCode = keyCode;
	        document.dispatchEvent(e);
	}

	function fakeKeypress(keyCode) { 
	         synthesizeEvent('keydown', keyCode);
	         synthesizeEvent('keypress', keyCode); 
	         setTimeout(function() {synthesizeEvent('keyup', keyCode); }, 30); 
	}
 
	$.extend($.keyboard.keyaction, {
		coin:  function(base) { fakeKeypress(53); },
		p1:    function(base) { fakeKeypress(49); },
		p2:    function(base) { fakeKeypress(50); }
	});

	$('#keyboard').keyboard({ 
		layout : 'custom',
		restrictInput : false, // Prevent keys not in the displayed keyboard from being typed in 
		preventPaste : true,  // prevent ctrl-v and right click 
		autoAccept : true,
		usePreview : false,
		alwaysOpen : true,
		useCombos : false,
		customLayout: {
			'default' : [
				'{coin}',
				'{p1}',
				'{p2}'
			]
		},
		display: {
			'coin': 'INSERT COIN',
			'p1': 'PLAYER 1',
			'p2': 'PLAYER 2'
                }
	});
});
