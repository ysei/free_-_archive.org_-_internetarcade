
(function( $ ) {
  var TILE_WIDTH = 180; // NOTE: needs to stay in sync w/ archive.less

  var ua=navigator.userAgent;  
  // (global) variable memnonic: Archive JS
  window.AJS = {
    ios:(ua.indexOf('iPhone')>0  ||
         ua.indexOf('iPad')>0  ||
         ua.indexOf('iPod')>0)
  };


  // convenient, no?
  // Stateless function, global to all methods
  var log = function(){
    if (typeof(console)=='undefined')
      return;
    if (location.host.substr(0,4)!='www-')
      return;
    console.log(arguments);
  };

  AJS.x = function(){
    $('#descript').css('max-height',0);
    $('.welcome .row .col-md-2').hide();
    $('#moreless').hide();

    $('.welcome .row .col-md-3 img').css({'width':'', 'max-height':
                                          $('.welcome .row .col-md-7 h1').height()});


    $('.welcome, .navbar, #nav-tophat').css({'position':'fixed',width:'100%','z-index':101});
    $('.welcome').css({'z-index':100,'padding-top':30});

    setTimeout(function(){
      $($('.container-ia').get(2)).css({'padding-top':$('.welcome').height()});
    },1000);
               
  };


  
  
    
  

  AJS.nav_tophat_setup = function(){

    // TV items, search, and 9/11 grid pages have top position:fixed div elements.
    // So, they arent compatible yet w/ the "nav tophat".
    if ($('body').hasClass('nav-tophat-over')){
      var navHT = false;
      var styles = {'#nav-tophat':'',
                    '.navbar':''};
      $('#nav-tophat').on('show.bs.collapse', function(){
        // save out any current style="..." attr in each element we gonna adjust to open nav tophat
        // so that we can revert back when nav tophat closes
        if (navHT===false){
          for (var i in styles)
            styles[i] = $(i).attr('style');
        }

        var tmp = $('#nav-tophat').css({'position':'fixed','width':'100%','z-index':10000001,'top':0}).outerHeight();
        if (navHT===false)
          navHT = tmp;
        $('.navbar').css({'position':'fixed','width':'100%','z-index':10000000,'top':0,'padding-top':navHT});
      }).on('shown.bs.collapse', function(){
        // "sm" width size *can* make some nav tophat text re-flow to 2 lines and push height down...
        var max = Math.max.apply(Math, $('#nav-tophat .fivecolumns .posrel').map(function(k,v){return $(v).height();}));
        if (max > navHT){
          //console.log('max',navHT,max);
          navHT = max;
          $('.navbar').css({'padding-top':navHT});
        }
      }).on('hide.bs.collapse', function(){
        $('.navbar').css({'padding-top':0});
      }).on('hidden.bs.collapse', function(){
        // revert each element's style attr
        for (var i in styles)
          $(i).attr('style', (typeof(styles[i])=='undefined' ? '' : styles[i]));
      });
    }
                          
    
    $('.navbar a.navia-link').on('click', function(evt){
      // are we in "mobile nav / xs width" mode?
      var xs=($('.navbar .hidden-xs-span:first:visible').length ? false : true);
      var hat_open = $('#nav-tophat').hasClass('in');
      var navlink=this;
      
      var mt = $(evt.target).attr('data-top-kind');//user hit text
      if (!mt)
        mt = $(evt.currentTarget).attr('data-top-kind'); //user hit icon


      if (!(xs  &&  hat_open  &&  isnow!=mt)){
        // show the links for mediatype clicked
        $('.toprow').hide();
        $('.toprow.'+mt).show();
      }
      
      
      if (!hat_open)
        $('#nav-tophat').collapse('show'); // .. top hat is *not* open.  clicked, so open tophat

      var isnow = $('.navbar .dropdown-ia.hatted a').attr('data-top-kind');
      if (typeof(isnow)!='undefined')
        log('isnow '+isnow);
      log('shouldB '+mt);
      
      if (isnow == mt){
        // 2nd click on same mediatype dropdown -- close tophat!
        $('#nav-tophat').collapse('hide');
        return false;
      }

      if (xs  &&  hat_open  &&  isnow!=mt){
        // Mobile/xs nav and user has clicked on *another* nav/MT icon.
        // So we want to (animate) close the tophat, and when closed,
        // reopen (animate) to the right open tophat -- this is because
        // mobile/xs makes the tophat heights much more arbitrary
        // and it looks weird hard jumping to other open area at another height!
        $('#nav-tophat').one('hidden.bs.collapse', function(){
          setTimeout(function(){ $(navlink).click(); }, 100);
        });
        $('#nav-tophat').collapse('hide');
        return false;
      }
        
      

      $('.navbar .dropdown-ia').removeClass('hatted').has('.'+mt).addClass('hatted');
      $('#nav-tophat').on('hidden.bs.collapse', function(){ $('.navbar .dropdown-ia').removeClass('hatted'); });
      

      // any click makes tophat hide
      $(document).one('click.tophat.nixer', function(evt){

        // ... except clicking on wayback machine form input
        if ($(evt.target).attr('id')=='nav-wb-url')
          return;
        
        if ($('#nav-tophat').hasClass('in'))
          $('#nav-tophat').collapse('hide');
      });
      
      return false;
    });
  };


  AJS.footer_tophat_setup = function(){
    var $footer=$('#footer')
    if (!$footer.length)
      return;

    var totalHT=303;
    var closedHT=3;

    // ugh, shooot me...
    $(window).on('resize  orientationchange', function(evt){
      $.doTimeout('footer_tophat_setup()'+evt.type, 500, function(){
        if ($footer.hasClass('open')){
          // tophat opened, auto-resize to height
          $footer.css({bottom:(closedHT - totalHT + ($('#footer-tophat').outerHeight()))});          
        }
      });
    });
    
    
    $footer.find('.footer-tab, .footer-indicator').on('click', function(evt){
      $('#footer .footer-indicator').toggle();

      $footer.toggleClass('open');
      
      if ($footer.hasClass('open')){
        // tophat closed, open!
        $footer.css({bottom:(closedHT - totalHT + ($('#footer-tophat').outerHeight()))});
        evt.stopPropagation();
        evt.preventDefault();
        return false;
      }
      else{
        // tophat opened, close it!
        $footer.css({bottom:(closedHT - totalHT)});
      }
      return true;
    });
  };
  
  
  
  AJS.wrapdark_watcher = function(){ //no longer needed/used but has some handy techniques...
    if (!AJS.$sharedown){
      AJS.$sharedown = $('#sharedown');
      AJS.$wrapdark  = $('#wrapdark');
    }
    
    // every second, sigh, check/adjust for the wrapdark height
    if (!AJS.wrapdark_watcher_ptr)
      AJS.wrapdark_watcher_ptr = setInterval(AJS.wrapdark_watcher, 1000);
    
    if (!AJS.$sharedown.length  ||  !AJS.$wrapdark.length){
      clearInterval(AJS.wrapdark_watcher_ptr);
      return;
    }
    
    var wrapdarkHT = AJS.$sharedown.offset().top + AJS.$sharedown.find('.panel-heading').innerHeight();
    if (parseInt(AJS.$wrapdark.css('height'),10)!=wrapdarkHT){
      AJS.$wrapdark.css({height:wrapdarkHT}).show();
      log('       WRAPDARK-ED to '+wrapdarkHT);
    }
  };



  AJS.mute_click = function(){
    if (!((JSMESS  &&  JSMESS.sdl_pauseaudio)  ||  typeof(_SDL_PauseAudio)!='undefined')){
      alert('coming soon');
      return false;
    }

    $('#theatre-ia-buttons .iconochive-mute, #theatre-ia-buttons .iconochive-unmute').toggleClass('hidden');

    if ($.cookie('unmute')){
      // sounds is on.  mute it!
      (JSMESS.sdl_pauseaudio  &&  JSMESS.sdl_pauseaudio(1))  ||  (_SDL_PauseAudio  &&  _SDL_PauseAudio(1));
      $.cookie('unmute', null, { path:'/details'});
      $.cookie('unmute', null, { path:'/stream' });//!UI3
    }
    else{
      // sounds is off.  make it loud
      (JSMESS.sdl_pauseaudio  &&  JSMESS.sdl_pauseaudio(0))  ||  (_SDL_PauseAudio  &&  _SDL_PauseAudio(0));
      $.cookie('unmute', 1, { path:'/details', expires:30});
      $.cookie('unmute', 1, { path:'/stream' , expires:30});//!UI3
    }
    return false;
  };

  
  AJS.emulate_setup = function(map){
    // put into global space for JSMESS JS files to be able to read/access
    window.loader_game = map;
    window.loader_game.toString = function(){ return loader_game.path; };

    if ($.cookie('unmute'))
      $('#theatre-ia-buttons .iconochive-unmute').removeClass('hidden');
    else
      $('#theatre-ia-buttons .iconochive-mute').removeClass('hidden');
  };


  AJS.emulate = function(){
    // move the virtual keyboard thing and give it a "go away!" button
    $('.ui-keyboard').prepend('<button style="position:relative;top:-5px;right:-8px;" type="button" class="close" onclick="$(\'.ui-keyboard\').hide()" aria-hidden="true"><span class="iconochive-remove-circle"></span></button>').appendTo($('#emulate .posrel')).addClass('showing');
    
    $('#jsmessSS').fadeOut('slow');
    $('#canvasholder').css('visibility','visible');
    loader_game.started=true;
    JSMESSstart();

    return false;  
  };

  
  

  AJS.booksize = function(){

    if (!AJS.booksize_watching){
      AJS.booksize_watching = true;
      $(window).on('resize  orientationchange', function(evt){
        $.doTimeout('AJS.booksize'+evt.type, 250, AJS.booksize);
      });
    }
    
    var aspect = 4/3.0;
    var metadataHeight = 100; //metadata peekaboo min height!        

    var maxH=$(window).height() - $('#texty').offset().top - metadataHeight; 
    var maxW=$('#texty').width();
    log('max rect avail: '+maxW+'x'+maxH);
        
    var vidW, vidH;
    var tries=[960,840,720,600,480,360,240,180];
    for (var idx in tries){
      vidH = tries[idx];
      vidW = Math.round(vidH * aspect);
      log('rect size try fit: '+vidW+'x'+vidH);
      if ((vidW <= maxW  &&  vidH <= maxH)  ||  vidW<=320)
        break;
    }

    $("#texty iframe").css({width:vidW,height:vidH}); // resize bookreader

    setTimeout(function(){ if (AJS.booksize_controls_hider) AJS.booksize_controls_hider();},  500);
    setTimeout(function(){ if (AJS.booksize_controls_hider) AJS.booksize_controls_hider();},  750);
    setTimeout(function(){ if (AJS.booksize_controls_hider) AJS.booksize_controls_hider();}, 1000);
    setTimeout(function(){ if (AJS.booksize_controls_hider) AJS.booksize_controls_hider();}, 5000);
    setTimeout(function(){ if (AJS.booksize_controls_hider) AJS.booksize_controls_hider();},10000);
  };
  // hides the book controls, per david!
  AJS.booksize_controls_hider = function(){
    // this is new from raj w/ suffix "?ui=embed#mode/2up"
    var e=$($('iframe').get(0).contentDocument);
    if (e.find('#BRnav').length){
      e.find('#BRnav').hide();
      AJS.booksize_controls_hider=false;
      $("#texty iframe").css('visibility','visible');
      log('BOOK CONTROLS HIDDEN!');
    }
    e.find('body, #BRcontainer').css('background-color','transparent');
  };


  AJS.pause = function(id){
    if (!id) id='jw6';
    var jw=jwplayer(id);
    if (jw  &&  jw.config  &&  jw.getState()=='PLAYING')
      jw.pause();
  };
  

  AJS.colplay_setup = function(){

    $('#tabby-items .row .item-ia:not(.collection-ia) .item-img').parent().tooltip({'title':'<span class="iconochive-play"></span> Click image to Preview','placement':'bottom','html':true});
    
    
    $('#tabby-items .row .item-ia:not(.collection-ia)').on('click', function(evt){
      
      var identifier = $(evt.target).parents('a').attr('href').replace(/\/details\//,'');
      
      $.get('/details/'+identifier+'&colplay=1', function(data){
        
        $('body').addClass('colplay');
        
        setTimeout(function(){
          $('.container-ia:first').css({'padding-top': 60 + $('.welcome').height()});

          AJS.tiler();
        },1000);
        
        
        if ($('#colplay').length){
          AJS.pause();
        }
        else{
          $('.container-ia:first').append('<div id="colplay"/>');
        }
        
        
        try{
          $('#colplay').html('<div class="colplay-exit" data-toggle="tooltip" data-placement="left" title="hide player preview"><span class="iconochive-play"></span></div>'+data);
          
          $('#colplay .colplay-exit').tooltip({}).click(function(){
            AJS.pause();
            $('#colplay').css({'right':'-50%'});
            setTimeout(function(){
              $('#colplay').remove();
              $('body').removeClass('colplay');
              $('.container-ia:first').css({'padding-top':''});

              AJS.tiler();
            },600);
          });
          
          $('#colplay .sharee[data-toggle="tooltip"]').tooltip({});
          
          $('#colplay').css({'right':0});
        }
        catch (e){
          log('********* EXCEPTION ******');
          log(e);
        }
      });
      
      return false;
    });
  };


  AJS.add2list = function(lnk){

    var selector = $(lnk).attr('data-target');
    var href=$(lnk).attr('href');
    var selectorID = selector.replace(/#/,'');

    $.get(href+'&headless=1&titlelist=' + $(lnk).text(), function(htm){
      // dynamically add modal to page (if isnt there already)
      AJS.modal_go(lnk, {title:'Add To List',
                         headerClass:'modal-header-new-list',
                         ignore_lnk:true,
                         auto_remove:true,
                         body:htm});
    });
          
    return false;
  };

  AJS.newlist = function(lnk){
    alert('coming soon!');
    return false;
    
    var selector = $(lnk).attr('data-target');
    var href=$(lnk).attr('href');
    var selectorID = selector.replace(/#/,'');

    $.get(href+'&headless=1', function(htm){
      // dynamically add modal to page (if isnt there already)
      AJS.modal_go(lnk, {title:'Create New List',
                         headerClass:'modal-header-new-list',
                         ignore_lnk:true,
                         body:htm});
    });
          
    return false;
  };

  AJS.newlist_submit = function(){
    // Strings with SPACE characters will be changed to "camel case".
    // For example:
    //    "my LA roadtrip"
    // will become:
    //    "MyLaRoadtrip"
    // Letters, numbers, periods (.), dashes (-), or underscores (_) are ok, but
    // other characters will be removed.
    var title=$('#new-list-form input[name=title]').val();
    if (!title.length){
      alert('Please enter a title for your List');
      return false;
    }
 
    // turn the title into an acceptable identifier
    var cleaned=title;
    /*
    if (title.match(/ /)){
      cleaned = title.toLowerCase();
      cleaned = cleaned.replace(/([ ]+[a-z])/g, function(v){ return v.toUpperCase().trim(); });
      cleaned = cleaned.replace(/^([a-z])/g,    function(v){ return v.toUpperCase().trim(); });
    }
    */
    
    // remove any "bad" chars
    cleaned = cleaned.replace(/[^a-zA-Z0-9_\-\.]/g, '');
    // OK now trim any *leading* chars like:  - _ .
    cleaned = cleaned.replace(/^[_\-\.]+/, '');
    log(cleaned);

    if (!cleaned.length){
      alert('Please try another title that contains more alphanumeric characters');
      return false;
    }
      
    return true;
  };
  
  

  AJS.tiles_toggle = function(btn, cookie_range){

    var to = ($('body').hasClass('tiles') ? 'lists' : 'tiles');
    
    $('body').removeClass('lists tiles').addClass(to);
    AJS.tiler();

    $.cookie('view-'+cookie_range, to, { path:'/', expires:30});      
    
    return false;
  };
  

  // returns one of:
  //   #ikind-downloads           (collection pages)
  //   #ikind-mostrecent          (collection pages)
  //   #ikind-alphabetic          (collection pages)
  //   #ikind-contributors        (collection pages)
  //
  //   #ikind-collections-mostrecent  (account pages)
  //   #ikind-collections-alphabetic  (account pages)
  //   #ikind-uploads-mostrecent      (account pages)
  //   #ikind-uploads-alphabetic      (account pages)
  //   #ikind-loans-waiting           (account pages)
  //   #ikind-loans-on                (account pages)
  //   #ikind-loans-history           (account pages)
  //
  //   #ikind-search              (search)
  //
  //   #ikind-downloads           (top page)
  //   #ikind-mostrecent          (top page)
  //   #ikind-avgrating           (top page)
  AJS.selector = function(){
    var inTab = $('.welcome .tabby.in').text().trim();
    if (inTab=='FORUM'  ||  inTab=='POSTS')
      return false; // no tile tab is showing

    var selector=false;
    var collectionPage = (inTab=='ITEMS'  ||  inTab=='CONTRIBUTORS');
    var accountPage    = (inTab=='COLLECTIONS'  ||  inTab=='UPLOADS'  ||  inTab=='LOANS');
    
    if (collectionPage){
      if (inTab=='CONTRIBUTORS')
        selector = '#ikind-contributors';
      else
        selector = '#ikind-'+$('.ikind.stealth.in:first').text().toLowerCase().replace(/ /,'');
    }
    else if (accountPage){
      // we want to end up with a selector like:
      //    #ikind-collections-mostrecent
      var tmp = '#tabby-'+inTab.toLowerCase();
      selector = '#ikind-'+(inTab.toLowerCase())+'-'+$(tmp+' .ikind.stealth.in:first').text().toLowerCase().replace(/ /,'');
    }
    else if ($('body').hasClass('top')){
      selector = '#ikind-'+$('.ikind.stealth.in:first').text().toLowerCase().replace(/ /,'');
    }
    else{
      // searchPage -- we will fallback to generic search (which is single tabbed in reality)
      selector = '#ikind-search';
    }
    log('SELECTOR '+selector);
    
    return selector;
  };




  // function that finds alternate (non-AJS.tiler()) sets of item tiles,
  // that are laid out in strips/columns and will hide entire columns
  // if they fall off the right side, in terms of fitting to browser/page width
  AJS.tilebars = function(){
    // iterate over all row classes that have at least 1 .tilebars .results div in them...
    $('.row .tilebars').find('.results').parents('.row').each(function(k,row){
      var first=false;
      $(row).find('.results').each(function(key,val){
        $(val).show().css({visibility:'hidden'});
        if (!first)
          first=$(val).offset();
        if ($(val).offset().top != first.top)
          $(val).hide();
        else
          $(val).css({visibility:'visible'});
      });
      //if (first) $(row).find('.tilebars > h4').css({'padding-left':first.left+10});
    });
  };

  

  AJS.tiler = function(selector, noRecall){ //xxx-xxx make it so we tiles can skip columns have prior bottom exceeding more than 1/2 of height of current tile to place into puzzle...
    var tileMarginH = 13;
    var tileMarginW = 17;

    
    if (!$('body').hasClass('tiles'))
      return;

    if (!selector)
      selector = AJS.selector();
    if (selector===false)
      return;

    var $selector = $(selector).first();
    var selectorID = $selector.attr('id');
    var $parentRow = $selector.parents('.row');
    
    if (!AJS.tilerPREV){
      // first time tiling page!
      AJS.tilerPREV = {firstLeft:-26,
                       winW:0,
                       pad:0,
                       nPerRow:1};
      
      $selector.find('.item-ia:not(.hov):not(.collection-ia):not(.account-ia)').addClass('hov')
        .mouseover(function(e){ $(e.currentTarget).find('.item-parent').addClass   ('hoverin'); })
        .mouseout (function(e){ $(e.currentTarget).find('.item-parent').removeClass('hoverin'); });
    }

    if ($(window).width() != AJS.tilerPREV.winW){
      // First time, or page resize/orientation change.
      // We will effectively "cache" all this adjusting computations, so multiple calls to tiler()
      // can reuse if window width hasnt changed (for efficiency, eg: many images still slowly loading..)
      $parentRow.css({'padding-left':'', 'padding-right':''});
      var facetsWidth = $parentRow.find('.columns-facets').outerWidth()  ||  0;
      var availWidth = $parentRow.width() - facetsWidth;
      var nPerRow = Math.max(1, Math.floor((availWidth + tileMarginW) / (TILE_WIDTH + tileMarginW)));
      var extra   = Math.max(0,            availWidth - (nPerRow * (TILE_WIDTH + tileMarginW)) + tileMarginW);

      var dbug = 'tiling '+selectorID+', parentRowW: '+$parentRow.width()+', facetsWidth: '+facetsWidth+', availWidth: '+availWidth+', number per row: '+nPerRow+'. ';
      log(dbug + 'With gutter margin: '+tileMarginW+', had '+extra+' extra pixels');
      AJS.tilerPREV.pad = Math.round(extra/2);

      AJS.tilerPREV.winW = $(window).width();
      AJS.tilerPREV.nPerRow = nPerRow;
    }
    
    $parentRow.css({'padding-left':AJS.tilerPREV.pad, 'padding-right':AJS.tilerPREV.pad});
    


    var nImgZeroHeight=0;
    var maxtop=0;
    var tops={0:0};
    var lefts={0:AJS.tilerPREV.firstLeft};
    for (var i=1; i < AJS.tilerPREV.nPerRow; i++){
      lefts[i] = lefts[i-1] + tileMarginW + TILE_WIDTH;
      tops[i] = 0;
    }
    $selector.find('.item-ia').css({visibility:'hidden'}).each(function(idx,val){
      var col = (idx % AJS.tilerPREV.nPerRow);
      var $val = $(val);
      var $valHT = $val.innerHeight();
      //log('$valHT: '+$valHT);

      
      $val.css({visibility:'visible', top:tops[col], left:lefts[col]})


      // track how many tile images are without height (either not loaded or no CSS height style set if img not loading from DB)
      if (!($val.find('.item-img').height()))
        nImgZeroHeight++;

      
      // setup for next tile...
      tops[col] += ($valHT + tileMarginH);

      maxtop = Math.max(maxtop, tops[col]);
    });

    // we have to manually set the height of the "selector"
    log('maxtop: '+maxtop);
    $selector.find('.results').css({height:maxtop});

    
    if ((nImgZeroHeight)  &&  !noRecall){
      log(nImgZeroHeight+' img (still w/o height); recall tiler('+selector+')...');
      // recall ourselves, but not more than once every 1.5 seconds!
      $.doTimeout('tiles-img-load', 1500, function(){
        AJS.tiler(selector, 0);
      });
    }
  };


  AJS.ikind = function(lnk,id){
    log('ikind('+id+')');
    $('#'+id).parent().find('div.ikind.in').fadeOut();
    $('#'+id).removeClass('hidden').addClass('in').fadeIn();
    $('#'+id).parents('.tabby-data').find('a.ikind').removeClass('in');

    $(lnk).addClass('in');

    AJS.tiler('#'+id);

    // now select the corresponding element in the shadowing select for mobile
    $sel = $('#'+id).parents('.tabby-data').find('select.ikind-mobile');
    var $new = $sel.find('option:contains('+($(lnk).text())+')');
    var $now = $sel.find('option:selected');
    if ($new.text() != $now.text()){
      log('changing ikind mobile now to '+$new.text());
      AJS.ikind_mobile_change_ignore_next = true;
      $new.attr('selected','selected');
      //$now.removeAttr('selected');
    }
    
    return false;
  };

  AJS.ikind_mobile_change = function(elm){
    if (AJS.ikind_mobile_change_ignore_next){
      AJS.ikind_mobile_change_ignore_next = false;
      return;
    }

    var $selopt = $(elm).find('option:selected');
    log("ikind mobile changed to: "+$selopt.text());
    var $ikind = $(elm).parents('.tabby-data').find('a.ikind:contains('+($selopt.text())+')');
    if ($ikind.length){
      $ikind.click();
    }
    else {
      // NO tabby!  eg: top page or search page
      var tmp = $('body').find('a.ikind:contains('+($selopt.text())+')').attr('href');//xxx-xxx
      if (tmp)
        location.href = tmp;
    }
  };
  

  
  AJS.tabby = function(lnk,id){
    log('AJS.tabby('+id+')');
    $('.tabby-data.in').removeClass('in').fadeOut();
    $('#'+id).removeClass('hidden').addClass('in').fadeIn();
    $('.tabby').removeClass('in');
    $(lnk).parent().addClass('in');

    var tmp=id.replace(/tabby\-/,'');
    if (tmp=='collections'  ||  tmp=='uploads'  ||  tmp=='items'  ||  tmp=='contributors'  ||  tmp=='loans')
      AJS.tiler();
    
    return false;
  };



  AJS.list_remove_item = function(xer){
    if (!xer){
      if (!$('.item-ia .xer').length){
        // make all items editable
        $('.item-ia').addClass('removable').append('<div onclick="AJS.list_remove_item(this)" class="xer" alt="remove this item from list" title="remove this item from list"></div>');
      }
      else{
        // cancel editability
        $('.item-ia').removeClass('removable');
        $('.item-ia .xer').remove();
      }
    }
    else{
      // single item is slated for utter destruction from this list...
      var $item = $(xer).parents('.item-ia');
      var idX = $item.attr('data-id');
      var url = location.href.replace(/#.*$/,'') + '&remove_item='+idX;
      log('GET: '+url);
      $.get(url, function(htm){
        log('GOT: '+url);
        log(htm);
        $item.remove();
        AJS.tiler();
      });
    }
    return false;
  };


  AJS.head_img_dragdrop_setup = function(identifier){
    if (AJS.head_img_dragdrop_setup_done)
      return;
    log('head_img_dragdrop_setup');
    AJS.head_img_dragdrop_setup_done = true;


    $('#file-dropper-wrap').bind('mouseenter', function(evt){
      log('enter');
      AJS.head_img_replaceable(identifier);
      $('#file-dropper').show();
    }).bind('mouseleave', function(evt){
      log('ouddie');
      if (!AJS.file_picked)
        $('#file-dropper').hide();
    });
    
    $('body').bind('dragover', function(evt) {
      //enable file dropping
      log('dragover');
      evt.stopPropagation();
      evt.preventDefault();

      AJS.head_img_replaceable(identifier);
      $('#file-dropper').addClass('drag_over').show();
      return false;
    });
    
    $('body').bind('dragleave', function(evt) {
      log('dragleave');
      
      // are we still over a file-dropper div/img?
      var over = ($('#file-dropper-wrap').is(':hover') ||
                  $('#file-dropper'     ).is(':hover') ||
                  $('#file-dropper-img' ).is(':hover'));
      if (!over){
        var x = evt.pageX || evt.originalEvent.pageX;
        var y = evt.pageY || evt.originalEvent.pageY;
        var e = $('#file-dropper-wrap');
        var left = e.offset().left;
        var top  = e.offset().top;
        if (x >= left  &&  x <= left + e.outerWidth()  &&
            y >= top   &&  y <= top  + e.outerHeight()){
          // still over the file drop target image area!
          over=true;
        }
        if (!over){
          e = $('#file-dropper');
          left = e.offset().left;
          top  = e.offset().top;
          if (x >= left  &&  x <= left + e.outerWidth()  &&
              y >= top   &&  y <= top  + e.outerHeight()){
            // still over the file drop target image area!
            over=true;
          }
        }
      }

      if (!over)
        $('#file-dropper').removeClass('drag_over').hide();
    });
                
  };

  AJS.head_img_replaceable = function(identifier){
    // helpfuls:
    //  https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest
    //  http://www.sitepoint.com/html5-ajax-file-upload/
    if ( $('#file-dropper').html().trim() != '' )
      return;

    var form_init = function(){
      $('#file-dropper').show().html('<div>\n\
        <button id="file-cancel" type="button" class="close" data-dismiss="alert" aria-hidden="true" title="cancel file upload" alt="cancel file upload">&times;</button>\n\
        <b><span style="font-size:40px; color:#aaa" class="iconochive-plus-circle"></span></b><br/>\n\
          <b>Drag & Drop an image file here or</b>\n\
        <button type="button" class="btn btn-info btn-xs" onclick="$(\'#file-selector\').click();">Pick image to upload</button>\n\
        <form method="POST" action="/services/post-file.php?submit=1&identifier='+identifier+'" enctype="multipart/form-data" id="poster">\n\
          <div class="hidden">\n\
            <input id="file-selector" name="file" type="file" accept="image/*"/>\n\
          </div>\n\
          <input type="hidden" name="identifier" value="'+identifier+'"/>\n\
          <input id="file-submit" type="submit" name="submit" value="SUBMIT" class="btn btn-success"/>\n\
          <div id="file-uploading">Uploading your file..</div>\n\
        </form>\n\
      </div>');
    };
    form_init();


    $('#file-cancel').bind('click',function(evt){
      AJS.cancelFile();
      
      evt.stopPropagation();
      evt.preventDefault();
      return false;
    });
                           

    
    $.event.props.push('dataTransfer'); //if not set, dataTransfer is not sent


    var success = function(){
      log('success!');
      //AJS.cancelFile();
    };
    
    
    // upload an image (typically a collection/list header or account/person profile)  via S3
    // returns '' on success; else string w/ error/fail reason
    var uploadFile = function(){
      var fail=false;
      
      if (typeof(XMLHttpRequest)=='undefined')
        fail = 'browser appears to not have HTML5 functionality';
      
      if (!fail)
        fail = AJS.badFile();
      
      
      if (!fail){
        log(AJS.file2post);
        var xhr = new XMLHttpRequest();
        if (!xhr.upload)
          fail = 'browser submit setup failed';
      }
      
      if (fail)
        return fail;

      
      
      // start upload
      log("post");
      //if (navigator.userAgent.indexOf('Safari') > 0)  xhr.timeout = 10 * 60 * 1000; // 10 minutes
      xhr.open("POST",
               location.protocol +'//'+ location.host +"/services/post-file.php?submit=1&identifier="+identifier+"&fname="+encodeURIComponent(AJS.file2post.name),
               false); // block synchronously on this post!
      //if (!options.sendBoundary) xxx-xxx
      xhr.setRequestHeader("Content-Type", "multipart/form-data; charset=UTF-8");
      xhr.send(AJS.file2post);

      var reply=xhr.responseText;
      var mat=reply.match(/SUCCESS \(task_id=(\d+)\)/);
      if (mat  &&  mat.length){
        var task_id=mat[1];
        log('STALKING TASK_ID: '+task_id);

        $('#file-dropper').html('waiting for updates..');
        var stalker_ptr = false;
        var stalker = function(){
          log('STALKING...');
          $.ajax({type: "GET",
                  url: '/catalog_status.php?where=task_id='+task_id+'&rand='+Math.random(),
                  dataType: "xml",
                  success: function(xml) {
                    log(xml);
                    var x={green:parseInt($(xml).find('wait_admin0').text(),10) || 0,
                           blue :parseInt($(xml).find('wait_admin1').text(),10) || 0,
                           red  :parseInt($(xml).find('wait_admin2').text(),10) || 0};
                    log(x);
                    var nWait = x.green + x.blue + x.red;
                    $('#file-dropper').html('waiting for '+nWait+' tasks to run');
                    if (!nWait){
                      log('task(s) done!');
                      clearInterval(stalker_ptr);
                      $('#file-dropper').html('reloading page with your image');
                      location.href=location.href; // ready! reload the page!
                    }
                    else if (x.red){
                      $('#file-dropper').html('<div class="alert alert-danger">status task failure -- an admin will need to resolve</div>');
                      clearInterval(stalker_ptr);
                    }
                  }
                 });
        };

        // every 2 seconds, check for status
        stalker_ptr = setInterval(stalker, 2000);
        
      }
      else{
        log(reply);
      }

      log("post done");
      return '';
    };


    
    
    $('#file-selector').bind('click',function(evt){
      AJS.file_picked='selected';
    }).bind('change', function(evt){
      log('file dropdown selected!');
      $('#file-submit, #file-cancel').show();
      log(evt);
      if (evt.target  &&  evt.target.files  &&  evt.target.files.length){
        AJS.file2post = evt.target.files[0];
        AJS.previewFile();
      }
    });
                                             

    $('#file-dropper').bind('drop', function(evt) {
      // we've been dropped a file (from a drag-and-drop)!
      evt.stopPropagation();
      evt.preventDefault();
      
      log(evt.dataTransfer.files);
      $('#file-dropper').removeClass('drag_over');

      $('#file-submit, #file-cancel').show();
      
       
      if (evt.dataTransfer.files.length){
        AJS.file_picked='dropped';
        AJS.file2post = evt.dataTransfer.files[0];
        
        AJS.previewFile();
      }
    });


    $('#poster').bind('focusin',function(evt){
      log(evt.type);
      $('#file-uploading').show();
    }).bind('submit', function(evt){
      log('submit!');
      $('#file-uploading').show();

      // First try the schmancy HTML5 submit via XMLHttpRequest and FileReader.
      // If we fail, we'll fallback to form submit normal action.
      var fail = uploadFile();
      if (fail===''){
        // SUCCESS!  we are done!
        success();
        evt.stopPropagation();
        evt.preventDefault();
        return false;
      }

      if (AJS.file_picked=='dropped'){
        // we had client drag-n-drop the file -- we can't post it!
        // epic fail...
        AJS.cancelFile();
        alert('Failure: '+fail);
        evt.stopPropagation();
        evt.preventDefault();
        return false;
      }

      // OK fallback to normal (selected) file submit (and full page reload)!
      return true;
    });
                      
    
    
    // now center file-dropper over current image
    var w1=$('#file-dropper-img').outerWidth();
    var h1=$('#file-dropper-img').outerHeight();
    var w2=$('#file-dropper').outerWidth();
    var h2=$('#file-dropper').outerHeight();
    $('#file-dropper').css({left:Math.round((w1-w2)/2),
                            top :Math.round((h1-h2)/2)});
    
    if (AJS.ios){
      $('#file-dropper > form > div.hidden').removeClass('hidden');
      $('#file-dropper > button').addClass('hidden');
    }
  };


  
  // client-side preview the image directly in the browser!  xxx-xxx catch exceptions, etc.
  AJS.previewFile = function(){
    $('#file-dropper .uppreview').remove();//remove any prior upload/preview
    
    if (AJS.badFile()){
      AJS.cancelFile();
      return false;
    }
    
    if (typeof(FileReader)=='undefined')
      return false;
    
    var reader = new FileReader();
    reader.onload = function (evt){
      log(evt.target);
      var im = new Image();
      im.src = evt.target.result;
      $('#file-dropper').append(im);
      $(im).addClass('uppreview');
      //$('#file-dropper').css({'background':'url(' + evt.target.result + ') no-repeat center center'});
    };
    
    log(AJS.file2post);
    reader.readAsDataURL(AJS.file2post);
    return true;
  };

  
  AJS.cancelFile = function(){
    $('#file-dropper .uppreview').remove();//remove any prior upload/preview
    $('#file-dropper, file-submit, #file-cancel, #file-uploading').hide();
    if (AJS.file_picked)
      delete AJS.file_picked;
    if (AJS.file2post)
      delete AJS.file2post;
  };
  

  AJS.badFile = function(){
    var fail=false;

    if (!fail  &&  !AJS.file2post)
      fail = 'file is missing';
    
    // php upload_max_filesize is 8M
    if (!fail  &&  AJS.file2post.size > 8000000)
      fail = 'file is over 8MB in size';

    if (!fail){
      var type=AJS.file2post.type.toLowerCase();
      if (!(type=="image/jpeg" || type=="image/png" || type=="image/gif"))
        fail = 'file not required format of JPEG or PNG or GIF';
    }
    
    if (fail)
      alert(fail);
    
    return fail;
  };
  
  
  
  // for microfilm books with many months of newspapers in PDF
  //
  // paginfo is a logical hashmap JSON object of:
  //   YYYYMMDD => [comma separated list of pages]
  // eg:
  //   20080129 =>  "1,2,3,11,17"
  AJS.drawPDF = function(identifier, pageinfo)
  {
    var str='';
    var lastyearmonth=666;
    var urlstart = '/download/'+identifier+
    '/'+identifier+'_pdf.zip/'+identifier+'_pdf/'+identifier+'_';

    var multi_year=false;
    var last_year =false;
    for (var key in pageinfo)
    {
      var year     = key.substr(0,4);
      if (last_year===false) last_year=year;
      if (last_year != year) {
        multi_year=true;
        break;
      }
    }

    for (var key in pageinfo)
    {
      var pages    = pageinfo[key].split(",");
      var year     = key.substr(0,4);
      var month    = key.substr(4,2);
      var day      = key.substr(6,2);
      var yearmonth= year + '-' + month;
      var skip_day = false;
      if (yearmonth != lastyearmonth)
      {
        /**/ if (month== 1) monthName=("January");
        else if (month== 2) monthName=("February");
        else if (month== 3) monthName=("March");
        else if (month== 4) monthName=("April");
        else if (month== 5) monthName=("May");
        else if (month== 6) monthName=("June");
        else if (month== 7) monthName=("July");
        else if (month== 8) monthName=("August");
        else if (month== 9) monthName=("September");
        else if (month==10) monthName=("October");
        else if (month==11) monthName=("November");
        else if (month==12) monthName=("December");
        else if (month===undefined) monthName=("Single Page PDFs");
        else /*          */ monthName=("Unknown_" + month);

        if (multi_year) monthName = year + ' ' + monthName;

        // make header/a that shows/hides a hidden div after it with the
        // month's data
        str +=
          (str == '' ? '' : '</div><!--mo--><br/>')+
          '<a href="#'+monthName+','+year+'" onclick="$(\'#m'+
          yearmonth+'\').toggle(); return false;">+<img src="/images/folder.png"/> <u>'+monthName+
          '</u></a>'+'<div class="mo" id="m'+yearmonth+'">';
      }

      // make header/a that shows/hides a hidden div after it with the page data
      if ((day===undefined)||(day=='')) skip_day=true;
      if (skip_day)
      {
        str += '<div class="day">';
      }
      else
      {
        str +=
        '<div class="day"><a href="#'+yearmonth+'-'+day+
        '" onclick="$(\'#m'+yearmonth+
        'd'+day+'\').toggle();return false;">+<img src="/images/folder.png"/> <u>'+day+
        '</u></a> <div class="pages" id="m'+yearmonth+'d'+day+'">';
      }

      // drop in the individual page links

      var offset = 1,page,pnum;
      for (j=0; j < pages.length; j++)
      {
        page = pages[j];
        if (!page)
          continue;
        if (offset>0) offset=1-page;
        pnum=parseInt(page)+offset;

        // left 0-pad to 4 digits as needed
        page = '0000'+page;
        page = page.substr(page.length-4, 4);

        var url = urlstart + page + '.pdf';
        str += '<a href="'+url+'">['+pnum+']</a> ';
      }

      if (skip_day)
        str += '</div>';
      else
        str += '</div><!--pages-->' + '</div><!--day-->';

      lastyearmonth = yearmonth;
    }

    str += '</div><!--mo-->';

    // replace the "pdfs" empty div with our hefty HTML
    var o=document.getElementById('pdfs');
    //alert(str);
    o.innerHTML = str;
    return false;
  };




  // code that deals with MORE and LESS in descriptions
  AJS.lessMaxHT=200;
  AJS.morelessClick = function(toggle){
    if (toggle  ||  !AJS.morelessClickFirst){
      $('#descript').toggleClass('less');
      $('#moreless').find('b').toggle();
    }
    AJS.morelessClickFirst=true;
    if($('#descript').hasClass('less'))
      $('#descript').css('max-height',AJS.lessMaxHT);
    else
      $('#descript').css('max-height',10000);
    return false;
  };
  AJS.morelessSetup = function(evt){
    var show=false;
    if ($('#descript .lesshere').length){
      AJS.lessMaxHT=Math.max(AJS.lessMaxHT, $('#descript .lesshere').offset().top - $('#descript').offset().top);
      $('#descript').addClass('less').css('max-height',AJS.lessMaxHT);
      show=true;
    }
    log('morelessSetup: '+$('#descript').height()+' -v- '+AJS.lessMaxHT);
    
    if ($('#descript').height() >= AJS.lessMaxHT){
      $('#moreless').show();
      AJS.morelessClick();
    }
    else if (show){
      $('#moreless').show();
      $('#descript').addClass('less');
      $('#moreless').find('b').toggle();
    }
  };
  // code that deals with MORE and LESS in descriptions
 
  


  // parse a CGI arg
  AJS.arg = function(theArgName){
    sArgs = location.search.slice(1).split('&');
    r = '';
    for (var i=0; i < sArgs.length; i++){
      if (sArgs[i].slice(0,sArgs[i].indexOf('=')) == theArgName){
        r = sArgs[i].slice(sArgs[i].indexOf('=')+1);
        break;
      }
    }
    return (r.length > 0 ? unescape(r).split(',') : '');
  };


  AJS.search_this_list = function(url){
    if (url===''){
      // take the page's current url, minus any hash
      var searchlist_submit_re = new RegExp(location.hash+'$');
      url = location.href.replace(searchlist_submit_re,'');
    }

    url += ($('#searchlist').val()==='' ? '' : (location.search===''?'?':'&')+'and[]=' +$('#searchlist').val());

    // add back in any hash (eg: tab selected)
    url += location.hash;

    location.href = url;
    return false;
  };
  

  // navigation search icon clicked
  AJS.search = function(lnk){
    var $li = $(lnk).parents('li');
    AJS.searchHTM = $li.html();
    
    var placeholder = 'Universal Access to Knowledge';
    var pegW = 250;
    var $iac = $('.container-ia:last');
    var hamburgered = false; // $('.navbar-toggle').is(':visible'); // always false now
    if (!hamburgered  &&  $iac.length){
      // We have a nav bar showing / aren't small width.
      // So figure out how much width we have avail for the text box...
      var maxRite = $iac.offset().left + $iac.width();

      var $nbc = $('.navbar .container');
      maxRite = Math.min(maxRite,  $nbc.offset().left + $nbc.width());

      maxRite -= 25; // space for "padding-right" and the "X"

      pegW = Math.max(40, maxRite - $li.offset().left);
      log('pegW: '+pegW);

      if (pegW < 150)
        // blow it out to the left, over other elements
        pegW = 250;
      else if (pegW < 190)
        placeholder='Search'; //shorten text
    }
    

    var liR = $li.offset().left + $li.outerWidth();

    $('#search-nav-slim').remove();
    $('.navbar .container').prepend('\n\
<div id="search-nav-slim" style="z-index:1;position:absolute;top:1px;left:'+(liR-pegW-10/*some space for [X]*/)+'px;width:'+(pegW)+'px">\n\
  <form class="form-inline" id="searchform" method="post" role="form" action="/searchresults.php" style="position:relative;right:0;width:'+pegW+'px;">\n\
    <div style="font-weight:600;font-size:14px;border-left:1px solid #bbb;background-color:white;width:20px;text-align:center;cursor:pointer;position:absolute;right:2px;top:3px;" onclick="$(\'#searchform\').submit()">\n\
      <span class="iconochive-search"></span>\n\
    </div>\n\
    <input class="form-control input-sm" tabindex="1" name="search" placeholder="'+placeholder+'" type="text" autofocus="autofocus" value="" style="padding-right:25px;'+(pegW?'width:'+pegW+'px;':'')+'">\n\
    <input style="position:absolute;z-index:-1;width:1px;height:1px;right:0;top:0;" type="submit" value=""/>\n\
    <div style="position:absolute;right:-20px;top:5px;color:white;font-weight:600;cursor:pointer;" class="iconochive-x ghost80 hidden-xs" onclick="$(\'#search-nav-slim\').remove();"></div>\n\
    <div style="position:absolute;right:-14px;top:8px;color:white;font-weight:600;cursor:pointer;font-size:10px;" class="iconochive-x ghost80 hidden-sm hidden-md hidden-lg" onclick="$(\'#search-nav-slim\').remove();"></div>\n\
  </form>\n\
</div>\n\
');

    
    // Fill w/ any existing search query in the GET url.
    // We do this *now* instead of above so we get complex query quoting right/more right ;-)
    var tmp = AJS.arg('query');
    if (tmp!=="")
      $('#searchform input[name=search]').val(tmp.toString().replace(/\+/g,' '));
    
    return false;
  };


  AJS.scrolled = function(){
    var newtop = $(window).scrollTop();
    //log('scrolled to '+newtop);

    var selector = AJS.selector() + ' .more_search';
    var $e=$(selector);
    if (!$e.length)
      return;
    
    var check = $e.offset().top + $e.outerHeight() - $(window).height();
    if (newtop > check){
      log('scrolled to top > '+check);
      if (!AJS.more_searching)
        $(selector+' > a').click();
    }
  };
  

  AJS.page_map = {};
  AJS.more_search = function(lnk, url){

    var selector = AJS.selector();
    if (selector===false)
      return false;

    
    var ikind = selector.replace(/#ikind-/,'');
    var $more_search = $(selector + ' .more_search');
    
    // we stash a reference of what page the client has requested, so we can increment it on
    // each "more_search hit
    var pageKey = selector;
    if (typeof(AJS.page_map[pageKey])=='undefined')
      AJS.page_map[pageKey]=1;

    if (AJS.page_map[pageKey] < 0){
      //$more_search.find('.more-search-all').show();
      return false; // all results showing -- no more for this ikind avail so noop/ignore
    }
    
    AJS.page_map[pageKey]++;
    var page = AJS.page_map[pageKey];

    $more_search.find('.more-search-fetching').show();
    
    AJS.more_searching = true;

    url += page + (selector==='#ikind-search' ? '' : '&ikind='+ikind);

    log('more_search(selector='+selector+', ikind='+ikind+', page='+page+', url='+url+')');
    //log(AJS.page_map);
    
      
    $.get(url, function(htm){
      if (htm.length < 100  &&  $(htm).find('div.no-results')){
        // no more results avail/found.  we have reached infinity!
        $more_search.find('.more-search-fetching, a.btn').hide();
        //$more_search.find('.more-search-all').show();
        AJS.page_map[pageKey] = -1; // flag to ignore future more_search attempts
        AJS.more_searching = false;        
        return;
      }

      
      $(selector+' .results').append(htm);
      
      // re-tile and re-flow!  (the force should flow through you)
      AJS.tiler(selector);
      $more_search.find('.more-search-fetching').hide();
      AJS.more_searching = false;

      // OK, this is quite a bit more subtle...  the HTM has been dropped in,
      // *and* we've done a basic re-tiling.  however, it's very likely many
      // of the images are still loading.  so listen for image "is loaded" events
      // if they trickle in, and at most re-tile every 1 second (even if they
      // likely trickling in at a faster rate than that) until they all loaded
      $(selector + ' img').on('load', function(evt){
        $.doTimeout('tiles-img-load', 1000, AJS.tiler);
      });
    });
         
      
    return false;
  };


  // when embed codes are being show, adjust their heights so they show all the content/codes!
  AJS.embed_codes_adjust = function(){
    log('showing embeds!');

    // these are found (only) on /details/ pages
    var embids={'embedcodehere':1, 'embedcodehereWP':1};
    for (var embid in embids){
      var $embid=$('#'+embid);
      $embid.removeAttr('rows').css('height','');

      var embtxt = $embid.text();
      
      // this is *puke* city -- since textareas are a PITA, make a shadow div w/ the
      // text we want in it, trying to be same width, same font-size, etc.
      // and *then* insert into DOM invisibily so we can calculate its overall height
      // .. and then peg the textarea height to that height
      $('body').prepend($('<div/>')
                        .attr({'id':embid+'Shadow','class':'textarea-invert-readonly roundbox5'})
                        .css({'position':'absolute',
                              'visibility':'hidden',
                              'top':60,
                              'left':10,
                              'padding':'5px 15px 5px 15px',
                              'width':$embid.width(),
                              'font-size':$embid.css('font-size')
                             })
                        .text(embtxt)
                       );
      var bestHT = $('#'+embid+'Shadow').outerHeight() + 15;
      log(embid + ' bestie height: '+bestHT + ' (for current width: '+$embid.width()+')');
      $('#'+embid+'Shadow').remove();

      $embid.height(bestHT);
    }
  };
  
  

  // gives us ability to have JS intercept an href click and instead do a bootstrap modal.
  // eg: when someone Favorites a list or item - a kind of confirmation box.
  //     In that case, we want it to just say Favorited with a big black star in the middle.
  //     We don't take user to the href target.
  //     The modal disappears if the user clicks anywhere and returns to page they were on.
  //     Timeout of a few seconds to close modal if they do nothing.
  // config is a hashmap with optional keys:
  //   auto_close, auto_remove, body, center, favorite, ignore_lnk, titlen, headerClass, shown, follow_link_on_dismiss, beta_feedback
  AJS.modal_go = function(lnk, conf){
    if (conf.favorite){
      // this flicks on a bunch of config options
      conf.center=true;
      conf.auto_close=true;
      conf.title='Favorited';
      conf.body='<center><span style="font-size:100px;" class="iconochive-favorite"></span></center>';
      conf.login=true; // must be logged in!
    }
    log(conf);

    if (conf.beta_feedback){
      conf.body = '\n\
<style>#fback h4 { margin-top:30px; }</style>\n\
<div id="fback" style="padding:20px">\n\
<b style="font-size:125%;">Thank you</b> for trying the beta version of Archive.org.<br/>\n\
We will consider your comments as we make changes to the site.\n\
\n\
<form method="POST" action="/services/exit.php" _target="top" onsubmit="AJS.beta_feedbacked=true">\n\
<h4>Will you try the beta again?</h4>\n\
<input type="radio" name="will_try_again" value="yes"/>        yes <br/>\n\
<input type="radio" name="will_try_again" value="maybe"/>      maybe <br/>\n\
<input type="radio" name="will_try_again" value="no"/>         no <br/>\n\
\n\
<h4>Give us feedback about the beta site:</h4>\n\
<textarea class="form-control input-sm" name="feedback" rows="4"></textarea> <br/>\n\
\n\
<h4>May we contact you about your feedback?</h4>\n\
<input type="radio" name="contact" value="yes"/> yes, here\'s my email address:<br/>\n\
<input class="form-control input-sm" type="text" name="eml"/> <br/>\n\
<input type="radio" name="contact" value="no"/> no<br/>\n\
\n\
<input class="btn btn-primary" type="submit" style="margin-top:20px" value="Submit"/>\n\
\n\
</form>\n\
</div>';
    }
    
    var selector = $(lnk).attr('data-target');
    var href     = $(lnk).attr('href');
    var selectorID = selector.replace(/#/,'');

    var headerClass = (conf.headerClass ? conf.headerClass : 'modal-header-std');

    // dynamically add modal to page (if isnt there already)
    if (!$(selector).length){
      $('body').prepend('\n\
<div id="'+selectorID+'" class="modal fade" role="dialog" aria-hidden="true">\n\
  <div class="modal-dialog modal-lg">\n\
    <div class="modal-content">\n\
      <div class="modal-header '+headerClass+'">\n\
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="iconochive-remove-circle"></span></button>\n\
        <h2 class="modal-title">'+(typeof(conf.title)=='undefined'?'Confirmed':conf.title)+'</h2>\n\
      </div>\n\
      <div id="'+selectorID+'-body">\n\
        '+(typeof(conf.body)=='undefined'?'':conf.body)+'\n\
      </div>\n\
    </div>\n\
  </div>\n\
</div>\n\
');
    }

    if (conf.shown){
      $(selector).on('shown.bs.modal', function(evt){
        conf.shown();
      });
    }

    if (conf.follow_link_on_dismiss){
      $(selector).on('hidden.bs.modal', function(evt){
        // a form submit might have taken over...
        if (!AJS.beta_feedbacked){
          log('modal hidden, going to '+href+'..');
          location.href = href;
        }
      });
    }
    
                     
    $(selector).modal('show');
    

    if (conf.login  &&  $.cookie('logged-in-user')===null){
      if (location.protocol!='https:'){
        // make absolutely sure we never login with http!
        location.href = 'https://archive.org/account/login.php';
        return false;
      }
      var url='/account/login.php';
      $.get(url, function(htm){

        $('#'+selectorID+' .modal-title').text('Nearly there!  Please login first');
        
        $(selector).modal('show');
        // this allows us to effectively rip off the header/nav and footer
        // if we are accessing a full page (eg: account login page)
        if ($(htm).find('.container-ia > div').length)
          htm = $(htm).find('.container-ia > div').get(0);
        
        $('#'+selectorID+'-body').html(htm);

        var $form=$('#'+selectorID+'-body form:has(input[type=submit])');
        
        if ($form.length){
          $form.on('submit',function(evt){
            evt.preventDefault();
            evt.stopPropagation();
            
            var keyvals={};
            $form.find('input').each(function(k,v){
              if ($(v).attr('name').length)
                keyvals[$(v).attr('name')] = $(v).val();
            });
            //log('POST: '+url);
            //console.log(keyvals);
            $.post(url, keyvals, function(htm){
              log('SUBMITTED');
              if ($.cookie('logged-in-user')!==null){
                // SUCCESS!
                // We are going to hide the login modal now, and need
                // to proceed only *after* the modal animation is done
                // and modal is gone.  So setup listener now, *then* hide.
                // Once login modal is gone, remove the modal, and
                // invoke the originally intended modal in fresh environment!
                $(selector).on('hidden.bs.modal', function(evt){
                  $(selector).remove();
                  log('SUCCESS');
                  AJS.modal_go(lnk, conf); // invoke original modal!
                });
                $(selector).modal('hide');
              }
              else{
                alert('Please try logging in again');
              }
            });
            
            return false;
          });
        }
      });
      return false;
    }
    
            
    if (conf.auto_remove){
      conf.auto_close = true;
      $(selector).on('hidden.bs.modal', function(){ $(selector).remove(); });
    }
    
    
    if (!conf.ignore_lnk){
      var url=lnk.href;
      //log('submit '+url);
      $.get(url, function(htm){
        $(selector).modal('show');
        if (conf.auto_close)
          setTimeout(function(){ $(selector).modal('hide'); },2000);
      });
    }
    else{
      $(selector).modal('show');
    }
    
    if (conf.center)
      $(selector+' .modal-dialog').center(); // vertically center

    if (conf.auto_close  &&  conf.ignore_lnk)
      setTimeout(function(){ $(selector).modal('hide'); },2000);
    
    return false;
  };


  
  
  

  // (adapted) from http://www.queness.com/code-snippet/6853/center-an-element-on-the-screen-using-jquery
  // then you can use it like:
  //      $("#gridmini").center()
  
  jQuery.fn.center = function () {
    var myheight = (this.height() + 
                    parseInt(this.css('padding-top'),10) + 
                    parseInt(this.css('padding-bottom'),10));
  
    log('myheight='+myheight);
    log('mywidth=' +this.width());
    log('w.height='+$(window).height()+' '+
        'w.width='+$(window).width()+' '+
        'w.scrollTop='+$(window).scrollTop()+' '+
        'w.scrollLeft='+$(window).scrollLeft()+' ');
    
    
    var top, left;
    if (AJS.ios){
      var vpW = window.innerWidth;
      var vpH = window.innerHeight;
      
      log('vpH: ' + vpH);
      log('vpW: ' + vpW);
      
      top  = (vpH - myheight     ) / 2  +  $(window).scrollTop();
      left = (vpW - this.width() ) / 2  +  $(window).scrollLeft();
    }
    else{
      top  = ($(window).height() - myheight)/2;
      left = ($(window).width()  - this.width())/2;
    }
    
    //for small viewports like iphone, when centering popups, ensure minimally +20px
    top  = Math.max(20, top);
    left = Math.max(20, left);
    
    this.css({"position":(AJS.ios?"absolute":"fixed"),
              "display":"block",
              "top":  top  +"px",
              "left": left +"px"});
    
    return this;
  }; // end jQuery.fn.center



  // Use a color sampling 3rd party piece of code, to find the 1st/primary image in the "welcome" area,
  // and find the "dominant color".  (internally, uses canvas to color sample).
  // Adjust text/foreground color as well as any "selected tab" background color.
  // NOT USED ANYMORE -- GONE SERVERSIDE!
  AJS.welcome_recolor = function(e){
    if (typeof(ColorThief)=='undefined'  ||  !e.length)
      return;

    if (AJS.welcome_recolored)
      return;
    AJS.welcome_recolored = true;

    var img=$(e)[0];
    log('welcome_recolor('+(img.src)+')');

    var ct = new ColorThief();
    var palette = ct.getPalette(img, 5);
    log(palette);
    if (!palette)
      return;
    var dominantColorRGB = palette[0];//3 element array!

    if (!dominantColorRGB)
      return;
    log(dominantColorRGB);

    // if dominant color is uh, kinda in the lighter range -- txt should be black
    var avgVal = Math.round($(dominantColorRGB).map( function(idx,val){ return val; }).toArray().reduce(function(a,b){return a+b;}) / dominantColorRGB.length);
    var color = (avgVal >= 128 ? 'black' : 'white');
    log('avgVal: '+avgVal);
    
    $('.welcome').css({'color':color,'background-color':'rgb('+dominantColorRGB[0]+','+dominantColorRGB[1]+','+dominantColorRGB[2]+')'});
    $('.welcome .stealth:not(.tabby .stealth)').css({'color':color});
  };
}( jQuery ));








/*********************************************************************************************
 *
 *
 *    THIRD PARTY INCLUDES WE CANT LIVE WITHOUT! 8-)
 *
 *
 *********************************************************************************************/


/**
 * Cookie plugin
 *
 * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */

/**
 * Create a cookie with the given name and value and other optional parameters.
 *
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Set the value of a cookie.
 * @example $.cookie('the_cookie', 'the_value', { expires: 7, path: '/', domain: 'jquery.com', secure: true });
 * @desc Create a cookie with all available options.
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Create a session cookie.
 * @example $.cookie('the_cookie', null);
 * @desc Delete a cookie by passing null as value. Keep in mind that you have to use the same path and domain
 *       used when the cookie was set.
 *
 * @param String name The name of the cookie.
 * @param String value The value of the cookie.
 * @param Object options An object literal containing key/value pairs to provide optional cookie attributes.
 * @option Number|Date expires Either an integer specifying the expiration date from now on in days or a Date object.
 *                             If a negative value is specified (e.g. a date in the past), the cookie will be deleted.
 *                             If set to null or omitted, the cookie will be a session cookie and will not be retained
 *                             when the the browser exits.
 * @option String path The value of the path atribute of the cookie (default: path of page that created the cookie).
 * @option String domain The value of the domain attribute of the cookie (default: domain of page that created the cookie).
 * @option Boolean secure If true, the secure attribute of the cookie will be set and the cookie transmission will
 *                        require a secure protocol (like HTTPS).
 * @type undefined
 *
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */

/**
 * Get the value of a cookie with the given name.
 *
 * @example $.cookie('the_cookie');
 * @desc Get the value of a cookie.
 *
 * @param String name The name of the cookie.
 * @return The value of the cookie.
 * @type String
 *
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */
jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { // name and value given, set cookie
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
      
        // CAUTION: Needed to parenthesize options.path and options.domain
        // in the following expressions, otherwise they evaluate to undefined
        // in the packed version for some reason...
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';

        if (navigator.userAgent.indexOf('MSIE 9.')>0){ // IE9 sucks and can kiss it (tracey may2012)
          domain='';
          path='';
        }
          
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
        return true;
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};
  

  
/*
 * jQuery doTimeout: Like setTimeout, but better! - v1.0 - 3/3/2010
 * http://benalman.com/projects/jquery-dotimeout-plugin/
 *
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function($){var a={},c="doTimeout",d=Array.prototype.slice;$[c]=function(){return b.apply(window,[0].concat(d.call(arguments)))};$.fn[c]=function(){var f=d.call(arguments),e=b.apply(this,[c+f[0]].concat(f));return typeof f[0]==="number"||typeof f[1]==="number"?this:e};function b(l){var m=this,h,k={},g=l?$.fn:$,n=arguments,i=4,f=n[1],j=n[2],p=n[3];if(typeof f!=="string"){i--;f=l=0;j=n[1];p=n[2]}if(l){h=m.eq(0);h.data(l,k=h.data(l)||{})}else{if(f){k=a[f]||(a[f]={})}}k.id&&clearTimeout(k.id);delete k.id;function e(){if(l){h.removeData(l)}else{if(f){delete a[f]}}}function o(){k.id=setTimeout(function(){k.fn()},j)}if(p){k.fn=function(q){if(typeof p==="string"){p=g[p]}p.apply(m,d.call(n,i))===true&&!q?o():e()};o()}else{if(k.fn){j===undefined?e():k.fn(j===false);return true}else{e()}}}})(jQuery);
