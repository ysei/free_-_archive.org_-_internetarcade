<!DOCTYPE html>
<html>
<!-- __ _ _ _ __| |_ (_)__ _____ 
    / _` | '_/ _| ' \| |\ V / -_)
    \__,_|_| \__|_||_|_| \_/\___| -->
<head>
  <title>Internet Archive Frequently Asked Questions</title>
      <script src="//archive.org/includes/jquery-1.6.1.js" type="text/javascript"></script>
    <script src="//archive.org/includes/analytics.js?v=62943" type="text/javascript"></script>
    <link href="//archive.org/includes/archive.css?v=62943" rel="stylesheet" type="text/css"/>
  <link rel="SHORTCUT ICON" href="/images/glogo.jpg"/>
  </head>
<body class="Home">
   
<!--BEGIN HEADER 1-->
<table id="nunuh1" cellspacing="0" width="100%" border="0" cellpadding="0">
  <tbody>
    <tr> 
      <td id="logo">
        <a href="/"><img title="Internet Archive" alt="Internet Archive" src="/images/glogo2.png"/></a>
      </td>
      <td valign="bottom" id="navbg" style="position:relative">
        <table width="100%" border="0" cellpadding="5">
          <tr> 
            <td class="level1Header">
                            <div class="tab">
                <a href="http://archive.org/web/">Web</a>
              </div>
                            <div class="tab">
                <a href="https://archive.org/details/movies">Video</a>
              </div>
                            <div class="tab">
                <a href="https://archive.org/details/texts">Texts</a>
              </div>
                            <div class="tab">
                <a href="https://archive.org/details/audio">Audio</a>
              </div>
                            <div class="tab">
                <a href="https://archive.org/details/software">Software</a>
              </div>
                            <div class="tabsel backColor1">
                <a href="https://archive.org/about/">About</a>
              </div>
                            <div class="tab">
                <a href="https://archive.org/account/login.php">Account</a>
              </div>
                            <div class="tab">
                <a href="https://archive.org/details/tv"><img style="margin-right:2px;width:16px;height:15px" src="/images/tvnews.png"/>TVNews</a>
              </div>
                            <div class="tab">
                <a href="https://openlibrary.org"><img style="margin-right:2px;width:16px;height:16px" src="/images/ol.png"/>OpenLibrary</a>
              </div>
                          </td>
          </tr>
        </table>
      </td>
      <td style="width:80px; height:72px; vertical-align:middle; text-align:right">
        <a href="/about/faqs.php"><img title="Universal Access To All Knowledge" alt="Universal Access To All Knowledge" id="TRimg" src="/images/access.png"/></a>      </td>
    </tr>
  </tbody>
</table>

     
<!--BEGIN HEADER 2-->
<table width="100%" class="level2Header">
  <tbody>
    <tr>
      <td align="left" valign="top" class="level2HeaderLeft">
        <a class="level2Header" href="/">Home</a>
      </td>
      <td class="level2Header" style="width:100%;">
        <a href="https://archive.org/donate/index.php">Donate</a> | 
<a href="https://store.archive.org">Store</a> | 
<a href="https://blog.archive.org">Blog</a> | 
<a class="headerAt" href="https://archive.org/about/faqs.php">FAQ</a> | 
<a href="https://archive.org/about/jobs.php">Jobs</a> | 
<a href="https://archive.org/about/volunteerpositions.php">Volunteer Positions</a> | 
<a href="https://archive.org/about/contact.php">Contact</a> | 
<a href="https://archive.org/about/bios.php">Bios</a> | 
<a href="https://archive.org/iathreads/forums.php">Forums</a> | 
<a href="https://archive.org/projects">Projects</a> | 
<a href="https://archive.org/about/terms.php">Terms, Privacy, & Copyright</a>
      </td>
    </tr>
  </tbody>
</table>
     

<!--BEGIN HEADER 3-->
<div class="level3HeaderColorBar"> </div>
<table cellspacing="0" width="100%" border="0" cellpadding="0">
  <tbody>
    <tr> 
      <td class="level3Header level3HeaderLeft">
        <form style="margin:0;padding:0;" action="http://archive.org/searchresults.php" id="searchform" method="post">
          <b>Search:</b>
          <input tabindex="1" size="25" name="search" value="" style="font-size: 8pt"/>
          <select tabindex="2" style="PADDING-TOP: 2px; font-size: 8pt;" name="mediatype">
            <option value="all">All Media Types</option>
            <option value="web">&nbsp;&nbsp;Wayback Machine</option>
            
            <option  value="movies">&nbsp;&nbsp;Video</option>
<option value="movies.animationandcartoons">&nbsp;&nbsp;&nbsp;&nbsp;Animation & Cartoons</option>
<option value="movies.artsandmusicvideos">&nbsp;&nbsp;&nbsp;&nbsp;Arts & Music</option>
<option value="movies.opensource_movies">&nbsp;&nbsp;&nbsp;&nbsp;Community Video</option>
<option value="movies.computersandtechvideos">&nbsp;&nbsp;&nbsp;&nbsp;Computers & Technology</option>
<option value="movies.culturalandacademicfilms">&nbsp;&nbsp;&nbsp;&nbsp;Cultural & Academic Films</option>
<option value="movies.ephemera">&nbsp;&nbsp;&nbsp;&nbsp;Ephemeral Films</option>
<option value="movies.moviesandfilms">&nbsp;&nbsp;&nbsp;&nbsp;Movies</option>
<option value="movies.newsandpublicaffairs">&nbsp;&nbsp;&nbsp;&nbsp;News & Public Affairs</option>
<option value="movies.prelinger">&nbsp;&nbsp;&nbsp;&nbsp;Prelinger Archives</option>
<option value="movies.spiritualityandreligion">&nbsp;&nbsp;&nbsp;&nbsp;Spirituality & Religion</option>
<option value="movies.sports">&nbsp;&nbsp;&nbsp;&nbsp;Sports Videos</option>
<option value="movies.television">&nbsp;&nbsp;&nbsp;&nbsp;Television</option>
<option value="movies.gamevideos">&nbsp;&nbsp;&nbsp;&nbsp;Videogame Videos</option>
<option value="movies.vlogs">&nbsp;&nbsp;&nbsp;&nbsp;Vlogs</option>
<option value="movies.youth_media">&nbsp;&nbsp;&nbsp;&nbsp;Youth Media</option>
<option  value="texts">&nbsp;&nbsp;Texts</option>
<option value="texts.americana">&nbsp;&nbsp;&nbsp;&nbsp;American Libraries</option>
<option value="texts.toronto">&nbsp;&nbsp;&nbsp;&nbsp;Canadian Libraries</option>
<option value="texts.universallibrary">&nbsp;&nbsp;&nbsp;&nbsp;Universal Library</option>
<option value="texts.opensource">&nbsp;&nbsp;&nbsp;&nbsp;Community Texts</option>
<option value="texts.gutenberg">&nbsp;&nbsp;&nbsp;&nbsp;Project Gutenberg</option>
<option value="texts.biodiversity">&nbsp;&nbsp;&nbsp;&nbsp;Biodiversity Heritage Library</option>
<option value="texts.iacl">&nbsp;&nbsp;&nbsp;&nbsp;Children's Library</option>
<option value="texts.additional_collections">&nbsp;&nbsp;&nbsp;&nbsp;Additional Collections</option>
<option  value="audio">&nbsp;&nbsp;Audio</option>
<option value="audio.audio_bookspoetry">&nbsp;&nbsp;&nbsp;&nbsp;Audio Books & Poetry</option>
<option value="audio.opensource_audio">&nbsp;&nbsp;&nbsp;&nbsp;Community Audio</option>
<option value="audio.audio_tech">&nbsp;&nbsp;&nbsp;&nbsp;Computers & Technology</option>
<option value="audio.GratefulDead">&nbsp;&nbsp;&nbsp;&nbsp;Grateful Dead</option>
<option value="audio.etree">&nbsp;&nbsp;&nbsp;&nbsp;Live Music Archive</option>
<option value="audio.audio_music">&nbsp;&nbsp;&nbsp;&nbsp;Music, Arts & Culture</option>
<option value="audio.netlabels">&nbsp;&nbsp;&nbsp;&nbsp;Netlabels</option>
<option value="audio.audio_news">&nbsp;&nbsp;&nbsp;&nbsp;News & Public Affairs</option>
<option value="audio.audio_foreign">&nbsp;&nbsp;&nbsp;&nbsp;Non-English Audio</option>
<option value="audio.audio_podcast">&nbsp;&nbsp;&nbsp;&nbsp;Podcasts</option>
<option value="audio.radioprograms">&nbsp;&nbsp;&nbsp;&nbsp;Radio Programs</option>
<option value="audio.audio_religion">&nbsp;&nbsp;&nbsp;&nbsp;Spirituality & Religion</option>
<option  value="education">&nbsp;&nbsp;Education</option>
<option value="education.msri">&nbsp;&nbsp;&nbsp;&nbsp;Math Lectures from MSRI</option>
<option value="education.chinese_u_lectures">&nbsp;&nbsp;&nbsp;&nbsp;Chinese University Lectures</option>
<option value="education.uchannel">&nbsp;&nbsp;&nbsp;&nbsp;UChannel</option>
<option value="education.ap_courses">&nbsp;&nbsp;&nbsp;&nbsp;AP Courses</option>
<option value="education.mit_ocw">&nbsp;&nbsp;&nbsp;&nbsp;MIT OpenCourseWare</option>
            
            <option value="forums" >Forums</option>
            <option value="faqs"   selected="selected">FAQs</option>
          </select>
          <input tabindex="3" style="vertical-align:bottom; text-align:center; width:21px; height:21px; border:0px" name="gobutton" type="image" id="gobutton" value="Find" src="/images/search.png"/>
          <input type="hidden" name="limit" value="100"/>
          <input type="hidden" name="start" value="0"/>
          <input type="hidden" name="searchAll" value="yes"/>
          <input type="hidden" name="submit" value="this was submitted"/>
          <a href="http://archive.org/advancedsearch.php" class="level3Header level3HeaderSearch">Advanced Search</a>
        </form>
      </td>

      
      

      <td class="level3Header level3HeaderUser2">
        <b>
          Anonymous User        </b>
        <span style="font-size:7pt; white-space:nowrap">
          (<a class="level3Header" href="https://archive.org/account/login.php">login</a>
          or
          <a class="level3Header" href="https://archive.org/account/login.createaccount.php">
            join us</a>)
        </span>
      </td>
      
  

      <!--upload files button-->
            <td class="level3Header level3HeaderUser2">
        <a alt="Share your Files with the Internet Archive" title="Share your Files with the Internet Archive" href="http://archive.org/create/"><div class="buttonB">Upload</div></a>
      </td>
      
    </tr>
  </tbody>
</table>

<div id="begPgSpcr" style="padding-bottom:17px; position:relative;"> </div>

<!--BEGIN PAGE-->
<div class="box" style="width:90%; margin-left:auto; margin-right:auto;">
  <h1>
        Frequently Asked Questions
  </h1>
  [
        <a style="font-weight:bold"
     href="#The_Internet_Archive">The Internet Archive</a>
      |  <a style="font-weight:bold"
     href="#Beta_Archive.org_Site">Beta Archive.org Site</a>
      |  <a style="font-weight:bold"
     href="#The_Wayback_Machine">The Wayback Machine</a>
      |  <a style="font-weight:bold"
     href="#Audio">Audio</a>
      |  <a style="font-weight:bold"
     href="#Law_Enforcement_Requests">Law Enforcement Requests</a>
      |  <a style="font-weight:bold"
     href="#Live_Music_Archive">Live Music Archive</a>
      |  <a style="font-weight:bold"
     href="#Texts_and_Books">Texts and Books</a>
      |  <a style="font-weight:bold"
     href="#Virtual_Library_Cards_(AKA_Accounts)">Virtual Library Cards (AKA Accounts)</a>
      |  <a style="font-weight:bold"
     href="#The_Internet_Arcade">The Internet Arcade</a>
      |  <a style="font-weight:bold"
     href="#Movies">Movies</a>
      |  <a style="font-weight:bold"
     href="#Downloading_Content">Downloading Content</a>
      |  <a style="font-weight:bold"
     href="#Borrow_from_Lending_Library">Borrow from Lending Library</a>
      |  <a style="font-weight:bold"
     href="#FreeCache">FreeCache</a>
      |  <a style="font-weight:bold"
     href="#DocuComp">DocuComp</a>
      |  <a style="font-weight:bold"
     href="#Uploading_Content">Uploading Content</a>
      |  <a style="font-weight:bold"
     href="#Prelinger_Movies">Prelinger Movies</a>
      |  <a style="font-weight:bold"
     href="#Search_Tips">Search Tips</a>
      |  <a style="font-weight:bold"
     href="#Forums">Forums</a>
      |  <a style="font-weight:bold"
     href="#SFLan">SFLan</a>
      |  <a style="font-weight:bold"
     href="#Archive_BitTorrents">Archive BitTorrents</a>
      |  <a style="font-weight:bold"
     href="#Rights">Rights</a>
      |  <a style="font-weight:bold"
     href="#Report_Item">Report Item</a>
      |  <a style="font-weight:bold"
     href="#Archive-It">Archive-It</a>
      |  <a style="font-weight:bold"
     href="#Equipment">Equipment</a>
    ]
</div>

<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#32">Does the Archive issue grants?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#311">Can I donate BitCoins?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#31">What is the nonprofit status of the Internet Archive? Where does its funding come from? </a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#294">How do I get assistance with research?  How about research about a particular book?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#292">What statistics are available about use of Archive.org?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#30">What's the significance of the Archive's collections?</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="The_Internet_Archive"></a>
          The Internet Archive        </h1>
                <p>
          <b>
            <a name="32"></a>
            Does the Archive issue grants?          </b>
                  </p>
        <p>
          No; although we promote the development of other Internet libraries through <a href="/about/contact.php#lists">online discussion</a>, <a href="/iathreads/forum-display.php?forum=news">colloquia</a>, and other means, the Archive is not a grant-making organization.        </p>
                <p>
          <b>
            <a name="311"></a>
            Can I donate BitCoins?          </b>
                  </p>
        <p>
          
Yes, please do.   Our BitCoin address is: 17gN64BPHtxi4mEM3qWrxdwhieUvRq8R2r .   Every bit helps.

        </p>
                <p>
          <b>
            <a name="31"></a>
            What is the nonprofit status of the Internet Archive? Where does its funding come from?           </b>
                  </p>
        <p>
          The Internet Archive is a 501(c)(3) nonprofit organization. It receives in-kind and financial donations from a variety of sources, including, but not limited to: <a href="http://www.alexa.com">Alexa Internet</a>, the Kahle/Austin Foundation, the Alfred P. Sloan Foundation, the William and Flora Hewlett Foundation, and <a href="/donate">you</a>.        </p>
                <p>
          <b>
            <a name="294"></a>
            How do I get assistance with research?  How about research about a particular book?          </b>
                  </p>
        <p>
          The Internet Archive focuses on preservation and providing access to digital cultural artifacts.  For assistance with research or appraisal, you are bound to find the information you seek elsewhere on the internet.  You may wish to inquire about reference services provided by your local public library.  

Your area's college library may also support specialized reference librarian services.  

We encourage your support of your local library, and the essential services your library's professional staff can provide in person.  Local libraries are still an irreplaceable resource!  
        </p>
                <p>
          <b>
            <a name="292"></a>
            What statistics are available about use of Archive.org?          </b>
                  </p>
        <p>
          <p>Aggregated statistics are graphed here:<br>
<a href="https://archive.org/stats">https://archive.org/stats</a>

<p>Additionally, each individual item shows a download counts are shown on the details page for individuals items, and collections pages list the most downloaded items for that collection.  

        </p>
                <p>
          <b>
            <a name="30"></a>
            What's the significance of the Archive's collections?          </b>
                  </p>
        <p>
          <p>Societies have always placed importance on preserving their culture and heritage. But much early 20th-century media -- television and radio, for example -- was not saved. <a href="http://www.bibalex.org/Home/Default_EN.aspx">The Library of Alexandria</a> -- an ancient center of learning containing a copy of every book in the world -- disappeared when it was burned to the ground.
</p>

<p>Special projects include <b>OpenLibrary.org</b> (<a href="https://archive.org/about/faqs.php#290">link to faq</a>). 
</p>


<br><br>
        </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#1023">Where is my account?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#1028">How can I try out the new beta site?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#1024">Where are my bookmarks?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#1025">How can I remove a favorite?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#1026">Where is the upload button?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#1027">How can I provide feedback about the beta?</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="Beta_Archive.org_Site"></a>
          Beta Archive.org Site        </h1>
                <p>
          <b>
            <a name="1023"></a>
            Where is my account?          </b>
                  </p>
        <p>
          You can get to your account by clicking the "My Library" link in the black bar at the top of your screen.  You can log in and out here, as well as see your favorites (formerly called bookmarks), forum posts, and anything you've uploaded.  Please try adding a photo and bio!        </p>
                <p>
          <b>
            <a name="1028"></a>
            How can I try out the new beta site?          </b>
                  </p>
        <p>
          Go to <a href="https://archive.org/v2">https://archive.org/v2</a> and you will be able to surf the archive.org web site in beta mode.  If you're in beta mode and you'd like to return to the old site, click the "Exit Beta" link in the black bar at the top of the site.        </p>
                <p>
          <b>
            <a name="1024"></a>
            Where are my bookmarks?          </b>
                  </p>
        <p>
          Bookmarks are called Favorites now!  You can favorite collections, media items, serches and people on the new beta site.  To find the list of your favorites, click the My Library link in the black bar at the top of the site and then go to your favorite's list.         </p>
                <p>
          <b>
            <a name="1025"></a>
            How can I remove a favorite?          </b>
                  </p>
        <p>
          Click the "My Library" link in the black bar at the top of the site, and then click to go into your favorites list.  Click "Remove items" in the upper right of the page, and then use the red "X" on each item to remove it.  Click "Remove items" again to turn off the functionality.        </p>
                <p>
          <b>
            <a name="1026"></a>
            Where is the upload button?          </b>
                  </p>
        <p>
          You will find an upload link in the upper right corner of your account page (click "My Library" in the black bar at the top of the site), and on any collection page that you have permissions to upload into.        </p>
                <p>
          <b>
            <a name="1027"></a>
            How can I provide feedback about the beta?          </b>
                  </p>
        <p>
          When you use the "EXIT BETA" link in the black bar on the top of the site, you will have an opportunity to give us feedback about the site.  You may also email info at archive dot org.        </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#18">Where is the rest of the archived site?  Why am I getting broken or gray images on a site?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#4">Can I link to old pages on the Wayback Machine?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#5">Why isn't the site I'm looking for in the archive?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#6">What does it mean when a site's archive data has been "updated"?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#7">Who was involved in the creation of the Internet Archive Wayback Machine?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#8">How was the Wayback Machine made?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#9">How large is the Wayback Machine?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#11">How do you archive dynamic pages?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#10">What type of machinery is used in this Internet Archive?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#14">Some sites are not available because of robots.txt or other exclusions. What does that mean?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#16">Can I search the Archive?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#2">How can I have my site's pages excluded from the Wayback Machine?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#12">Why are some sites harder to archive than others?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#29">How do you protect my privacy if you archive my site?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#19">How do I contact the Internet Archive?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#21">Why is the Internet Archive collecting sites from the Internet? What makes the information useful?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#22">Do you archive email? Chat?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#26">How can I get a copy of the pages on my Web site? If my site got hacked or damaged, could I get a backup from the Archive?'</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#24">Is there any personal information in these collections?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#20">What is the Wayback Machine's Copyright Policy?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#28">Can people download sites from the Wayback?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#201">What does 'failed connection' and other error messages mean?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#103">Why are there no recent archives in the Wayback Machine?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#134">How does the Wayback Machine behave with Javascript turned off?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#202">How did I end up on the live version of a site? or I clicked on X date, but now I am on Y date, how is that possible?  Why can I only see 930 out of the 2000 results?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#271">Where does the name come from?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#276">For more information...</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#265">How do I cite Wayback Machine urls in MLA format?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#3">What is the Archive-It service of the Internet Archive Wayback Machine?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#1"><a name='includeMe'></a>What is the Wayback Machine?  How can I get my site included in the Wayback Machine?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#15">How can I help the Internet Archive and the Wayback Machine?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#23">Do you collect all the sites on the Web?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#25">Who has access to the collections? What about the public?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#274">How can I get pages authenticated from the Wayback Machine?  How can use the pages in court?</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="The_Wayback_Machine"></a>
          The Wayback Machine        </h1>
                <p>
          <b>
            <a name="18"></a>
            Where is the rest of the archived site?  Why am I getting broken or gray images on a site?          </b>
                  </p>
        <p>
          Broken images (when there is a small red "x" where the image should be) occur when the images are not available on our servers.  Usually this means that we did not archive them.  Gray images are the result of robots.txt exclusions.  The site in question may have blocked robot access to their images directory.
<p>
You can tell if the link you are looking for is in the Wayback Machine by entering the url into the Wayback Machine search box at archive.org (http://www.archive.org/web/web.php ).  Whatever archives we have are viewable in the Wayback Machine. 
<p>
The archived webpages are meant to be a "snap shot" of past Internet sites.  Please note that while we try to archive an entire site, this is not always possible.  That is why some images or links might be missing.  Additionally some sites do not archive well and we cannot fix that.  There is a list of common problems that make a site difficult to archive: http://www.archive.org/about/faqs.php#12. 
<p>
If you see a box with a red X or a broken image icon that means that we unfortunately do not have the images.   Files over 10MB are not archived in this "snap shot" of the website.  
<p>
The best way to see all the files we have archived of the site is:
http://web.archive.org/*/www.yoursite.com/*
<p>
Please note that there is a 6 - 14 month lag time between the date a site is crawled and the date it appears in the Wayback Machine.
<p><p>        </p>
                <p>
          <b>
            <a name="4"></a>
            Can I link to old pages on the Wayback Machine?          </b>
                  </p>
        <p>
          Yes!  The Wayback Machine is built so that it can be used and referenced.  If you find an archived page that you would like to reference on your Web page or in an article, you can copy the URL.  You can even use fuzzy URL matching and date specification... but that's a bit more advanced.        </p>
                <p>
          <b>
            <a name="5"></a>
            Why isn't the site I'm looking for in the archive?          </b>
                  </p>
        <p>
          Some sites may not be included because the automated crawlers were unaware of their existence at the time of the crawl.  It's also possible that some sites were not archived because they were password protected, blocked by robots.txt, or otherwise inaccessible to our automated systems.  Siteowners might have also requested that their sites be excluded from the Wayback Machine.  When this has occurred, you will see a "blocked site error" message. When a site is excluded because of robots.txt you will see a "robots.txt query exclusion error" message.        </p>
                <p>
          <b>
            <a name="6"></a>
            What does it mean when a site's archive data has been "updated"?          </b>
                  </p>
        <p>
          When our automated systems crawl the web every few months or so, we find that only about 50% of all pages on the web have changed from our previous visit.  This means that much of the content in our archive is duplicate material.  If you don't see ""*"" next to an archived document, then the content on the archived page is identical to the previously archived copy.        </p>
                <p>
          <b>
            <a name="7"></a>
            Who was involved in the creation of the Internet Archive Wayback Machine?          </b>
                  </p>
        <p>
          "The original idea for the Internet Archive Wayback Machine began in 1996, when the Internet Archive first began archiving the web.  Now, five years later, with over 100 terabytes and a dozen web crawls completed, the Internet Archive has made the Internet Archive Wayback Machine available to the public.  The Internet Archive has relied on donations of web crawls, technology, and expertise from Alexa Internet and others.  The Internet Archive Wayback Machine is owned and operated by the Internet Archive."        </p>
                <p>
          <b>
            <a name="8"></a>
            How was the Wayback Machine made?          </b>
                  </p>
        <p>
          Alexa Internet, in cooperation with the Internet Archive, has designed a three dimensional index that allows browsing of web documents over multiple time periods, and turned this unique feature into the Wayback Machine.
        </p>
                <p>
          <b>
            <a name="9"></a>
            How large is the Wayback Machine?          </b>
                  </p>
        <p>
          The Internet Archive Wayback Machine contains almost 2 petabytes of data and is currently growing at a rate of 20 terabytes per month.  This eclipses the amount of text contained in the world's largest libraries, including the Library of Congress.        </p>
                <p>
          <b>
            <a name="11"></a>
            How do you archive dynamic pages?          </b>
                  </p>
        <p>
          There are many different kinds of dynamic pages, some of which are easily stored in an archive and some of which fall apart completely. When a dynamic page renders standard html, the archive works beautifully. When a dynamic page contains forms, JavaScript, or other elements that require interaction with the originating host, the archive will not contain the original site's functionality.        </p>
                <p>
          <b>
            <a name="10"></a>
            What type of machinery is used in this Internet Archive?          </b>
                  </p>
        <p>
          Much of the Internet Archive is stored on hundreds of slightly modified x86 servers. The computers run on the Linux operating system. Each computer has 512Mb of memory and can hold just over 1 Terabyte of data on ATA disks. However we are developing a new way of storing our data on a smaller machine.  Each machine will store 1 terabyte. For more information go to <a href="https://www.petabox.org">www.petabox.org</a>. 
        </p>
                <p>
          <b>
            <a name="14"></a>
            Some sites are not available because of robots.txt or other exclusions. What does that mean?          </b>
                  </p>
        <p>
          The Internet Archive follows the <a
href="http://www2.sims.berkeley.edu/research/conferences/aps/removal-policy.html">Oakland Archive Policy</a> for Managing Removal Requests And Preserving Archival Integrity
<p>
The Standard for Robot Exclusion (SRE) is a means by which web site owners can instruct automated systems not to crawl their sites. Web site owners can specify files or directories that are disallowed from a crawl, and they can even create specific rules for different automated crawlers. All of this information is contained in a file called robots.txt. While robots.txt has been adopted as the universal standard for robot exclusion, compliance with robots.txt is strictly voluntary. In fact most web sites do not have a robots.txt file, and many web crawlers are not programmed to obey the instructions anyway. However, Alexa Internet, the company that crawls the web for the Internet Archive, does respect robots.txt instructions, and even does so retroactively. If a web site owner decides he / she prefers not to have a web crawler visiting his / her files and sets up robots.txt on the site, the Alexa crawlers will stop visiting those files and will make unavailable all files previously gathered from that site. This means that sometimes, while using the Internet Archive Wayback Machine, you may find a site that is unavailable due to robots.txt (you will see a "robots.txt query exclusion error" message).  Sometimes a web site owner will contact us directly and ask us to stop crawling or archiving a site, and we endeavor to comply with these requests. When you come accross a "blocked site error" message, that means that a siteowner has made such a request and it has been honored.
<p>
Currently there is no way to exclude only a portion of a site, or to exclude archiving a site for a particular time period only.  
<p>
<p>When a URL has been excluded at direct owner request from being archived, that exclusion is retroactive and permanent.          </p>
                <p>
          <b>
            <a name="16"></a>
            Can I search the Archive?          </b>
                  </p>
        <p>
          Using the Internet Archive Wayback Machine, it is possible to search for the names of sites contained in the Archive (URLs) and to specify date ranges for your search.  We hope to implement a full text search engine at some point in the future.         </p>
                <p>
          <b>
            <a name="2"></a>
            How can I have my site's pages excluded from the Wayback Machine?          </b>
                  </p>
        <p>
          You can exclude your site from display in the Wayback Machine by placing a simple robots.txt file on your Web server.</p>

<p class="bodycontent">  Here are <a href="/about/exclude.php">directions on how to automatically exclude your site</a>.  If you cannot place the robots.txt file, opt not to, or have further questions, email us at info@archive.org.  
<br><br>
If you are emailing to ask that your website not be archived, please note that you'll need to include the url (web address) in the text of your message.  
        </p>
                <p>
          <b>
            <a name="12"></a>
            Why are some sites harder to archive than others?          </b>
                  </p>
        <p>
          If you look at our collection of archived sites, you will find some broken pages, missing graphics, and some sites that aren't archived at all. Here are some things that make it difficult to archive a web site: <ul><li>Robots.txt   -- We respect robot exclusion headers. </li><li>Javascript   -- Javascript elements are often hard to archive, but especially if they generate links without   having the full name in the page. Plus, if javascript needs to contact the originating   server in order to work, it will fail when archived. </li><li>Server   side image maps -- Like any functionality   on the web, if it needs to contact the originating   server in order to work, it will fail when   archived. </li><li>Unknown   sites -- The archive contains crawls of the   Web completed by Alexa Internet. If Alexa doesn't know about your site, it won't be   archived. Use the Alexa Toolbar (available   at <a href=http://www.alexa.com>www.alexa.com</a>),   and it will know about your page. Or you can   visit Alexa's Archive Your Site page at <a href=http://pages.alexa.com/help/webmasters/index.html#crawl_site>http://pages.alexa.com/help/webmasters/index.html#crawl_site</a>.</li><li>Orphan   pages -- If there are no links to your pages,   the robot won't find it (the robots don't enter queries in search boxes.) </li>  </ul>  As a general rule of thumb, simple html is the easiest to archive.        </p>
                <p>
          <b>
            <a name="29"></a>
            How do you protect my privacy if you archive my site?          </b>
                  </p>
        <p>
          The Archive collects Web pages that are publicly available the same ones that you might find as you surfed around the Web. We do not archive pages that require a password to access, pages tagged for "robot exclusion" by their owners, pages that are only accessible when a person types into and sends a form, or pages on secure servers. We also provide information on removing a site from the collections. Those who use the collections must agree to certain terms of use.<BR><BR>Like a public library, the Archive provides free and open access to its collections to researchers, historians, and scholars. Our cultural norms have long promoted access to documents that were, but no longer are, publicly accessible.<BR><BR>Given the rate at which the Internet is changing the average life of a Web page is only 77 days if no effort is made to preserve it, it will be entirely and irretrievably lost. Rather than let this moment slip by, we are proceeding with documenting the growth and content of the Internet, using libraries as our model.<BR><BR>If you are interested in these issues, please join and contribute to our <a href="/about/contact.php#lists">announcement and discussion lists</a>.        </p>
                <p>
          <b>
            <a name="19"></a>
            How do I contact the Internet Archive?          </b>
                  </p>
        <p>
          All questions about the Wayback Machine, or other Internet Archive projects, should be addressed to info at archive dot org.        </p>
                <p>
          <b>
            <a name="21"></a>
            Why is the Internet Archive collecting sites from the Internet? What makes the information useful?          </b>
                  </p>
        <p>
          Most societies place importance on preserving artifacts of their culture and heritage. Without such artifacts, civilization has no memory and no mechanism to learn from its successes and failures. Our culture now produces more and more artifacts in digital form. The Archive's mission is to help preserve those artifacts and create an Internet library for researchers, historians, and scholars. The Archive collaborates with institutions including the <a href="../internet/index.html#Lobby_Sculpture">Library of Congress</a> and the <a href="../internet/index.html#Smithsonian">Smithsonian</a>.        </p>
                <p>
          <b>
            <a name="22"></a>
            Do you archive email? Chat?          </b>
                  </p>
        <p>
          No, we do not collect or archive chat systems or personal email messages that have not been posted to Usenet bulletin boards or publicly accessible online message boards.        </p>
                <p>
          <b>
            <a name="26"></a>
            How can I get a copy of the pages on my Web site? If my site got hacked or damaged, could I get a backup from the Archive?'          </b>
                  </p>
        <p>
          Our <a href="/about/terms.php">terms of use</a> do not cover backups for the general public. However, you may use the Internet Archive Wayback Machine to locate and access archived versions of a site to which you own the rights. We can't guarantee that your site has been or will be archived. We can no longer offer the service to pack up sites that have been lost.        </p>
                <p>
          <b>
            <a name="24"></a>
            Is there any personal information in these collections?          </b>
                  </p>
        <p>
          We collect Web pages that are publicly accessible. These may include pages with personal information.        </p>
                <p>
          <b>
            <a name="20"></a>
            What is the Wayback Machine's Copyright Policy?          </b>
                  </p>
        <p>
          The Internet Archive respects the intellectual property rights and other proprietary rights of others. The Internet Archive may, in appropriate circumstances and at its discretion, remove certain content or disable access to content that appears to infringe the copyright or other intellectual property rights of others. If you believe that your copyright has been violated by material available through the Internet Archive, please provide the Internet Archive Copyright Agent with the following information:</p><ul><li class="bodycontent">Identification of the copyrighted work that you claim has been infringed;</li><li class="bodycontent">An exact description of where the material about which you complain is located within the Internet Archive collections;</li><li class="bodycontent">Your address, telephone number, and email address;</li><li class="bodycontent">A statement by you that you have a good-faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law;</li><li class="bodycontent">A statement by you, made under penalty of perjury, that the above information in your notice is accurate and that you are the owner of the copyright interest involved or are authorized to act on behalf of that owner;</li><li class="bodycontent">Your electronic or physical signature.</li></ul><p class="bodycontent">Internet Archive uses the  exclusion policy intended for use by both academic and non-academic digital repositories and archivists. See our <a href="http://www.sims.berkeley.edu/research/conferences/aps/removal-policy.html">full exclusion policy</a>.</p><p class="bodycontent">The Internet Archive Copyright Agent can be reached as follows:</p><p class="bodycontent">Internet Archive Copyright Agent<br>Internet Archive<br>300 Funston Ave.<br>San Francisco, CA 94118<br>Phone: 415-561-6767<br>Email: info at archive dot org        </p>
                <p>
          <b>
            <a name="28"></a>
            Can people download sites from the Wayback?          </b>
                  </p>
        <p>
          Our <a href="/about/terms.php">terms of use</a> specify that users of the Wayback Machine are not to copy data from the collection.
        </p>
                <p>
          <b>
            <a name="201"></a>
            What does 'failed connection' and other error messages mean?          </b>
                  </p>
        <p>
          Below is a list of the main error messages you will see while searching the Wayback Machine.  If you see an error message that does not have the Internet Archive Wayback Machine logo in the upper left corner, you are most likely looking at an archived page or the live web. 
<p>
Failed Connection:  The server that the particular piece of information lives on is down.  Generally these clear up within two weeks. </p>
<p>
Robots.txt Query Exclusion: A robots.txt is something that a site owner puts on their site that keeps crawlers like our own from crawling them.  The Internet Archive retroactively respects all robots.txt. </p>
<p>
Blocked Site Error:  Site owners, copyright holders and others who fit Internet Archive's exclusion policy have requested that the site be excluded from the Wayback Machine.  For exclusion criteria, please see our <a href="http://www.sims.berkeley.edu/research/conferences/aps/removal-policy.html">exclusion policy</a> (we use the same one used and developed by other digital repositories and archivists both academic and non-academic). </p>
<p>
Path Index Error: A path index error message refers to a problem in our database wherein the information requested is not available (generally because of a machine or software issue, however each case can be different).  We cannot always completely fix these errors in a timely manner. <p>
Not in Archive:  Generally this means that the site archived has a redirect on it and the site you are redirected to is not in the archive or cannot be found on the live web.          </p>
                <p>
          <b>
            <a name="103"></a>
            Why are there no recent archives in the Wayback Machine?          </b>
                  </p>
        <p>
          It generally takes 6 months or more (up to 24 months) for pages to appear in the Wayback Machine after they are collected, because of delays in transferring material to long-term storage and indexing, or the requirements of our collection partners. 

<p>In some cases, crawled content from certain projects can appear in a much shorter timeframe &mdash; as little as a few weeks from when it was crawled. Older material for the same pages and sites may still appear separately, months later.

<p>There is no access to files before they appear in the Wayback Machine.        </p>
                <p>
          <b>
            <a name="134"></a>
            How does the Wayback Machine behave with Javascript turned off?          </b>
                  </p>
        <p>
          If you have Javascript turned off, images and links will be from the live web, not from our archive of old Web files.        </p>
                <p>
          <b>
            <a name="202"></a>
            How did I end up on the live version of a site? or I clicked on X date, but now I am on Y date, how is that possible?  Why can I only see 930 out of the 2000 results?          </b>
                  </p>
        <p>
          <p><b>How did I end up on the live version of a site? or I clicked on X date, but now I am on Y date, how is that possible?</b></p>

<p>Not every date for every site archived is 100% complete. When you are surfing an incomplete archived site the Wayback Machine will grab the closest available date to the one you are in for the links that are missing. In the event that we do not have the link archived at all, the Wayback Machine will look for the link on the live web and grab it if available. Pay attention to the date code embedded in the archived url. This is the list of numbers in the middle; it translates as yyyymmddhhmmss. For example in this url http://web.archive.org/web/20000229123340/http://www.yahoo.com/ the date the site was crawled was Feb 29, 2000 at 12:33 and 40 seconds. </p>

<p>You can see a listing of the dates of the specific URL by replacing the date code with an asterisk (*), ie: http://web.archive.org/*/www.yoursite.com</p>

<p>Whatever archives we have are viewable in the Wayback Machine. Please note that there is a 6 - 14 month lag time between the date a site is crawled and the date it appears in the Wayback Machine. </p>

<p><b>Why can I only see 930 out of the 2000 results?</b></p>

<p>The list of results displayed shows the total number of pages we have for a given domain name.  This includes numerous repeats as we return to sites to recrawl their content.  The reported results is this total; whereas the smaller number relates to the number of unique results only. </p>


        </p>
                <p>
          <b>
            <a name="271"></a>
            Where does the name come from?          </b>
                  </p>
        <p>
          The Wayback Machine is named in reference to the famous Mr. Peabody's WABAC (pronounced way-back) machine from the Rocky and Bullwinkle cartoon show.          </p>
                <p>
          <b>
            <a name="276"></a>
            For more information...          </b>
                  </p>
        <p>
          Check out our <a href="http://www.archive.org/iathreads/forum-display.php?forum=web">Wayback Machine Forum</a>        </p>
                <p>
          <b>
            <a name="265"></a>
            How do I cite Wayback Machine urls in MLA format?          </b>
                  </p>
        <p>
          This question is a newer one.  We asked MLA to help us with how to cite an archived URL in correct format.  They did say that there is no established format for resources like the Wayback Machine, but it's best to err on the side of more information.  You should cite the webpage as you would normally, and then give the Wayback Machine information.  They provided the following example:

McDonald, R. C. "Basic Canary Care." _Robirda Online_. 12 Sept. 2004. 18
Dec. 2006 [http://www.robirda.com/cancare.html]. _Internet Archive_. [
http://web.archive.org/web/20041009202820/http://www.robirda.com/cancare.html].

They added that if the date that the information was updated is missing, one can use the closest date in the Wayback Machine.  Then comes the date when the page is retrieved and the original URL.  Neither URL should be underlined in the bibliography itself.  

Thanks MLA!        </p>
                <p>
          <b>
            <a name="3"></a>
            What is the Archive-It service of the Internet Archive Wayback Machine?          </b>
                  </p>
        <p>
          <p>For information on the <a href=https://www.archive-it.org><b>Archive-It</b></a> subscription service that allows institutions to build and preserve collections of born digital content, see <a href=https://www.archive.org/about/faqs.php#Archive-It><b>https://www.archive.org/about/faqs.php#Archive-It</b></a>

        </p>
                <p>
          <b>
            <a name="1"></a>
            <a name='includeMe'></a>What is the Wayback Machine?  How can I get my site included in the Wayback Machine?          </b>
                  </p>
        <p>
          <p>The <b>Internet Archive 
<a href=https://www.archive.org/web/web.php>Wayback Machine</a></b> is a service that allows people to visit archived versions of Web sites. Visitors to the Wayback Machine can type in a URL, select a date range, and then begin surfing on an archived version of the Web. Imagine surfing circa 1999 and looking at all the Y2K hype, or revisiting an older version of your favorite Web site. The Internet Archive Wayback Machine can make all of this possible.

<p><b>How can I get my site included in the Wayback Machine?</b>

<p>Much of our archived web data comes from our own crawls or from Alexa Internet's crawls.  Neither organization has a "crawl my site now!" submission process.  Internet Archive's crawls tend to find sites that are well linked from other sites.  The best way to ensure that we find your web site is to make sure it is included in online directories and that similar/related sites link to you.

<p>Alexa Internet uses its own methods to discover sites to crawl.  It may be helpful to install the free Alexa toolbar and visit the site you want crawled to make sure they know about it.

<P>Regardless of who is crawling the site, you should ensure that your site's 'robots.txt' rules and in-page META robots directives do not tell crawlers to avoid your site. 

<P>When a site is crawled, there is usually at least a 6-month lag, and sometimes as much as a 24-month lag, between the date that web pages are crawled and when they appear in the Wayback Machine. 

<p>In some cases, crawled content from certain projects may appear in a much shorter timeframe â€” as little as a few weeks from when it was crawled. Older material for the same pages and sites may still appear separately, months later. 

<p><b>Yahoo is closing Geocities.  Now what?</b>

<p>The Internet Archive has set up this page to help people submit Geocities sites for preservation:  <a href=https://www.archive.org/web/geocities.php>https://www.archive.org/web/geocities.php</a>

<p>Yahoo also provides this information: <a href=http://help.yahoo.com/l/us/yahoo/geocities/close/>http://help.yahoo.com/l/us/yahoo/geocities/close/</a>

        </p>
                <p>
          <b>
            <a name="15"></a>
            How can I help the Internet Archive and the Wayback Machine?          </b>
                  </p>
        <p>
          The Internet Archive actively seeks donations of digital materials for preservation. If you have digital materials that may be of interest to future generations, please let us know by sending an email to info at archive dot org. The Internet Archive is also seeking additional funding to continue this important mission. You can click the donate tab above or click <a href="https://www.archive.org/donate/">here</a>.  Thank you for considering us in your charitable giving.        </p>
                <p>
          <b>
            <a name="23"></a>
            Do you collect all the sites on the Web?          </b>
                  </p>
        <p>
          No, we collect only publicly accessible Web pages. We do not archive pages that require a password to access, pages tagged for "robot exclusion" by their owners, pages that are only accessible when a person types into and sends a form, or pages on secure servers. If a site owner properly requests removal of a Web site through <a href="https://www.archive.org/about/exclude.php">https://www.archive.org/about/exclude.php</a>, we will exclude that site from the Wayback Machine.        </p>
                <p>
          <b>
            <a name="25"></a>
            Who has access to the collections? What about the public?          </b>
                  </p>
        <p>
          Anyone can access our collections through our website archive.org.  The web archive can be searched using the <a href="https://www.archive.org/web/web.php">Wayback Machine</a>. <p>

The Archive makes the collections available at no cost to researchers, historians, and scholars. At present, it takes someone with a certain level of <a href="../web/researcher/intended_users.php">technical knowledge</a> to access collections in a way other than our website, but there is no requirement that a user be affiliated with any particular organization.        </p>
                <p>
          <b>
            <a name="274"></a>
            How can I get pages authenticated from the Wayback Machine?  How can use the pages in court?          </b>
                  </p>
        <p>
          The Wayback Machine tool was not designed for legal use.  We do have a legal request policy found at <a href=https://www.archive.org/legal>our legal page</a>.  Please read through the entire policy before contacting us with your questions.  We do have a <a href=https://www.archive.org/legal/affidavit.php>standard affidavit</a> as well as a <a href=https://www.archive.org/legal/faq.php>FAQ section for lawyers</a>.  We would prefer that before you contact us for such services, you see if the other side will stipulate instead.  We do not have an in-house legal staff, so this service takes away from our normal duties.  Once you have read through our policy, if you still have questions, please <a href=https://www.archive.org/about/contact.php>contact us</a> for more information.          </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#233">How can I add a thumbnail image to my item's details page?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#173">How can I get iTunes to create a new playlist when I stream MP3s?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#174">How can I play OGG files on a Mac?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#206">I'm having trouble with a 'blank'/corrupted ZIP file. What do I do?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#232">How can I add a logo to the upper right corner of my collection?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#272">How can I get my tracks to show up in the right order?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#236">What kind of audio file should I submit?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#238">The flash player is covering my files!  How do I move it?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#281">For more information...</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="Audio"></a>
          Audio        </h1>
                <p>
          <b>
            <a name="233"></a>
            How can I add a thumbnail image to my item's details page?          </b>
                  </p>
        <p>
          <p>First, make sure you're logged on to archive.org with the same email address you used to upload the item. </p> 

<p>The image you upload must be named <i>identifier</i>.jpg (where <i>identifier</i> is your item's identifier name) and you must choose file format JPEG in the metadata editor.</p>

<p>To upload the image:
<ul>
<li>Go to your item's details page</li>
<li>Click the "Edit item" link in the lower left box</li>
<li>Upload the .jpg</li>
<li>After a few minutes, return to your item's details page.  Click "Edit item" and find the .jpg file you just uploaded in the list of files near the bottom of this page.  Select the file format JPEG from the drop down menu, and click the submit button.</li>
<li>Wait 5-20 minutes for your changes to show up.  If you're still not seeing your new file, please try clearing your cache and viewing the page again, since you may still be looking at an old version of the page.</li>
</ul>
</p>
<p></p>        </p>
                <p>
          <b>
            <a name="173"></a>
            How can I get iTunes to create a new playlist when I stream MP3s?          </b>
                  </p>
        <p>
          <p>As an iTunes user, you might have noticed that iTunes loads the Archive's streaming MP3s (M3U files) into your library, and subsequentially the files get shuffled and are out of order.  We have come up with a solution to this problem.</p>

<p>Step by step instructions:</p>

<ul>
<li>Download this <a href="/audio/m3uPlayer.dmg">AppleScript application</a>.</li>
<li>Copy the m3uPlayer application to a permanent location</li>
<li>Choose some recording in the Archive to stream.  This will cause an M3U to download to your default download folder (typically your desktop).</li>
<li>Click on the downloaded M3U file, hit option-I (or option-click and select Get Info).  Change "open with" from ITunes to m3uPlayer (locate it wherever you saved it)</li>
<li>Click change all so that all future M3U files will open this way</li>
</ul>
<p>That's it!  If you have trouble, post a message to this <A onclick="javascript:window.opener.location=this.href; return false;" href="/audio/collection.php?collection=opensource_audio#forum">forum</a></p>

<p>Thanks to <a href="http://www.balnaves.com/archives/000092.php">http://www.balnaves.com/archives/000092.php</a> for the code, instructions, and inspiration</p>        </p>
                <p>
          <b>
            <a name="174"></a>
            How can I play OGG files on a Mac?          </b>
                  </p>
        <p>
          On the mac, there is a free component to ogg-ify itunes.
The freeware VLC Media Player will also play OGG files.
<a href='http://www.macosxhints.com/article.php?story=20020424233612407'>http://www.macosxhints.com/article.php?story=20020424233612407</a>

        </p>
                <p>
          <b>
            <a name="206"></a>
            I'm having trouble with a 'blank'/corrupted ZIP file. What do I do?          </b>
                  </p>
        <p>
          There are a variety of problems that may be causing this. Here are a couple of the most common.

If you have a Mac running OS X, the default unzip utility (Stuffit) does not deal well with those Archive ZIP files that are 'compressed on the fly'. You may see an empty directory - if so, then try downloading <a href="http://homepage.mac.com/roger_jolly/software/">Zip Tools for Mac OS X</a> and using the drag and drop software within that to unzip your download. [Make sure you save your download to your desktop before trying things on it.]

If you're having any trouble with downloads timing out or being incomplete, especially on Windows, then you may be able to use download managers such as <a href="http://www.getright.com/">GetRight</a>. These will restart your download if it fails. However, some 'ZIP on the fly' downloads don't play well with download managers. If you find that to be the case, the safest thing to do is to download each track individually in a download manager.        </p>
                <p>
          <b>
            <a name="232"></a>
            How can I add a logo to the upper right corner of my collection?          </b>
                  </p>
        <p>
          First, make sure you're logged on to archive.org with the same email address you used when you created your collection. 

Note: Images should have a height of no more that 72 pixels.

<ul>
<li>Go to your collection's front page</li>
<li>Click the "Edit Item!" link next to your user name.</li>
<li>Click "Item Manager" near the top of the page.</li>
<li>Click the "checkout --edit items files (non XML)" button in the "Edit Operations" section of the form.</li>
<li>In Step 1 of 2, click the "Share" button.</li>
<li>Locate and select the image to be uploaded and click "Select".</li>
<li>In Step 2 of 2 click the "Update Item!" button</li>
<li>Return to collection front page and click "edit" link again</li>
<li>Find logo file at bottom of page, choose "Collection Header" from the drop down list and click submit.</li>
</ul>

It might take a few minutes for the changes to appear.        </p>
                <p>
          <b>
            <a name="272"></a>
            How can I get my tracks to show up in the right order?          </b>
                  </p>
        <p>
          <p>The most reliable way to have your tracks appear on the page in the correct order is to name the individual files with track numbers, like this:<br>
01_nameoffirstsong.mp3<br>
02_nameofsecondsong.mp3<br>
03_nameofthirdsong.mp3<br>
</p>

<p>(If you have more than 9 files you need to start numbering with 01 - not 1 - otherwise the files will go in this order: 1, 10, 11, 12, 2, 3 etc.)</p>

<p>If you have already created an item and you would like to change the file names to rearrange them correctly, do the following:
<ol>
<li>Click the "Edit Item!" link</li>
<li>Rename your original files using track numbers</li>
<li>Delete all "derived" files, leaving only your original files and the .xml files</li>
<li>Click "Edit item" > "Item Manager" and then click the "derive" button</li>
</ol>
</p>

<p>It will take a little while for the derive to finish running, but once it does you'll have all new files, in the correct order, in both the flash player and the page itself.</p>        </p>
                <p>
          <b>
            <a name="236"></a>
            What kind of audio file should I submit?          </b>
                  </p>
        <p>
          <p>The archive is all about free access to information, so you should submit file formats that are easily downloadable and/or streamable for other site patrons.</p>

<p>We prefer that you submit the highest quality file that you have available, and then we will attempt to create smaller file sizes and formats automatically with our deriver program.  We recommend that you do not attempt to do any special encoding of your files - the more settings you mess around with, the less likely our deriver code will be able to process the file.</p>

<p>If you are submitting a Live Music Archive item, please only submit Flac or Shorten files.  Even for non-LMA items, these are the best formats to use.</p>

<p>Whatever format you choose, please upload each file to your item individually (you can submit multiple files per item), in a non-compressed format. Uploading content in a .zip or .rar file makes your item unstreamable and significantly less accessible to others.  If you upload .zip, .rar, non-audio formats (like .exe), or password-protected files, they may be removed by our moderators.
</p>

<p>The table below describes what file formats we will attempt to derive depending on what type of file you submit.
</p>

<iframe style="width:700px; height:1000px; border:0px"
        src="/help/derivatives.php?mediatype=audio">
</iframe>

        </p>
                <p>
          <b>
            <a name="238"></a>
            The flash player is covering my files!  How do I move it?          </b>
                  </p>
        <p>
          If an item has little or no description, sometimes the flash player doesn't have enough room in the top portion of the page and covers the files below.

If you don't want to add a description (which would be nice, so that people know what they're listening to), you can add extra space in the description field using paragraph tags. 

<ul>
<li>Click the "Edit item" link in the lower left box</li>
<li>Add several paragraph tags to the description field, like this:<br>
&lt;p&gt;<br>
&lt;p&gt;<br>
&lt;p&gt;<br>
&lt;p&gt;<br>
</li>
<li>Click the submit button</li>
</ul>

After 10-20 minutes, when you return to your item you should see that the files have moved down further on the page, allowing the flash player enough room at the top.  Usually 4-5 &lt;p&gt; tags is enough.

        </p>
                <p>
          <b>
            <a name="281"></a>
            For more information...          </b>
                  </p>
        <p>
          Check out our <a href="http://www.archive.org/iathreads/forum-display.php?forum=audio">Audio Forum</a>        </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#1007">Does the Internet Archive release transparency reports about law enforcement requests?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#1005">Does the Internet Archive have general guidelines for how it treats requests for non-public information about users from law enforcement?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#1006">Does the Internet Archive take a public stance on bulk surveillance by governments?</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="Law_Enforcement_Requests"></a>
          Law Enforcement Requests        </h1>
                <p>
          <b>
            <a name="1007"></a>
            Does the Internet Archive release transparency reports about law enforcement requests?          </b>
                  </p>
        <p>
          Yes! Starting with the report below:  
<br>
<center><img src="https://archive.org/download/ialerequestsummary2013/transparency.png" alt="transparency.png" /></center>
        </p>
                <p>
          <b>
            <a name="1005"></a>
            Does the Internet Archive have general guidelines for how it treats requests for non-public information about users from law enforcement?          </b>
                  </p>
        <p>
          The Internet Archive requires appropriate legal process (i.e., subpoena, court order, or other valid process) before disclosing non-public user account information.  
<p>
The Internet Archive requires a search warrant before disclosing to law enforcement the contents of non-public user communications.  
<p>
The Internet Archive attempts to notify users about criminal subpoenas or other formal requests seeking their non-public data unless prohibited by law or if doing so would be futile or ineffective.
        </p>
                <p>
          <b>
            <a name="1006"></a>
            Does the Internet Archive take a public stance on bulk surveillance by governments?          </b>
                  </p>
        <p>
          Our position is that governments should limit surveillance to specific, known users for lawful purposes and not undertake bulk collection of non-public communications data.        </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#211">A recording I uploaded and marked 'no lossy formats' had them created (mp3, ogg, m3u, etc...) .  How can I remove them?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#209">Can I upload live recordings that were broadcast on XM Radio or Sirius Satellite Radio?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#240">What is the Live Music Archive all about?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#117">Can I upload concert videos?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#253">What are MD5 files?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#252">What are FLAC files and how can I listen to them?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#254">What are FFP files?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#208">There's no setlist for this show -  OR - The setlist does not match up with the number of files.  Should I submit an error report?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#255">How do I burn FLAC files to CD as audio tracks?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#256">How do I burn SHN files to CD as audio tracks?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#304">How can I add a logo to the upper right corner of my collection?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#115">I'm an artist who would like to be included in the Archive, what do I need to do?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#221">The progress of my upload says 'File metadata XML invalid.  Waiting for user to correct.'  How can I fix this?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#120">I have more Live Music Archive questions...who do I ask?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#121">I have a different source for a show that is already in the archive, should I upload it anyway?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#122">How can I help get bands into the Live Music Archive?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#219">When I download concerts, I constantly get disconnected before the download completes.  What can I do to fix this?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#128">What are the WAV MD5 files that are sometimes in filesets?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#129">I just uploaded a directory that contained WAV MD5 checksums, is that OK?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#220">My failure email is indicating that the text file failed.  What can I do?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#131">Can bands place restrictions on material to be archived?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#133">I just uploaded a show and all the files fail the MD5 check, what's the deal?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#136">Where have all the Dave Matthews Band concerts gone? Will they be back? </a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#137">Why is there no Phish?  What about Widespread Panic?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#138">I used to use a download manager and now it stopped working. What's the deal? </a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#155">What's the deal with magic number errors?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#156">Do you provide an RSS feed of new updates to the LMA?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#163">What does the 'Transferred by' field mean?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#167">Why don't I get an email when my uploads fail MD5 checksums?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#250">Can I log into an FTP server to download concerts?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#193">My in-progress upload says ' No metadata describing files found. Waiting for user to enter metadata' - what do I do?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#210">The Grateful Dead is here, when will we see Jerry Garcia recordings?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#214">Regarding removing the lossy files ... I edited my show, checked the box to remove them and clicked update.  Now when I click update again, the box is still not checked. Why?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#229">The upload instructions require a 'FLAC Fingerprint' file with my recording - how can I create this?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#225">I've got a great 'filler' for the recording I am about to upload to the collection - should I include it?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#226">Where can I find other recordings by [trade-friendly band] that aren't in the collection?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#270">I tried downloading a show and I got a '403 Forbidden' page. Why?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#241">How do I upload a show to the LMA?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#242">How do I make corrections to shows?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#243">What file formats are accepted for contributions to the Live Music Archive?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#244">I like adding concerts.  Do you have a preference on the way I put in information?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#245">About Grateful Dead concerts on the Archive</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#248">What are the options for streaming a full recording?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#249">What are the options for downloading a full recording?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#268">Where can I see the rest of the 'Most Downloaded Items' in the Live Music Archive?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#269">Where can I see the rest of the 'Top Batting Averages' of shows in the Live Music Archive?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#282">For more information...</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#307">How can I add a logo to the upper right corner of my collection?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#310">How are download counts calculated?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#257">Why are there no shows by band X?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#101">What is the status of band X for the Archive?</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="Live_Music_Archive"></a>
          Live Music Archive        </h1>
                <p>
          <b>
            <a name="211"></a>
            A recording I uploaded and marked 'no lossy formats' had them created (mp3, ogg, m3u, etc...) .  How can I remove them?          </b>
                  </p>
        <p>
          If you come across this situation and you are the uploader, click [edit], select the derivation option you prefer, and then 'Update'.  You should see the message "Format Options Updated Successfully".  Within 10 minutes the system will create a "_rules.conf" file in the recording's folder.  Then, the next time the system performs an automatic sweep looking for changes, it will notice the new rules file and remove the lossy files automatically.  The sweep occurs approximately twice a day, so you should see the files removed within 12-24 hours. <br><br> If you are not the uploader, send us an email (etree at this domain) and an admin will remove them.        </p>
                <p>
          <b>
            <a name="209"></a>
            Can I upload live recordings that were broadcast on XM Radio or Sirius Satellite Radio?          </b>
                  </p>
        <p>
          At this point in time, Archive.org cannot host recordings that were broadcast over either of these services.  Subscribers have informed us that they were required to sign a "Terms of Use" document that forbids the recording/hosting/rebroadcasting of any material received from these services.  Until we hear otherwise, these recordings cannot be hosted here.        </p>
                <p>
          <b>
            <a name="240"></a>
            What is the Live Music Archive all about?          </b>
                  </p>
        <p>
          This audio archive is an online public library of live recordings available for royalty-free, no-cost public downloads. We only host material by <a href= "http://wiki.etree.org/index.php?page=TradeFriendly">trade-friendly</a> artists: those who like the idea of noncommercial distribution of some or all of their live material. Live recordings are a part of our culture and might be lost in 100 years if they're not archived. We think music matters and want to preserve it for future generations.
<br><br>
The LMA draws strength from the members of <a href="http://wiki.etree.org">etree.org</a> and other online communities of music fans devoted to providing public access to high-quality digital recordings of tradable performances. Typically, recordings are made by the fans themselves. Recordings are preserved in "Lossless" archival compression formats such as Shorten or FLAC (MP3 is not Lossless) for highest quality preservation.
<br><br>
Patrons may download from the LMA with the understanding that the artists still hold their copyrights. All material is strictly noncommercial, both for access here and for any further distribution.        </p>
                <p>
          <b>
            <a name="117"></a>
            Can I upload concert videos?          </b>
                  </p>
        <p>
          At this time, video uploads are not being accepted, namely because most of the bands archived prohibit the video taping of their shows.  Moreover, unlike audio, where we actually have a shot at archiving the vast majority of any given band's live concerts (in very high quality format), video is scarce and, unless made by the artist (in which case, it's typically for commercial purposes), is not of particularly good quality.        </p>
                <p>
          <b>
            <a name="253"></a>
            What are MD5 files?          </b>
                  </p>
        <p>
          MD5 files contain checksums, strings of characters used to uniquely represent a file.  These checksums enable users to verify that music files downloaded correctly.<BR><BR>

A recommended tool for creating these files is <a href="http://www.md5summer.org/">MD5summer</a>. Please note that before uploading the MD5 created with this tool you should open the MD5 in a text editor and remove the top 3 lines so the first signature is now flush with the top of the file.        </p>
                <p>
          <b>
            <a name="252"></a>
            What are FLAC files and how can I listen to them?          </b>
                  </p>
        <p>
          FLAC stands for free lossless audio codec. It is an open source, lossless compression algorithm for digital music. It compresses music files to 50-60% of their original size, with no loss in quality. More FLAC information can be found on the <a href="http://flac.sourceforge.net/index.html">FLAC sourceforge site</a> and in <a href="http://wiki.etree.org/index.php?page=FLAC">this etree FAQ</a>.
<br><br>
If you upload FLAC filesets to the LMA, please follow the <a href= "http://wiki.etree.org/index.php?page=NamingStandards">naming standards</a> to help the checking program here. Directories should be named with .flac16 or .flac24 suffix, not .flac. Otherwise, the program will report failures.

<p>To listen to FLAC files:</p>
<p>Macintosh: Download and install <a href="http://cogx.org/">Cog</a>, a multi-format audio player.
<br><br>Windows: Download and install <a href="http://www.winamp.com/">WinAmp</a>, a multi-format audio player, and then install the <a href="http://www.mikewren.com/flac/">FLAC Plugin</a> for WinAmp.  If you would like to use FLAC with your Windows Media Player (WMP) download and install the <a href="http://www.illiminable.com/ogg/">Directshow Filters for Ogg Vorbis, Speex, Theora and FLAC</a>. This will allow WMP to not only play .flac files but .ogg files as well.

<br><br>
Linux or any other UNIX-based architecture: <a href="http://sourceforge.net/project/showfiles.php?group_id=13478">Download</a> and copy "libxmms-flac.so" to your <a href="http://www.xmms.org">XMMS media player</a> input plugins folder.        </p>
                <p>
          <b>
            <a name="254"></a>
            What are FFP files?          </b>
                  </p>
        <p>
          FFP files contain checksums, strings of characters used to uniquely represent a FLAC file. These checksums enable users to verify which particular source a file comes from.        </p>
                <p>
          <b>
            <a name="208"></a>
            There's no setlist for this show -  OR - The setlist does not match up with the number of files.  Should I submit an error report?          </b>
                  </p>
        <p>
          There has been an increasing number of shows uploaded to the Live Music collection without setlist information, or the setlist was not properly matched to the files.  When you notice a recording like this, please email us (etree at this domain) <b>only</b> if you have an updated setlist, or you are able to match the files up correctly.
<br><br>
We would prefer that you do not submit error reports letting us know that there is no setlist - tracking down setlists for every concert and matching them up to the recordings is a monumental task that has grown beyond the capabilities of the small group of Archive.org admins.  We would like fans that are familiar with each artist's material to help us with this project - in your email, please give us specific instructions on what changes to make and we will do so.          </p>
                <p>
          <b>
            <a name="255"></a>
            How do I burn FLAC files to CD as audio tracks?          </b>
                  </p>
        <p>
          You will first need to convert the FLAC files to another format that your burning program is familiar with. Windows users can use the <a href="http://www.mikewren.com/flac">FLAC Frontend</a>, to convert FLAC files to WAV files, which are suitable for burning programs. For Macintosh OS X users, Scott Brown has created a tool called <a href="http://www.macupdate.com/info.php/id/14246">xACT</a>.        </p>
                <p>
          <b>
            <a name="256"></a>
            How do I burn SHN files to CD as audio tracks?          </b>
                  </p>
        <p>
          You will first need to convert the SHN files to another format that your burning program is familiar with.  The following programs will convert SHN files to WAV files, which can be burned to a CD.  More resources are listed in <a href="http://research.umbc.edu/~hamilton/shnfaq.html">this FAQ</a>.<br><br>
Macintosh: Download and install Scott Brown's <a href="http://www.macupdate.com/info.php/id/14246">xACT</a>. <br><br> 
Windows: Download and install<a href="http://home.att.net/~mkw/"> Michael K. Weise's</a> tool, <a href="http://etree.org/mkw.html">mkwACT</a>.  Or, another good tool is <a href="http://www.foobar2000.org/">Foobar2000</a> - make sure you get the "Special" version to have Shorten compatibility!<br><br> 
Linux or any other UNIX-based architecture: Download and install <a href="http://www.etree.org/shnutils/shorten/">shorten</a>.        </p>
                <p>
          <b>
            <a name="304"></a>
            How can I add a logo to the upper right corner of my collection?          </b>
                  </p>
        <p>
          First, make sure you're logged on to archive.org with the same email address you used when you created your collection. 

Note: Images should have a height of no more that 72 pixels.

â€¢ Â Go to your collection's front page
â€¢ Â Click the "Edit Item!" link next to your user name.
â€¢ Â Click "Item Manager" near the top of the page.
â€¢ Â Click the "checkout --edit items files (non XML)" button in the "Edit Operations" section of the form.
â€¢ In Step 1 of 2, click the "Share" button.
â€¢ Â Locate and select the image to be uploaded and click "Select".
â€¢ Â In Step 2 of 2 click the "Update Item!" button
â€¢ Â Return to collection front page and click "edit" link again
â€¢ Â Find logo file at bottom of page, choose "Collection Header" from the drop down list and click submit.

It might take a few minutes for the changes to appear.        </p>
                <p>
          <b>
            <a name="115"></a>
            I'm an artist who would like to be included in the Archive, what do I need to do?          </b>
                  </p>
        <p>
          We'd love to have you!  Just write to us at etree at archive dot org in English giving some kind of permission for us to archive your shows for public download and noncommercial, royalty-free circulation.  It does not need to be a formally worded declaration, and can come from anyone you feel has the "say-so."  We just need to be clear on how you feel about the project.  We will put relevant quotes onto a new "collection" page (<a href= "http://www.archive.org/browse.php?mediatype=collection&collection=etree&field=%2Fmetadata%2Fcreator">examples</a>) for your performances, along with a link to your official website.
<br><br>It is necessary for you to <u>email us</u> at etree at archive dot org in order to create a new section. We want to be sure that the go-ahead really is coming from you. Please do not attempt to create your own collection, or to upload any of the band's shows, in advance of receiving an emailed confirmation message from curators; such attempts may significantly complicate or delay the curators' setup process.
<br><br>
You can give as much or as little scope for archiving as you like. Some bands place <a href= "http://www.archive.org/about/faqs.php#131">limits</a> on what can be hosted, and we can accomodate those.  Archive Curators, volunteer fans who have proven to be in line with the spirit of this archive, will attempt to screen contributions for OK'ed material only.
<br><br>
At the same time you give the go-ahead, feel free to pass along any notes or policy links on your general taping/trading stance as well.  You don't need to have a formal written or posted policy before inclusion, but we'd like to know how you feel about the topic.
<br><br>
Besides fans' sending their copies of your shows, you can also <a href= "http://wiki.etree.org/index.php?page=SeedingGuidelines">prepare</a> and <a href="http://www.archive.org/create.php?lma=1">upload</a> your own live recordings to the Archive, if you like. In fact, if you'd like to limit your material to selected contributions from you only,  please just let us know.
<br><br>
If you have any questions about the project, please ask us anytime at etree at archive dot org</a>.        </p>
                <p>
          <b>
            <a name="221"></a>
            The progress of my upload says 'File metadata XML invalid.  Waiting for user to correct.'  How can I fix this?          </b>
                  </p>
        <p>
          This is typically caused by illegal symbols being used somewhere in the information that was put into one of the forms submitted with the show (either the import form or "File Options").  Double check that the only characters being used are those visible on a standard English-language 104 key keyboard.  More information and a few examples are <a href="http://www.archive.org/iathreads/post-view.php?id=35788">here.</a><br><br>

If you have trouble finding the cause, please post to the forum for help.  An admin will have to resubmit the recording for another try, so please send an email including a link to the recording to etree AT archive DOT org if you believe you have cleared the issue.
<BR><BR>
More information on what XML files are and how they are created <a href="http://www.archive.org/iathreads/post-view.php?id=41069">can be read here.</a>        </p>
                <p>
          <b>
            <a name="120"></a>
            I have more Live Music Archive questions...who do I ask?          </b>
                  </p>
        <p>
          Feel free to email etree at archive dot org with any questions, and we'll do our best to post the answers here as soon as possible.  Also, the <a href="http://www.archive.org/iathreads/forum-display.php?forum=etree">message board</a> is a great resource; with so many kind, knowledgable folks out there, you can often get a speedy answer to your question.        </p>
                <p>
          <b>
            <a name="121"></a>
            I have a different source for a show that is already in the archive, should I upload it anyway?          </b>
                  </p>
        <p>
          Yes!  In keeping with the nature of this Archive, it is appropriate for multiple sources of the same show to be available for download.  When you upload the new source, be sure to name the source in the show's top level folder to avoid confusion. Some bands do place <a href= "http://www.archive.org/about/faqs.php#131">limits</a> on the types of sources allowed (such as soundboard recordings), so please <a href= "http://www.archive.org/browse.php?mediatype=collection&collection=etree&field=%2Fmetadata%2Fcreator">check the policy</a> for any given band.        </p>
                <p>
          <b>
            <a name="122"></a>
            How can I help get bands into the Live Music Archive?          </b>
                  </p>
        <p>
          If you know of a <a href= "http://wiki.etree.org/index.php?page=TradeFriendly">trade-friendly</a> live-performing band that is a good candidate for the Archive, you can initiate contact.  Some tips and letter templates can be found <a href="http://www.archive.org/iathreads/post-view.php?id=1353">here</a>.  When you write, make it clear you are asking about the Live Music Archive at archive.org. Don't just ask about their general taping/trading stance.  We want bands to know what's up.
<br><br>
Next, follow up with a message to etree at archive dot org</a>. Mention when you tried to contact the band and what contact point you used.  These are important in order to update our contact records. Admins will update the contact status in <a href= "http://www.archive.org/iathreads/forum-display.php?forum=PendingBands">an announcement forum about Pending Bands</a> based on the message you send us.
<br><br>
If you receive a reply from the band, positive <b>or</b> <a href= "http://www.archive.org/iathreads/forum-display.php?forum=OptOutBands">negative</a>, send a complete copy of the email, complete with its sender's address/brief header info, to etree at archive dot org</a>. It's a good idea to send a copy of what you asked them as well (if not quoted in the reply), since it will give context to the answer.  We need to have full info in hand in order to set up the band appropriately in the Archive, and we may need to contact them for followup questions.
<br><br>
If you are hesitant to make contact yourself, you can mention the band to Archive admins (send email to etree at archive dot org) and they can try a contact as time permits. To help out, supply any contact or policy info you may already know about the band.
        </p>
                <p>
          <b>
            <a name="219"></a>
            When I download concerts, I constantly get disconnected before the download completes.  What can I do to fix this?          </b>
                  </p>
        <p>
          Most web browsers now support robust http downloading.  For questions, see the support website for your browser.          </p>
                <p>
          <b>
            <a name="128"></a>
            What are the WAV MD5 files that are sometimes in filesets?          </b>
                  </p>
        <p>
          MD5 checksums files are not exclusive to SHN files. An MD5 checksum can be used to ensure the accuracy of <i>any</i> data file (e.g. .doc, .mp3, .mpeg).  Some seeders produce MD5 checksums for their WAV files, as well as for their SHN files.  This is just an extra level of confirm to ensure exact copies of the original WAV files are being burned from the SHN files.  Checking a WAV file with a MD5 cheksum is no different than checking a SHN file.  If you use mkwACT, you can just right click on the wav MD5 and choose "verify."        </p>
                <p>
          <b>
            <a name="129"></a>
            I just uploaded a directory that contained WAV MD5 checksums, is that OK?          </b>
                  </p>
        <p>
          The WAV MD5 checksums are ignored by our robot and will not cause problems for your recording.        </p>
                <p>
          <b>
            <a name="220"></a>
            My failure email is indicating that the text file failed.  What can I do?          </b>
                  </p>
        <p>
          Unlike FLAC or SHN, text files do not translate identically from 1 platform to another.  Since the archive.org servers run Unix, text files created on other Operating Systems will fail their MD5check.  We recommend uploaders remove any text files from their MD5's if they are having this problem.        </p>
                <p>
          <b>
            <a name="131"></a>
            Can bands place restrictions on material to be archived?          </b>
                  </p>
        <p>
          Yes.  Each band can tailor the extent of their permission to the Archive.  We quote the band's wishes in the Rights section of the band's <a href="http://www.archive.org/browse.php?mediatype=collection&collection=etree&field=%2Fmetadata%2Fcreator">Collection page</a>.  <a href="http://www.archive.org/iathreads/post-view.php?id=1958">Here are some examples</a> of special restrictions bands have requested. We point out different cases in a band's policy information using a shorthand "<a href= "http://wwww.archive.org/iathreads/post-view.php?id=1958">Limited Flag</a>" tag.
<br><br>
We have a contribution system set up to accomodate individual bands' requirements.  During the upload process, contributors are urged to double check the band's policy notes at different stages.  Archive Curators, volunteer fans who have proven to be in line with the spirit of this archive, will attempt to screen contributions for OK'ed material only. In addition, access to a particular item can be removed if it becomes restricted later (for example, a date newly chosen for commercial release must be removed under some band's policies).
<br><br>
Bands, please contact us at etree at archive dot org anytime to let us know how we can work with you to make things happen.        </p>
                <p>
          <b>
            <a name="133"></a>
            I just uploaded a show and all the files fail the MD5 check, what's the deal?          </b>
                  </p>
        <p>
          Please be sure that if you are choosing any upload format, you are uploading the files in "binary" mode.  If you try to upload .shn or .flac files in "ASCII" mode the files will fail the MD5 check.  ASCII is the standard format for encoding plain text files (actually a subset of binary), while binary is used to encode almost all other types of files.  More information on binary vs. ASCII can be found <a href="http://www.techtv.com/screensavers/answerstips/story/0,24330,3373133,00.html">here</a>.<br><br>
If this does not solve the problem, be sure that all the file names in the MD5 file match the .shn file names.  Be aware that the UNIX system the Internet Archive runs on is case-sensitive.

<br><br>
If you upload FLAC filesets to the LMA, please follow the <a href= "http://wiki.etree.org/index.php?page=NamingStandards">naming standards</a> to help the checking program here. Directories should be named with .flac16 or .flac24 suffix, not .flac. Otherwise, the program will report failures.        </p>
                <p>
          <b>
            <a name="136"></a>
            Where have all the Dave Matthews Band concerts gone? Will they be back?           </b>
                  </p>
        <p>
          At the request of the band's management and as a result of the band's 2003 policy change, Dave Matthews Band concerts (as well as Dave Matthews solo concerts and Dave and Tim shows) have been removed from the Internet Archive. We're very sorry about this unfortunate turn of events but feel like it is important to honor the wishes of the band and its management. 
<br><br>
For more information and discussion see this post:<br>
<a href="http://www.archive.org/iathreads/post-view.php?id=3670"> http://www.archive.org/iathreads/post-view.php?id=3670</a>
        </p>
                <p>
          <b>
            <a name="137"></a>
            Why is there no Phish?  What about Widespread Panic?          </b>
                  </p>
        <p>
          Phish has decided not to participate in the Archive at this point in time<br><br>
Similarly, Widespread Panic has opted out of the project for the time being.  They were last contacted on 11/9/2004.        </p>
                <p>
          <b>
            <a name="138"></a>
            I used to use a download manager and now it stopped working. What's the deal?           </b>
                  </p>
        <p>
          Download managers increase your download speed by connecting to the server multiple times.  Doing this does not significantly increase download speeds but dramatically hurts the performance of the server.  If you wish to use queue to download from the HTTP servers, be sure you set your download program to only use <u>one connection</u> at a time.        </p>
                <p>
          <b>
            <a name="155"></a>
            What's the deal with magic number errors?          </b>
                  </p>
        <p>
          If you get a magic number error when listening to or decoding a SHN file, the SHN file is most likely corrupt.  First, make sure the SHN file passes MD5 verification; if it does not, redownload the file.  If the file passes MD5 verification and you are still getting the magic number error, leave am error report via the show details page noting the magic number error and which track the error occurs on.  Hopefully others who have download the show will confirm or deny the error.  If the error occurs for all downloaders, the seeder will be contacted to provide a new, uncorrupted track.  Please note that there is nothing the Internet Archive administrators can do about a magic number error, becuase the only solution to the error is re-encoding the SHN file from the original WAV file.        </p>
                <p>
          <b>
            <a name="156"></a>
            Do you provide an RSS feed of new updates to the LMA?          </b>
                  </p>
        <p>
          Indeed!  The URL of the feed is http://www.archive.org/services/collection-rss.php?mediatype=etree&collection=etree

You can plug this into a front end like AmphetaDesk (available at: <a href="http://www.amphetadesk.com">http://www.amphetadesk.com</a>)        </p>
                <p>
          <b>
            <a name="163"></a>
            What does the 'Transferred by' field mean?          </b>
                  </p>
        <p>
          This field indicates the person who did the original DAT/MD/Cassette to WAV conversion.  Also, note that in the case of recordings made directly to laptops there is no transfer.        </p>
                <p>
          <b>
            <a name="167"></a>
            Why don't I get an email when my uploads fail MD5 checksums?          </b>
                  </p>
        <p>
          The system currently only sends emails when MD5 files are included.  This means that, if you're uploading FLAC files, you still need to generate and include an MD5 file if you want to receive informational emails about the failures.  <br><br>

A recommended tool for creating these files is <a href="http://www.md5summer.com">MD5summer</a>.  Please note that before uploading the MD5 created with this tool you should open the MD5 in a text editor and remove the top 3 lines so the first signature is now flush with the top of the file.        </p>
                <p>
          <b>
            <a name="250"></a>
            Can I log into an FTP server to download concerts?          </b>
                  </p>
        <p>
          <p>Update (2009April):  To allow us more flexibility on access, we are discontinuing FTP read access.  HTTP read access (as in downloading through your web browser), remains more popular with users, and shall continue.</p>

<p> For more information, please see the discussion forum:<br><br>
<a
href="http://www.archive.org/iathreads/post-view.php?id=240921">http://www.archive.org/iathreads/post-view.php?id=240921</a>
</p>        </p>
                <p>
          <b>
            <a name="193"></a>
            My in-progress upload says ' No metadata describing files found. Waiting for user to enter metadata' - what do I do?          </b>
                  </p>
        <p>
          There are 2 XML files that get created during the import of any recording in the collection:<br><br>

showfolder_meta.xml<br>
showfolder_files.xml<br><br>

The first file gets created when you submit the import form to the collection.  If that file does not exist, you can create it by editing the details page and clicking Update.<br><br>

The second file gets created by filling out File Options.  Just click the link on the left side of the details page and fill out the form as accurately as you can.<br><br>

If either of these files are missing, your Contribution may give you this message.  Please note that once the files get created, it takes 5-10 minutes before the system notices them and moves on to the next stage.        </p>
                <p>
          <b>
            <a name="210"></a>
            The Grateful Dead is here, when will we see Jerry Garcia recordings?          </b>
                  </p>
        <p>
          The taping policy of the Grateful Dead does not extend to recordings of Jerry Garcia's other lineups.  Jerry's solo work is controlled by his estate.  Representatives have said No to the idea of hosting shows in the Live Music Archive.<p>        </p>
                <p>
          <b>
            <a name="214"></a>
            Regarding removing the lossy files ... I edited my show, checked the box to remove them and clicked update.  Now when I click update again, the box is still not checked. Why?          </b>
                  </p>
        <p>
          It takes 2-10 minutes for your checking of that box to 'stick' ...  see 
this discussion board post: <a href="http://www.archive.org/iathreads/post-view.php?id=22816">http://www.archive.org/iathreads/post-view.php?id=22816</a> for an explanation of why.        </p>
                <p>
          <b>
            <a name="229"></a>
            The upload instructions require a 'FLAC Fingerprint' file with my recording - how can I create this?          </b>
                  </p>
        <p>
          <u>In Windows:</u><br><br>

1. Open <a href="http://www.mikewren.com/flac">FLAC Frontend</a><br>
2. Drag all of the FLAC files of your recording into Flac Frontend
window.  (you can also use the "add" button to do this)<br>
3. Click the "Fingerprint" button.<br>
4. Save the fingerprint file with a name like this:
bandYYYY-MM-DD.ffp<br>        </p>
                <p>
          <b>
            <a name="225"></a>
            I've got a great 'filler' for the recording I am about to upload to the collection - should I include it?          </b>
                  </p>
        <p>
          A 'filler' is music from a different performance in addition to the main recording, typically used to fill up extra space on a CD.  Sometimes the filler is a different artist, other times it is the same artist, but a different show and date.<br><br>While this is convenient for burning full CD's, it is not appropriate to include fillers on recordings here in the collection since they get filed under the artist and date of main performance.  Please only include the performance for the artist and date you are importing.  Fillers should be filed under their own entries elsewhere in the collection.        </p>
                <p>
          <b>
            <a name="226"></a>
            Where can I find other recordings by [trade-friendly band] that aren't in the collection?          </b>
                  </p>
        <p>
          If the artist is OK with Internet trading, you may be able to find downloadable recordings through <a href="http://bt.etree.org">http://bt.etree.org</a>.  Also, check <a href="http://db.etree.org">http://db.etree.org</a> to find people who have copies of shows and who may be willing to trade.  Etree.org has additional trading forums at <a href="http://forums.etree.org">http://forums.etree.org</a> Lastly, you can check out a band's own fan forums and mailing lists.  Good luck!

<p>In contrast, the Live Music Archive forum at the Internet Archive is <i>not</i> a good place to post about trades, or to ask for shows that are not yet archived here, whether or not the band presently has a section here.  Moderators may delete these posts.  More posting etiquette tips for that forum are <a href= "http://www.archive.org/iathreads/post-view.php?id=39913">here</a>.        </p>
                <p>
          <b>
            <a name="270"></a>
            I tried downloading a show and I got a '403 Forbidden' page. Why?          </b>
                  </p>
        <p>
          As part of the new (as of May 2007) QA/QC checks that the archive conducts on shows that are uploaded, more refined checks are conducted on shows. For more detail, see this forum post: 

<a href="http://www.archive.org/iathreads/post-view.php?id=124098">http://www.archive.org/iathreads/post-view.php?id=124098</a>

What happens though, when a show either fails it's md5 check, it's internal flac checksum check, or is missing an info.txt file, every non .xml file in the show fileset (the flac files, the mp3's, etc) all become non-downloadable. If you try and click any of the music files, you will be taken to a webpage titled "403 Forbidden" that will say: "Forbidden

You don't have permission to access "ARCHIVE.ORG_Server/show_location/file" (specific to your show file) on this server. **** What this means is that the uploader has a problem with their show files, and as a measure to 'stop the spread' of bad files, the system is preventing people from downloading until the uploader contacts the archive to fix the show. If you as a user find a show that has the above problem, please check back later and once the uploader has fixed the problem, the show will be downloadable as normal.        </p>
                <p>
          <b>
            <a name="241"></a>
            How do I upload a show to the LMA?          </b>
                  </p>
        <p>
          <u>As of 5/2006, the upload method has changed significantly.</u> Here is a walkthrough in <a href= "http://www.archive.org/serve/uploaded/greenone-LMA_Uploads_082609.pdf">PDF with screenshots</a>. Another <a href= "http://www.archive.org/iathreads/post-view.php?id=61285">text description is here</a>.
<br><br>
<u>Before uploading any show</u>, <a href="http://www.archive.org/browse.php?mediatype=collection&collection=etree&field=%2Fmetadata%2Fcreator">read the band's policy notes for this site</a>. Many artists place <a href= "http://archive.org/about/faqs.php#131">limitations</a> on their material here, and info is often updated. Please do not upload shows for any band that does not yet have a <a href= "http://www.archive.org/search.php?query=mediatype%3Acollection%20collection%3Aetree&sort=-%2Fmetadata%2Faddeddate">curator-created collection page</a> here, even if you know the band has recently emailed their permission. Advance attempts may significantly complicate or delay the curators' setup process for the band.
<br><br>
Next, be sure that you are logged in as an Internet Archive member. Have the fileset on your computer already, <a href="http://wiki.etree.org/index.php?page=SeedingGuidelines">correctly prepared</a> and <a href="http://wiki.etree.org/index.php?page=NamingStandards">correctly named</a>. Files <b>must</b> be in <a href= "http://www.archive.org/about/faqs.php#243">lossless format</a> (.flac or .shn), from <i>lossless</i> parent source material; we will optionally create the extra "lossy derivative" copies (.mp3, .ogg) onsite. Prepare to <a href= "http://www.archive.org/create.php?lma=1">create an item</a>, following example tips <a href= "http://www.archive.org/iathreads/post-view.php?id=61854">here</a> or <a href= "http://www.archive.org/iathreads/post-view.php?id=61285">here</a>.         </p>
                <p>
          <b>
            <a name="242"></a>
            How do I make corrections to shows?          </b>
                  </p>
        <p>
          Sometimes people make typos or other mistakes on uploads, or leave gaps in info that can be filled in later. You can help supply good information for archived items. Here is the current best method to submit corrections: 
<br><br>
If you uploaded the show, you can make the changes to the details page yourself.  Make sure you are logged in as the user who uploaded the show and go to the details page of the show you are trying edit.  Click on the "edit" link next to the band name at the top of the details page and you will be able to edit the show details including venue, location, source, setlist, etc.  Be aware that editing these fields will only change the show details, not the files themselves.
<br><br>
<u>5/2006 update</u>: If you uploaded the item and would like to replace or add to files within your item, under the current system this can be done without reuploading the entire fileset. More description may follow; meanwhile there is a walkthrough as a <a href= "http://www.archive.org/iathreads/post-view.php?id=61116">Word document with screenshots</a>. Specifically to fix your items derived between 5/11-22/2006 that sound too fast in the onsite flash player (chipmunk problem), see this <a href= "http://www.archive.org/iathreads/post-view.php?id=61861">PDF document with screenshots</a>.
<br><br>
If you did not upload the show, please email the admins (etree at this domain), and state precisely what the problem with that particular show is. If the problem is a missing setlist, please see this <a href="http://www.archive.org/about/faqs.php#208">FAQ</a>). If there are one or more missing or broken files that you can provide, please re-upload and re-import the entire show under a new directory name, and then email us a link to the old, broken show, asking for that show to be removed.        </p>
                <p>
          <b>
            <a name="243"></a>
            What file formats are accepted for contributions to the Live Music Archive?          </b>
                  </p>
        <p>
          Currently, the Live Music Archive will only accept audio files in either of two lossless formats: FLAC (.flac) or Shorten (.shn).  Please Note that MKW files (.mkw) are *NOT* an acceptable file format for your contributions because they lack cross-platform compatibility (Mac users are unable to play or decode MKW files)<br><br>

In addition, please do not upload the lossy files (MP3 or OGG) next to your FLAC or SHN format files - the Archive <a href= "http://www.archive.org/about/faqs.php#236">creates</a> those files automatically, provided that the contributor agrees to having them available.  This ensures that all the files here have uniform quality options selected.<br><br>

Please follow etree.org's <a href="http://wiki.etree.org/index.php?page=SeedingGuidelines">Seeding Guidelines</a> when preparing your contributions for addition to the collection.  Pay particular attention to the <a href="http://wiki.etree.org/index.php?page=NamingStandards">Naming Standards</a> section. A well-named identifier helps patrons find your show in our large collection. A well-named set of files allows files to be listed in the proper order at the site, and allows patrons to listen to them in playlists and burn them to CD in the proper order, too.        </p>
                <p>
          <b>
            <a name="244"></a>
            I like adding concerts.  Do you have a preference on the way I put in information?          </b>
                  </p>
        <p>
          First of all - thank you so much for contributing to the Archive.  Yes, here are some guidelines that will help us maintain good records for each concert.

<ul>
<li>Do not include HTML in the source and lineage fields.</li>
<li>Do not repeat information in the notes fields (such as source information, or number of discs).  Only include information in the notes fields that is not already in any other field.</li>
<li>If at all possible, keep absolutely nothing but song names in the setlist (even things like disc splits, set splits, etc. should not be in this field).  If possible, putting all song names on one line, separated by commas is wonderful.</li>
<li>Do not fill in unknown field with questions marks or N/A - just leave them blank.  The exception to this guideline is the venue, setlist and source fields (which are mandatory) - in the event that this information is not known, simply write "unknown".</li>
</ul>

Once again, thank you so much!        </p>
                <p>
          <b>
            <a name="245"></a>
            About Grateful Dead concerts on the Archive          </b>
                  </p>
        <p>
          <a href= "http://www.archive.org/iathreads/post-view.php?id=49753">Audience-made</a>
Grateful Dead concert recordings are available as <a href= "http://www.archive.org/details/GratefulDead">downloads</a>
 while available soundboards are accessible in <a href= "http://www.archive.org/iathreads/post-view.php?id=49553">streaming
format</a> only.
<p>
The Grateful Dead is being separated from the <a href= "http://www.archive.org/audio/etree.php">Live Music Archive</a> into its own
<a href= "http://www.archive.org/details/GratefulDead">collection</a> (with its own <a href= "http://www.archive.org/details/GratefulDead#forum">forum</a>) to avoid confusion about lossless availability. The metadata and reviews for shows and recordings, even those not available for regular download, will remain available for those who maintain direct links. No filesets have been deleted from the Archive; certain items are simply not public now. Prior to our completing the changes, <a href= "http://www.archive.org/iathreads/post-view.php?id=51348">text files</a> are easily referenced at a separate <a href="http://db.etree.org/shncirc/gd">database</a>.
<p>
At this time, the Grateful Dead collection is not open to public uploads. The <a href= "http://www.archive.org/iathreads/post-view.php?id=49431">Grateful Dead Internet Archive Project</a> (GDIAP) will continue its direct management of this collection for the time being.
<p>
As far as we know, there has been no change to standard GD fan trading. It is common for bands to have <a href= "http://www.archive.org/audio/etree-band-showall.php">policies</a> that differ between fan trading, versus archiving here.        </p>
                <p>
          <b>
            <a name="248"></a>
            What are the options for streaming a full recording?          </b>
                  </p>
        <p>
          <p><b>Hi-Fi:</b> An MP3 playlist, readable by most players, that has the addresses of MP3 files encoded with a variable bit rate.</p>
<p><b>Lo-Fi:</b> An MP3 playlist, readable by most players, that has the addresses of MP3 files encoded with at a constant bit rate of 64 kilobits per second.  These files are ideal for users with slower Internet connections.</p>        </p>
                <p>
          <b>
            <a name="249"></a>
            What are the options for downloading a full recording?          </b>
                  </p>
        <p>
          <u>Update 5/2006:</u> Please note that due to a major system transition, many items' ZIP files (for their "Lossless" links) have been <u>deliberately disabled</u> for the time being. Engineers are still working on the best method for the new system.
<p><b>Lossless:</b> A ZIP file containing Shorten files or Flac files.  Unlike formats like MP3, lossless formats are true to the original - there is no degradation in quality.</p>
<p><b>Hi-Fi:</b> A ZIP file containing MP3 files encoded with a variable bit rate to deliver high quality at roughly 160kilobits per second.</p>
<p><b>Lo-Fi:</b> A ZIP file containing MP3 files encoded at a constant bit rate of 64 kilobits per second.  These files are ideal for users with slower Internet connections.</p>
<p><b>Other Web Options:</b> All files are displayed as individual links on any item's details page. Web-based download managers can be set up to download all the files you want from the page, as a group. For <a href= "http://www.mozilla.com/firefox/">Firefox</a>, the extension <a href= "http://www.downthemall.net/">DownThemAll</a> is a popular option.
<p><b>BitTorrent:</b> Some Items that are downloadable via HTTP are also downloadable via a BitTorrent client; these items show a 'Torrent' link next to the 'HTTP' download link. (To trigger creation of a BitTorrent file for an item in the LMA that does not yet have one, write a review for it, e.g. "Make me a Torrent!"). <i>Note: only items downloadable via HTTP can be downloaded via BitTorrent.</i>         </p>
                <p>
          <b>
            <a name="268"></a>
            Where can I see the rest of the 'Most Downloaded Items' in the Live Music Archive?          </b>
                  </p>
        <p>
          To view the entire Live Music Archive (everything in the "etree collection") sorted by 'Most Downloaded Items' go to this link: 

<a href="http://www.archive.org/search.php?query=collection%3Aetree&sort=-%2Fmetadata%2Fdownloads">http://www.archive.org/search.php?query=collection%3Aetree&sort=-%2Fmetadata%2Fdownloads</a>

<p>
And here's one that lists everything but the Grateful Dead (like the one on the LMA front page):

<span style="display:block;overflow:hidden">
<a href="http://www.archive.org/search.php?query=collection%3Aetree%20AND%20NOT%20collection%3AGratefulDead&sort=-%2Fmetadata%2Fdownloads">http://www.archive.org/search.php?query=collection%3Aetree%20AND%20NOT%20collection%3AGratefulDead&sort=-%2Fmetadata%2Fdownloads</a>        </p>
                <p>
          <b>
            <a name="269"></a>
            Where can I see the rest of the 'Top Batting Averages' of shows in the Live Music Archive?          </b>
                  </p>
        <p>
          To view the entire Live Music Archive sorted by 'Batting Average' go to this link: 

<a href="http://www.archive.org/search.php?query=collection%3Aetree&sort=-%2Fmetadata%2Fndba">http://www.archive.org/search.php?query=collection%3Aetree&sort=-%2Fmetadata%2Fndba</a>        </p>
                <p>
          <b>
            <a name="282"></a>
            For more information...          </b>
                  </p>
        <p>
          Check out our <a href="http://www.archive.org/iathreads/forum-display.php?forum=etree">Live Music Archive Forum</a>        </p>
                <p>
          <b>
            <a name="307"></a>
            How can I add a logo to the upper right corner of my collection?          </b>
                  </p>
        <p>
          Please email the logo to the admin email address (etree at this domain). Images should have a height of no more than 100 pixels, and be no wider than 200 pixels.        </p>
                <p>
          <b>
            <a name="310"></a>
            How are download counts calculated?          </b>
                  </p>
        <p>
          Downloads are calculated per item page, per IP address, per day. If you stream a show today, that's one download. If you view the txt file tomorrow, that's another download. If you download every file from a show's page the next day, that counts as one more download. If you download the same file a thousand times the day after that, that still only counts as one more download.        </p>
                <p>
          <b>
            <a name="257"></a>
            Why are there no shows by band X?          </b>
                  </p>
        <p>
          We'd like to make sure that a <a href= "http://wiki.etree.org/index.php?page=TradeFriendly">trade-friendly</a> band would not mind having their shows in the Archive for public download.  The best way for us to find out is by getting permission from a band representative or by the band's having an explicit policy that covers this type of site.  If there are no shows by the band, either we don't have enough of this information to go forward with archiving, they have declined participation, or we are ready to accept shows but no one has uploaded anything yet. (Also, see the <a href= "https://www.archive.org/about/faqs.php#101">band status FAQ</a>).
<br><br>
Trade-unfriendly bands will not be found in the Archive, nor will otherwise trade-friendly bands who have declined to have material archived here.
<br><br>
Bands, see other relevant FAQs <a href= "https://archive.org/about/faqs.php#115">here</a> and <a href= "https://archive.org/about/faqs.php#131">here</a>.  Patrons, see more about how you can help <a href= "https://archive.org/about/faqs.php#122">here</a>.        </p>
                <p>
          <b>
            <a name="101"></a>
            What is the status of band X for the Archive?          </b>
                  </p>
        <p>
          <u>5/2006, significant site changes in progress</u>: Formerly, you could check on the status of a band relative to the Archive on the <a href="/audio/etree-band-showall.php">Trade-Friendly Band Information</a> page, which is no longer updated. This FAQ question has been updated for the new-system presentation of info. We have 3 categories:
<br><br>
<i><a href= "https://www.archive.org/browse.php?mediatype=collection&collection=etree&field=%2Fmetadata%2Fcreator">May be Archived</a>-</i>  Band sections have been activated by Archive admins.  Shows can be hosted here to the extent permitted by the band.  <a href= "https://www.archive.org/browse.php?mediatype=collection&collection=etree&field=%2Fmetadata%2Fcreator">Click on the band name</a> and then through to their Policy Notes link to see what <a href= "https://www.archive.org/about/faqs.php#131">limits</a> they may have placed on taping, trading or archiving.
<br><br>
<i><a href= "https://www.archive.org/iathreads/forum-display.php?forum=PendingBands">Pending</a>-</i>  When a patron sends us information about having contacted an additional trade-friendly band, the new band is considered to be "Pending". Admins will update <a href= "https://www.archive.org/iathreads/forum-display.php?forum=PendingBands">notes</a> we keep on the band based on the information that people send to etree at archive dot org</a>. (Sensitive parts of the info- such as email addresses used- will <i>not</i> be posted in the public notes.)
<br><br>
<i>Important</i>: Under the new system, we cannot create a "collection page" for the band name unless and until we know that the band May Be Archived. Further, no shows may be uploaded for any band in advance of a band section's activation. Under the new system, there is no temporary "upload area" to store filesets for bands whose sections are not prepared yet. Please send shows for <a href="https://www.archive.org/browse.php?mediatype=collection&collection=etree&field=%2Fmetadata%2Fcreator">bands on the active list</a> only.
<br><br>
<i><a href= "https://www.archive.org/iathreads/forum-display.php?forum=OptOutBands">Opted Out</a>-</i>  Some bands that may be otherwise trade-friendly may have explicitly said, "No, thanks" to our project.  We respect their wishes.  We still keep notes of their taping/trading policies for reference. 
<br><br>
If your favorite band name is not in any of these 3 categories, there are several possible reasons: They may not be trade-friendly in the first place. No one may have contacted them yet. Someone who contacted them may not have informed us yet. The band may not have written us back yet. If a band did write to us, we may not have had a chance to activate a section yet, or we may not have received enough information back from them to setup their section. In some cases, we may not have received the email successfully, so that a resend may be necessary.
<br><br> 
Bands, see other relevant FAQs <a href= "https://archive.org/about/faqs.php#115">here</a> and <a href= "https://archive.org/about/faqs.php#131">here</a>.  Patrons, see more about how you can help <a href= "https://archive.org/about/faqs.php#122">here</a>.        </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#302">How do I report that something's wrong with a book?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#95">How do I read the books in other formats, like ePub, Mobi, DJVU?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#89">How do I view the PDF books?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#140">What is the directory structure for the texts?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#171">How do you remove line breaks from the Gutenberg texts?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#175">What is the best way to link to a book?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#196">Can I volunteer for the book project?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#295">I see some books from a series, but not all.  How can I access the rest?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#279">For more information...</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#309">What is a book identifier? How is it generated?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#195">I'd like to upload a book.  What format should it be in?  How do you do your sponsored scanning for Contributing Libraries?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#139">What equipment does the Bookmobile use to print and bind books?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#290">What is OpenLibrary?  How can I make my book available via OpenLibrary.org?</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="Texts_and_Books"></a>
          Texts and Books        </h1>
                <p>
          <b>
            <a name="302"></a>
            How do I report that something's wrong with a book?          </b>
                  </p>
        <p>
          <p>If you see an error with a book digitized by the Internet Archive, we'd appreciate knowing about it!

<p>   <b>Please send an email with the URL (web address) of the book, and description of the problem</b>, to info -at- archive.org

<p>In some cases, you may know of alternate information about a book that is supplemental to the library bibliographic record. (For example, a new, more modern transliteration of an author's name.)

<p>To share additional information like the above, you may wish to post it using the option to write a review of a book.  Your additional information will then be available for everyone to see. 
<br><br>
        </p>
                <p>
          <b>
            <a name="95"></a>
            How do I read the books in other formats, like ePub, Mobi, DJVU?          </b>
                  </p>
        <p>
          <br>
ePub is an open textual format (not images of pages). Many readers are becoming available.  A free one is from <a href="http://www.adobe.com/products/digitaleditions/">Adobe</a>.
<br>
Mobi is a proprietary textual format from Amazon supported on the Kindle.
<br>
DJVU is an open format for scanned documents with free readers for
<a href="http://www.celartem.com/en/download/djvu.asp">windows, mac os-x, linux.</a>
It is compact, searchable, good looking, and open format.
<br>
        </p>
                <p>
          <b>
            <a name="89"></a>
            How do I view the PDF books?          </b>
                  </p>
        <p>
          Please see 

<a href=https://www.archive.org/about/faqs.php#62>https://www.archive.org/about/faqs.php#62</a>. 
        </p>
                <p>
          <b>
            <a name="140"></a>
            What is the directory structure for the texts?          </b>
                  </p>
        <p>
          Note re the instructions below:<br>

<li>"XXXX" stands for a 4-digit sequence number, starting with 0000.</li>

<li>What you're uploading is technically considered "processed" images, not "original" ones, even though they are in fact the originals, because archive.org processors wouldn't be doing any rotating or cropping.</li>

<li>The zip or tar has to be built from the parent directory, so that the directory name is included as part of the filename of each file stored in the zip/tar.</li>



<p>In order to store all the texts that the archive has, and will eventually acquire, the directory structure is:<br><br>
<br>IDENTIFIER/IDENTIFIER.extension (tif, djvu, pdf)<br><br>
IDENTIFIER: 
Unique in Archive's collection, alphanumeric (URL safe), this is the original name adopted by the originating collection (alphanumeric characters and _-. Best if from 5 to 80 characters).  One format is [title:8-16][vol:2][author:4][scanninglocation:0-4]<br><br>

EXTENSIONS: 

<li>  If the original files are tif files, then:<br></li>

<li>	IDENTIFIER_orig.tif:
	All the orginal tiffs are stored in the form of multi page tiff.   Demoware windows viewer Informatik Image Viewer.  If it goes over 2GB, then it is stored as a tar of singlepage tifs the directory named IDENTIFIER_orig_tif/IDENTIFIER_orig_XXXX.tif resulting in a file called IDENTIFIER_orig_tif.tar<br></li>
<li>	IDENTIFIER.tif:
	All the cleaned up tifs (usually cropped, despeckled, deskewed) are stored in the form of multi page tiffs. If it goes over 2GB, then it is stored as a tar of a directory named ./IDENTIFIER_tif/IDENTIFIER_XXXX.tif resulting in a file called IDENTIFIER_tif.tar<br></li>
<br>
<li>  If the original files are JPEG JP2 or CR2 files, then:<br></li>

<li>    All the original jpg files are used to make a zip file named IDENTIFIER_orig_jpg.zip   where the names of the pages in the zipped directory are
IDENTIFIER_orig_jpg/IDENTIFIER_orig_XXXX.jpg.   If the resulting file is greater than 2GB (thus breaking the zip format until zip64 is common), then the file will be in tar format named IDENTIFIER_orig_jpg.tar . If the originals are jp2 or cr2 files, then substitute these extentions above.</li>

<li>    Similarly all the processed jpg files (cropped and deskewed) are used to make a zip file named IDENTIFIER_jpg.zip   where the names of the pages in the zipped directory are
IDENTIFIER_jpg/IDENTIFIER_XXXX.jpg.   If the resulting file is greater than 2GB (thus breaking the zip format until zip64 is common), then the file will be in tar format named IDENTIFIER_jpg.tar </li>

<br> 

<li>    In the case where there is a small jpg version of the files for on-screen access then a similar naming convention is used from the _orig.jpg version above, but with _200KB resulting in a file named IDENTIFIER_200KB_jpg.zip where the names of the pages in the zipped directory are
IDENTIFIER_200KB_jpg/IDENTIFIER_200KB_XXXX.jpg.  An equivalent version can be done with other sizes and different formats such as jp2. </li>

<li>	IDENTIFIER.djvu:
A nifty open scanned book format created by AT&T Labs and enhanced by LizardTech.com enabling compression and ease of reprinting. This file will also be ocr'd to make the 	text searchable.( /djvu/bin/documenttodjvu --filelist.txt temp.djvu, /djvu/bin --ocr aatttt.djvu)<br></li>
<li>    IDENTIFIER_djvu.xml this is an xml version of the OCR output which has the word positions (as a bounding box).  this is used for building the djvu file, and is used for searching the flip books, and maybe constructing a searchable pdf in the future. </li>
<li>	IDENTIFIER.pdf:
Adobe acrobat format that is derived from the .tif file if present.<br></li>
<li>	IDENTIFIER.txt.tar.gz or .art.tar.gz:
	If there are OCR'ed text files associated with each page, these are tarred and gzipped in txt format or art which is sakhr format.<br></li>
<li>	IDENTIFIER_cover.doc or .sxw:<br>
cover of the book, some in legal and some letter.  doc is Microsoft Word, and sxw is OpenOffice.<br></li>
<li>    IDENTIFIER_xxxx_bookplate.jp2 or .jpg: is the file that has a bookplate that acknowledges those behind creating the digital version.  xxxx is the page that it will replace in the access formats.</li><br>
<br>
<li>	IDENTIFIER_meta.xml:
	This has the catalog data (title, author, publisher, copyright information) and information about the book found while scanning (size, who scanned it) stored in a dublincore-like XML format. <br></li>
<li>	IDENTIFIER_meta.mrc:
 This will be the MARC (Machine Readable Cataloging) records for the book which provides the mechanism by which computers exchange, use and interpret
	bibliographic information and its data elements make up the foundation of most library catalogs used today. <br></li>
<li>	IDENTIFIER_marc.xml: marcxml format of marc record </li>

<li>    IDENTIFIER_metasource.xml: where the metadata information came from (metadata about the metadata :) ). </li><br>

LEGACY FORMATS: This could be OTIFF | PTIFF | TXT. <ul><li>OTIFF:  These are the original tiff images of the scans of the books.  (to create multipage tifs we used a unix util: tiffcp OTIFF/*.tif aaattt_orig.tif)</li>

<li>PTIFF: These are processed images (cropped,desqewed,depeckled) from the originaltiffs.</li><li>TXT: These are the text files that have been created by doing Optical Character Recoginiton (OCR) on the tiff images.<br></li></ul>
* We plan to eventually remove OTIFF|PTIFF|TXT directories.<br><br>


</ul>

        </p>
                <p>
          <b>
            <a name="171"></a>
            How do you remove line breaks from the Gutenberg texts?          </b>
                  </p>
        <p>
          In Word use find and replace 3 times:

<p>Step 1. Find two paragraph markers - ^p^p
<p>Replace with a neutral character ~ or # or @

<p>Step 2. Find one para markers - ^p
<p>Replace with a single space 
<p>(This might take about 10-15 minutes on large files)

<p>Step 3. Put 2 para markers back in - find ~
<p>Replace ^p^p        </p>
                <p>
          <b>
            <a name="175"></a>
            What is the best way to link to a book?          </b>
                  </p>
        <p>
          Every book in the Archive has an identifier.  For example, RomeoAndJuliet.  To link to the book, you should use the following URL:<br /><br /> http://www.archive.org/download/RomeoAndJuliet        </p>
                <p>
          <b>
            <a name="196"></a>
            Can I volunteer for the book project?          </b>
                  </p>
        <p>
          Volunteers are welcome to come to our San Francisco location! We need your help to scan books! Please contact info at archive.org for more information.         </p>
                <p>
          <b>
            <a name="295"></a>
            I see some books from a series, but not all.  How can I access the rest?          </b>
                  </p>
        <p>
          <p>Many contributing libraries work with the Internet Archive to scan and provide online access to books.  

<p>To ask about whether there are plans to include additional volumes, or other particular books, you can contact the Contributing Library.  

<p>You may wish to also consult <a
href="http://www.archive.org/about/faqs.php#195">http://www.archive.org/about/faqs.php#195</a> and <a
href="http://openlibrary.org/bpl">http://openlibrary.org/bpl</a>        </p>
                <p>
          <b>
            <a name="279"></a>
            For more information...          </b>
                  </p>
        <p>
          <p>Check out our <a href="http://www.archive.org/iathreads/forum-display.php?forum=texts">Text Forum</a>

<p>and have you seen <a href="http://OpenLibrary.org">OpenLibrary.org</a>, a new beta project of Archive.org?          </p>
                <p>
          <b>
            <a name="309"></a>
            What is a book identifier? How is it generated?          </b>
                  </p>
        <p>
          <p>For all items at archive.org, the "identifier" is a unique sequence of letters (with numbers also permitted) that is the basic unit of identification of an item. It travels with the digital object, and is involved in all ways of accessing or otherwise referring to an item.

<p>You see the identifier at the end of an archive.org URL (web address).

<p>For this URL: http://www.archive.org/details/lifeworksofabrah112linc the identifier is "lifeworksofabrah112linc".

<p>For sponsored scanned books, the Internet Archive uses a custom algorithm to generate each book identifier.

<p>Example: hereismytitle00auth

<p>Using this algorithm, up to 16 characters are pulled from the 245 field in the MARC record (<a href="http://www.loc.gov/marc/" target="_blank">MARC</a> is a library catalog record format), and these make up the first part of the identifier.

<p>Then, whatever volume information the loader indicates shows up immediately after that (for monographs this will usually read 00). And then the first 4 letters of the creator are pulled from the MARC 100 field.

<p>The algorithm also has rules that pull out any articles or punctuation to decrease the chances of duplicating an identifier.

<p>If a duplicate identifier is generated, the person loading the book record at the beginning of the digitization process is notified, and manually edits it to make it unique.
        </p>
                <p>
          <b>
            <a name="195"></a>
            I'd like to upload a book.  What format should it be in?  How do you do your sponsored scanning for Contributing Libraries?          </b>
                  </p>
        <p>
          Probably the simplest way to contribute a text item currently is as a pdf.  That way, the entire set of images can be submitted as a single file, and there are no special naming requirements, beyond ending the filename with ".pdf".  If the pdf has no hidden text layer (i.e., isn't searchable), then after doing OCR, Archive.org creates a second pdf with a text layer.
<br><br>
Items can also be submitted as a stack of image files, one image per page.  The files can be in JPEG2000, JPG, or TIFF format.  We plan to provide a more flexible intake procedure, but at present, there are rather strict requirements for how the files in an image stack are to be named, and the stack needs to be packed into a single .zip or .tar file before submission.
<br><br>
When Archive.org scans a book for a Contributing Library, we use the custom-engineered "Scribe" workstation, but for many materials, adequate images can be made with off-the-shelf scanners or good-quality digital cameras.  For best results, use the highest resolution your device is capable of.  Most images we process were produced at a resolution of 300-600 ppi. 
<br><br>
<p><b>How do you do your sponsored scanning for Contributing Libraries?</b>
<p>The Smithsonian Institution shares this video about the scanning Archive.org does to help make more of their Libraries' materials accessible:  
<br><a
href="http://www.youtube.com/watch?v=WztO6fSsxCA">Smithsonian Institution Libraries: Creating the Digital Library (video)</a>
<br><br>
<p>One <b>Do It Yourself </b>approach can be found here: 
<br><a
href="http://www.instructables.com/id/DIY-High-Speed-Book-Scanner-from-Trash-and-Cheap-C/">http://www.instructables.com/id/DIY-High-Speed-Book-Scanner-from-Trash-and-Cheap-C/</a>
<br>
<a
href="http://www.instructables.com/id/SGP6LHRFTM72YMN/">http://www.instructables.com/id/SGP6LHRFTM72YMN/</a>
<br>
<br>
This $300 book-scanning machine is somewhat similar to the Scribe machine used by Archive.org, which also uses open source software for processing book images.
<br><br>
The <b>open source image processing software</b> used by Archive.org:<br>
<a
href="http://sourceforge.net/projects/scribesw/">http://sourceforge.net/projects/scribesw/</a>
<br><br>
Discussion as development proceeded is in the reviews of 
<a
href="https://www.archive.org/details/thelatchkey01millarch">https://www.archive.org/details/thelatchkey01millarch/</a>
<br><br>
You may wish to also consult 
<a
href="https://www.archive.org/about/faqs.php#140">https://www.archive.org/about/faqs.php#140</a>
<br><br>
<p><b>For more on uploading</b>, see <br>
<a
href="https://www.archive.org/about/faqs.php#Uploading_Content">https://www.archive.org/about/faqs.php#Uploading_Content</a>
<br><br>        </p>
                <p>
          <b>
            <a name="139"></a>
            What equipment does the Bookmobile use to print and bind books?          </b>
                  </p>
        <p>
          You can find a list of all the hardware and software used in the bookmobile here: <a href="https://www.archive.org/texts/bookmobile-in_it.php">https://www.archive.org/texts/bookmobile-in_it.php</a> <p>

You can also see a movie of a book being made here: <a href="https://www.archive.org/details/HowToMakeABookmov">https://www.archive.org/details/HowToMakeABookmov</a>
<br>
<br>
<b>What is the status of the Internet Bookmobile?</b>
<br><br>
Internet Archive's Internet Bookmobile is currently out of commission. 
<br>
<br>
        </p>
                <p>
          <b>
            <a name="290"></a>
            What is OpenLibrary?  How can I make my book available via OpenLibrary.org?          </b>
                  </p>
        <p>
          <p>The Open Library is a project of the Internet Archive (archive.org), a non-profit organization in San Francisco, guided by the goal of universal access to human knowledge.  Our small team is working to create a web page for every book ever published, at openlibrary.org. 

<p>Some facts about Open Library you might like to know:

<li>   You are free to edit/correct any errors or omissions you see on openlibrary.org - it's an open, editable wiki. (Just look for the "EDIT" button.)
<li>   We serve a catalog some 23 million books, but not the books themselves.
<li>   We don't buy or sell books
<li>   We have no way of putting you in touch with authors or publishers
<li>   Our team isn't able to help you do research on titles you find in Open Library

<p>There is more information on the Open Library site itself:

<p>About OpenLibrary.org <br>
<a href="http://openlibrary.org/about">http://openlibrary.org/about</a>

<p>Frequently Asked Questions <br>
<a href="http://openlibrary.org/about/faq">http://openlibrary.org/about/faq</a>

<p>Developer Center<br>
<a href="http://openlibrary.org/about/tech">http://openlibrary.org/about/tech</a>

<br><br>
<p>Many <strong>authors write in to ask how they can make their book available as a free download via OpenLibrary.org.  Here's one option:</strong>

<p>Since OpenLibrary.org is a user-editable project, you can sign in to OpenLibrary.org to create a page for your book.  You can upload the book to Archive.org (see information above), and link to the copy you upload to Archive.org. 

<p>You have the option of choosing a particular Creative Commons license for your work, or making a custom statement on what specifically people can or can't do with your item.  Remember that if you wish people to contact you regarding use permissions, you'll need to provide contact information, such as a mailing address or website.  Some uploaders choose to include this information in the description field.  
<br><br>
        </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#77">What happens if my email address changes?  How can I change my email address?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#78">How can I remove my account?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#287">If I remove my account, will my items also be removed from the Archive?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#71">I forgot my password, what can I do?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#72">When I attempt to log in using my username and password, I am told that the username or password is invalid.  What could be wrong?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#73">What is the difference between a virtual library card and an account?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#74">How do I change my password?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#75">How do I change my screen name?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#76">What happens to my forum posts and movie, software, audio, and book reviews when I change my screen name?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#286">What is an Open ID?  Do I have to register for one to use Archive.org?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#288">My account is locked. What can I do?</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="Virtual_Library_Cards_(AKA_Accounts)"></a>
          Virtual Library Cards (AKA Accounts)        </h1>
                <p>
          <b>
            <a name="77"></a>
            What happens if my email address changes?  How can I change my email address?          </b>
                  </p>
        <p>
          <p>You can use <a href="/account/login.changepw.php">this form</a> to change your email address.  </p>

<p>However, be aware that if you change the email address for your account, you will no longer be able to "edit" files posted from your old email address.  If you would like to have your items' ownership transferred to a new email address, send an email to info AT archive DOT org from your OLD email address (the one you want to get rid of - that's how we know you own the items) and tell us which address you'd like to change it to.</p>        </p>
                <p>
          <b>
            <a name="78"></a>
            How can I remove my account?          </b>
                  </p>
        <p>
          You can use <a href="/account/login.changepw.php">this form</a> to remove your account.        </p>
                <p>
          <b>
            <a name="287"></a>
            If I remove my account, will my items also be removed from the Archive?          </b>
                  </p>
        <p>
          No, your items will stay on archive.org once you delete your account. If you would like your items removed, please contact us at info AT archive DOT org.        </p>
                <p>
          <b>
            <a name="71"></a>
            I forgot my password, what can I do?          </b>
                  </p>
        <p>
          As long as you remember the email address which you originally used when signing up for your virtual library card, you can use <a href="/account/login.forgotpw.php">this form</a> to have your password emailed to you.  Bear in mind that your password will be sent in clear text, which means that anyone who views the email (or anyone with sophisticated "packet sniffing" software) can obtain your password.  For this reason you should return to the Internet Archive website once you have your old password and <a href="/account/login.changepw.php">change it to something new</a>.          </p>
                <p>
          <b>
            <a name="72"></a>
            When I attempt to log in using my username and password, I am told that the username or password is invalid.  What could be wrong?          </b>
                  </p>
        <p>
          There are several things to keep in mind when you encounter this error.
<ul>
<li>Your username is your email address, not your screen name.  Make sure you enter the same email address that you supplied when signing up for your virtual library card.</li>
<li>Your password is case-sensitive.  Check to see if the CAPS-LOCK key is engaged (typically a light would be illuminated on your keyboard).  </li>
<li>You might have forgotten your password.  If you think this is the case, you can have your password emailed to you <a href="/account/login.forgotpw.php">here</a></li>
</ul>        </p>
                <p>
          <b>
            <a name="73"></a>
            What is the difference between a virtual library card and an account?          </b>
                  </p>
        <p>
          These two terms are used interchangably.          </p>
                <p>
          <b>
            <a name="74"></a>
            How do I change my password?          </b>
                  </p>
        <p>
          You can use <a href="/account/login.changepw.php">this form</a> to change your password.        </p>
                <p>
          <b>
            <a name="75"></a>
            How do I change my screen name?          </b>
                  </p>
        <p>
          You can use <a href="/account/login.changepw.php" >this form</a> to change your screen name.        </p>
                <p>
          <b>
            <a name="76"></a>
            What happens to my forum posts and movie, software, audio, and book reviews when I change my screen name?          </b>
                  </p>
        <p>
          Your old reviews and posts will be updated with your new screen name.          </p>
                <p>
          <b>
            <a name="286"></a>
            What is an Open ID?  Do I have to register for one to use Archive.org?          </b>
                  </p>
        <p>
          For what an Open ID is and how you can use it, see <a href="http://openid.net/">http://openid.net</a>
<br><br>
An Open ID is not required to obtain a library card (account) for Archive.org
        </p>
                <p>
          <b>
            <a name="288"></a>
            My account is locked. What can I do?          </b>
                  </p>
        <p>
          It is likely that your account was locked because you uploaded multiple items that seemed to have rights issues or the content you uploaded was inappropriate for the Archive. 
If you do have rights to the content you uploaded and you believe it is appropriate for Internet Archive, please contact us with your thoughts at info AT archive DOT org.        </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#1029">How do I play an arcade game?</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="The_Internet_Arcade"></a>
          The Internet Arcade        </h1>
                <p>
          <b>
            <a name="1029"></a>
            How do I play an arcade game?          </b>
                  </p>
        <p>
          Use this clickpath:<br />
https://archive.org/details/internetarcade ><br />
Click an image of one of the games ><br />
On the next page click "Run an in-browser emulation of the program" below the image on the right ><br />
Read the instructions for how to play below the game image ><br />
Press the spacebar to start ><br />
Click "Insert Coin" in upper left menu >
Click either "Player One" or "Player Two" ><br />
The game should then start.<br />        </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#34">What software can play the downloaded movies?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#40">Can I use these movies in FinalCutPro -- in the Quicktime format?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#41">Sometimes when I play a movie, the video is choppy or very pixelated. Why is that?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#49">Who owns the rights to these movies?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#55">Is there a discussion list about the movies?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#56">Are there other similar archives on the Web?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#70">What are those animations associated with each movie and how did you make them?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#94">Can I stream the movies?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#146">Encoding Parameters</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#168">What is an editable file?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#234">Can I upload this movie?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#235">What kind of movie file should I submit?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#273">How can I embed a flash player with my movie on my web page?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#280">For more information...</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#305">How can I add a logo to the upper right corner of my collection?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#217">How do I make DVD's from Internet Archive movies?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#260">How can I make a DVD using linux?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#39">Why do I get errors when I try to play a movie?</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="Movies"></a>
          Movies        </h1>
                <p>
          <b>
            <a name="34"></a>
            What software can play the downloaded movies?          </b>
                  </p>
        <p>
          <p><a href="http://www.videolan.org/">VLC Media Player</a> is the most versatile player we've found for playing the wide variety of movies found in the Archive.  And, it's free!  We also recommend MPlayer.

<p>For Windows: <br>
   MPEG1 (VCD) most players;  <br>
   MPEG2 (DVD) freeware <a href="http://www.videolan.org/">VLC</a>, shareware player from <a href="http://www.elecard.com">http://www.elecard.com</a>, or for-pay quicktime6 plugin: <a href="http://www.apple.com/quicktime/products/mpeg2playback/">http://www.apple.com/quicktime/products/mpeg2playback/</a>  ; <br>
   MPEG4 quicktime6 from <a href="http://www.apple.com">www.apple.com</a> or VLC   .  Latest flash plugin for browsers.<br><br>

<p>For Mac OSX and 9: <br>
    MPEG1 (VCD) most players;  <br>
    MPEG2 (DVD)  freeware  VLC ( <a href="http://www.videolan.org/">http://www.videolan.org/</a> ) the for-pay quicktime6 add-on (see <a href="http://www.apple.com/quicktime/products/mpeg2playback/">http://www.apple.com/quicktime/products/mpeg2playback/</a> ). <br> 
    MPEG-4  Quicktime6.  Latest flash plugin for browsers.<br> <br>   

<p>Some Mac users have written to us suggesting MPlayer (OS X), BBDEMUX, and MPEG2DECX --  
free on <a href="http://www.versiontracker.com">www.versiontracker.com</a>.


<p>For more details, troubleshooting, and how to play movies on other operating systems, see this <A HREF=http://www.pressthebutton.com/archives/>how-to page</A>.
        </p>
                <p>
          <b>
            <a name="40"></a>
            Can I use these movies in FinalCutPro -- in the Quicktime format?          </b>
                  </p>
        <p>
          You can Re-encode Mpeg2 movies to quicktime for FinalCut Pro using Cleaner5.0.2 using the following settings. There is no de-interlacing, so you don't lose anything.  The files increase in size 10 fold, so make sure you have enough HD space.  This procedure gives you quicktime movies suitable for use with final cut.<p> Cleaner 5 -- if you don't have 5.0.2, you can download.0.2 from the terran.com site.<br> - output > quicktime, .mov<br>  - tracks > process everything<br>  - image > image size constrain to 720*480, display size   normal, do not deinterlace, field dominance-SHIFT DOWN<br>  - encode > apple DV-ntsc codec, millions of colors, spatial   quality 100%, frame rate, same as source<br>  - Audio > we're still not sure about which is best. start   with mono, 48kb, experiment. </p><p>Some have had good results with their decoder cards. compare a few films done   both ways on a good monitor with scopes and see which   method is best.</p>If you still have trouble, post your question on our discussion list (<a href=mailto:moviearchive-subscribe@yahoogroups.com>moviearchive-subscribe@yahoogroups.com</a>) or write to us at info at archive dot org</a>.</p>
 -- NEW -- One of the simplest ways to transcode movies from MPEG-2 to DV format for editing is to use the freeware utility MPEG Streamclip (Mac OS X and Windows) available at squared5.com.  It offers many settings and maintains video/audio sync.        </p>
                <p>
          <b>
            <a name="41"></a>
            Sometimes when I play a movie, the video is choppy or very pixelated. Why is that?          </b>
                  </p>
        <p>
          <p>Try downloading the movie to your computer and watching it locally.  Sometimes choppiness occurs when we can't stream it to you quickly enough (because your connection is slow or our servers are overloaded).</p>

<p>If you're watching an MPEG-4 that we derived from an original MPEG-2, we first reduce its size to 320 x 240 - a quarter of the resolution of NTSC video. We then translate it at 350 kbps, which is really borderline for that resolution. You see errors occasionally because there simply isn't enough bandwidth available, so the MPEG-4 encoder either drops frames - resulting in jerky or choppy motion - or drops macro blocks - resulting in blurred or pixelated video. That is the price we pay for the small file size - 80 MB for a 1/2-hour clip is really very small in the digital video world.  If this is the case, download the original MPEG-2 to solve the problem.</p>        </p>
                <p>
          <b>
            <a name="49"></a>
            Who owns the rights to these movies?          </b>
                  </p>
        <p>
          <p>This will vary from movie to movie.</p>

<p>Many of the movies and collections are licensed with Creative Commons Licenses.  Uploaders may designate whether or not an item has a CC License.  If they do so, the Creative Commons logo will appear on the left hand side of the movie's detail page.  Click on this logo to see details about the specific type of license that the uploader has assigned to the movie.  Archive.org cannot guarantee the accuracy of uploader-provided information.</p>

<p>Some films may have the contact information listed for the filmmaker. If the information is provided, feel free to contact the filmmaker or organization the film comes from.</p>
        </p>
                <p>
          <b>
            <a name="55"></a>
            Is there a discussion list about the movies?          </b>
                  </p>
        <p>
          Yes â€” our list is about both movie content and technical issues. You can subscribe at <a href=mailto:moviearchive-subscribe@yahoogroups.com>moviearchive-subscribe@yahoogroups.com</a>.
        </p>
                <p>
          <b>
            <a name="56"></a>
            Are there other similar archives on the Web?          </b>
                  </p>
        <p>
          <p>There are many sites that allow users to upload videos, but most of them only display very low quality video and/or do not let you download the videos.</p>

<p>As far as we know, this is the only site that presents high-quality downloadable movie data files with such liberal use restrictions. See the <a href=http://www.prelinger.com/links.html>Links page</a> at Prelinger Archives for a number of sites that may be useful to researchers or those seeking specific films or footage.</p>        </p>
                <p>
          <b>
            <a name="70"></a>
            What are those animations associated with each movie and how did you make them?          </b>
                  </p>
        <p>
          <p>The animations on the details pages and on the browse pages are animated GIF files.  In most cases, still shots from each minute of the program were grabbed and saved as JPG files (these are the thumbnails which you can reach by clicking on the "View thumbnails" links).  Then a tool called ImageMagick was used to create the animated GIF files from the JPGs.</p>

<p>We try to create an animated gif for every movie when it is uploaded (it may take a while to appear), but there are some file formats and/or encoding settings that make this difficult.  If an animated gif hasn't appeared for your item by the day after you uploaded it, we probably couldn't make one for your item.</p>        </p>
                <p>
          <b>
            <a name="94"></a>
            Can I stream the movies?          </b>
                  </p>
        <p>
          <p>There are several programs you can use to stream movies in the Archive.  Because we allow users to upload video files in any format, the same player will not always work for every single file, so it's a good idea to have a couple of programs available that you can try.  Also, some files simply can't be streamed.  Usually, this happens when the program that created the video file uses a codec that our software doesn't understand.  So if you click on a stream link and get an "unsupported media" sort of error, use the download links instead.</p>

<p>Here are some free players that might come in handy:</p>

<p><a href="http://www.apple.com/quicktime">Quicktime</a><br>
If you have Quicktime installed, many mp4 streaming movies will play right in your browser window just by clicking a stream (or download) link.  Make sure you have the latest version so that you can play the widest array of files.</p>

<p><a href="http://www.videolan.org/vlc/">VLC Media Player</a><br>
Open your VLC Media Player and go to File > Open Network Stream.  Click the File tab and enter the <b>download</b> link of the file you want to watch.  Yes, this seems backward, but it works! </p>

<p>So, if you were trying to stream the movie Duck and Cover found at http://www.archive.org/details/DuckandC1951 you would:<br>
Use this URL: <br>http://www.archive.org/<b>download</b>/DuckandC1951/DuckandC1951_256kb.mp4<br>
<b>NOT</b> this URL: <br><span
 style="text-decoration: line-through;">http://www.archive.org/</span><span
 style="font-weight: bold; text-decoration: line-through;">stream</span><span
 style="text-decoration: line-through;">/DuckandC1951/DuckandC1951_256kb.mp4</span></p>

<p>VLC will stream mp4, avi, mpg and other file formats, so it is quite useful for viewing the majority of the files in the archive.</p>

<p><a href="http://www.real.com">Real Player</a><br>
You can use Real Player to stream Real Media files.</p>

<p>We support two bitrates: 32Kbps-192Kbps for modem and ISDN users plus 256Kbps-450Kbps for DSL and cable-modem users.</p>
<p> </p>
<p> </p>        </p>
                <p>
          <b>
            <a name="146"></a>
            Encoding Parameters          </b>
                  </p>
        <p>
          We attempt DVD, VCD, and MP4 streaming for broadband.  We want these parameters to easily work with low-end video editors. <br> <br>

MPEG-2, DVD -- 720x480 or 702x480 interlaced. With a system header on each pack to be compatible with DVD. (Prelinger movies are 1/2 D1 352x480 29.97 fps which causes some players to make them look skinny)<br> <br>

MPEG-1, VCD -- Video Resolution SIF (352 x 288 <br>PAL, 352x240 NTSC)
<br>Framerate 29.7 or 25 for PAL
<br>Video Compression MPEG-1
<br>Video Bitrate Up to 1151 kbps constant bitrate (CBR)
<br>Audio 224 kbit/sec MPEG-1 Layer2
<br>Stereo 44.1khz
<br/>Created with ffmpeg.
<br/><br/>

MPEG-4 -- 512Kbps h.264 VBR 320x240 video with 64Kbps AAC audio.  Hinted for streaming. 
Created with ffmpeg and mp4creator.
<br/> <br/>
        </p>
                <p>
          <b>
            <a name="168"></a>
            What is an editable file?          </b>
                  </p>
        <p>
          An editable file is a file which can be downloaded and used in an editing program.  The MPEG-4 are the highest bitrate versions we could do with the linux mpeg-2 to mpeg-4 conversion tools we use.  These files can be read directly into FinalCut-Pro from Apple, and can be converted to mov using Quicktime-pro and read directly into iMovie from Apple.

        </p>
                <p>
          <b>
            <a name="234"></a>
            Can I upload this movie?          </b>
                  </p>
        <p>
          <p>You may upload movies that you own the copyright to, or that are in the public domain.</p>

<p>We are not copyright lawyers, and copyright is a tricky business, so you may want to consult a copyright researcher to clear material before you use it. You may also want to check this <a href="http://www.archive.org/download/movie_research/movie_research.html">list of movies</a> that one of our volunteers has already researched.</p>

<p>Here is some general information on the subject that may help you decide if your movie is okay to upload.  The information below applies to films produced in the United States only.</p>

<p>1) <b>Is there a copyright notice visible in the film?</b> It is usually visible with the title or at the end of the film.</p>

<p><b>If the work was made in 1923 or earlier</b>, it is probably public domain and can be uploaded. <b>NOTE!</b> Restored versions of the film or new soundtracks for silent films can have more recent copyrights that are still valid - usually a copyright notice for a new soundtrack or restoration will appear in the film.</p>

<p><b>For works made from 1923 to 1949</b>, post a question to the movie forum on this site before you upload. The copyright could have been renewed and there isn't a way online to check a film's copyright status.</p>

<p><b>For works made from 1950 to 1963</b>, you can check the title at the Library of Congress Copyright Database for copyright renewals: <a href="http://www.copyright.gov/records/cohm.html">http://www.copyright.gov/records/cohm.html </a>. This will list copyright renewals for most films.</p>

<p><b>If the copyright notice is 1964 or later</b>, the copyright is probably still valid and the film should not be uploaded unless you are the copyright holder.</p>

<p>2) <b>Is the copyright notice in the correct format?</b> It needs to state three things - the word 'copyright' or the copyright symbol or '(c)', the year and who owns the copyright? If it is missing one of those elements or if there is no notice, it could be public domain. If you aren't sure, please post a question to the movie forum on this site.</p>

<p>3) <b>Is the film foreign (not from the U.S.)?</b> Foreign titles might not have a copyright notice, but still may be copyrighted in their country of origin. Traditionally the U.S. wouldn't recognize the copyright of a foreign film unless it was registered in the U.S. That has recently changed with the GATT treaty. Many foreign works had their copyrights restored. Please post a question to the movie forum on this site about these films before you upload.</p>        </p>
                <p>
          <b>
            <a name="235"></a>
            What kind of movie file should I submit?          </b>
                  </p>
        <p>
          <p>The archive is all about free access to information, so you should submit file formats that are easily downloadable and/or streamable for other site patrons.</p>

<p>We prefer that you submit the highest quality format that you have available, and then we will attempt to create smaller file sizes and formats automatically with our deriver program. MPEG2 files are the easiest file type for us to deal with. We recommend that you do not attempt to do any special encoding of your files - the more settings you mess around with, the less likely our deriver code will be able to process the file.</p>

<p>Whatever format you choose, please upload each file to your item individually, in a non-compressed format. Uploading content in a .zip or .rar file makes your item unstreamable and significantly less accessible to others. If you upload .zip, .rar, non-video formats (like .exe), or password-protected files, they may be removed by our moderators.</p>

<p>The table below describes what file formats we will attempt to derive depending on what type of file you submit.</p>

<iframe style="width:700px; height:800px; border:0px"
        src="/help/derivatives.php?mediatype=movies">
</iframe>

        </p>
                <p>
          <b>
            <a name="273"></a>
            How can I embed a flash player with my movie on my web page?          </b>
                  </p>
        <p>
          It's really easy to embed our flash player with your movie into your web site. To do so, go to the item page for the movie you want to embed. Then click the flash player as if to watch the movie. When you do, you'll see a small question mark beneath the player. Click on this and you'll get the instructions and code you need to embed the movie into your web page.         </p>
                <p>
          <b>
            <a name="280"></a>
            For more information...          </b>
                  </p>
        <p>
          Check out our <a href="http://www.archive.org/iathreads/forum-display.php?forum=movies">Moving Images Forum</a>        </p>
                <p>
          <b>
            <a name="305"></a>
            How can I add a logo to the upper right corner of my collection?          </b>
                  </p>
        <p>
          First, make sure you're logged on to archive.org with the same email address you used when you created your collection. 

Note: Images should have a height of no more that 72 pixels.

â€¢ Â Go to your collection's front page
â€¢ Â Click the "Edit Item!" link next to your user name.
â€¢ Â Click "Item Manager" near the top of the page.
â€¢ Â Click the "checkout --edit items files (non XML)" button in the "Edit Operations" section of the form.
â€¢ In Step 1 of 2, click the "Share" button.
â€¢ Â Locate and select the image to be uploaded and click "Select".
â€¢ Â In Step 2 of 2 click the "Update Item!" button
â€¢ Â Return to collection front page and click "edit" link again
â€¢ Â Find logo file at bottom of page, choose "Collection Header" from the drop down list and click submit.

It might take a few minutes for the changes to appear.        </p>
                <p>
          <b>
            <a name="217"></a>
            How do I make DVD's from Internet Archive movies?          </b>
                  </p>
        <p>
          Please read this forum posting about how to create DVDs from many of the movies found in the Archive: <a href="https://www.archive.org/iathreads/post-view.php?id=26467">https://www.archive.org/iathreads/post-view.php?id=26467</a>.  If you have further information to add, please <A HREF="mailto:info@archive.org?subject=DVD%20FAQ">email us</a>.         </p>
                <p>
          <b>
            <a name="260"></a>
            How can I make a DVD using linux?          </b>
                  </p>
        <p>
          An Archive user sent in the following instructions for creating DVDs on a linux system:

<blockquote>

To do this under linux from the command line:

This requires a few common programs. Using any modern package
distribution of linux installing these should be quite simple.

<ul>
<li><a href="http://www.mplayerhq.hu/">mplayer</a> (http://www.mplayerhq.hu/)</li>
<li><a href="http://www.transcoding.org">transcode</a> (http://www.transcoding.org)</li>
<li><a href="http://mjpeg.sourceforge.net/">mjpegtools</a> (http://mjpeg.sourceforge.net/)</li>
<li><a href="http://dvdauthor.sourceforge.net/">dvdauthor</a> (http://dvdauthor.sourceforge.net/)</li>
</ul>


<ol>

<li>The first command copies just the video out of input.mpeg and produces
output.video:<br>
<strong>mplayer input.mpeg -dumpstream -dumpfile /dev/stdout | tcextract -t
vob -a 0 -x mpeg2 > output.video</strong>
<br>
</li>


<li>The second command copies just the audio out of input.mpeg and
produces output.audio:<br>
<strong>mplayer input.mpeg -aid 128 -dumpaudio -dumpfile output.audio</strong>
<br>
</li>

<li>The third command combines the video and audio back together again in
a format ready for dvdauthor:<br>
<strong>mplex -f 8 -V -o complete.vob output.video output.audio</strong>
<br>
</li>

<li>This step creates the dvd structure.  Create a new file with any text editor with the following:<br>
<strong>
<dvdauthor dest="DVD_folder"><br>
 <vmgm /><br>
  <titleset><br>
    <titles><br>
      <pgc><br>
        <vob file="complete.vob" chapters="0,15:00,30:00,45:00,60:00"/><br>
      </pgc><br>
    </titles><br>
  </titleset><br>
 </dvdauthor><br>
</strong>
The chapters line lists the points to include chapter marks on the DVD
for jump navigation.
</li>

<li>Now let dvdauthor create our dvd:<br>
<strong>dvdauthor -x dvdauthor.xml</strong>
</li>

</ol>

<p>Done!  You should now have a folder called "DVD_folder" with your movie. You
can create an ISO or BIN image with mkisofs:<br>
<b>mkisofs -dvd-video -V "Movie Title" -o movie.iso DVD_folder/</b>
</p>

<p>You can play movie.iso in most any video player or burn it to a DVD:<br>
<b>growisofs -speed=16 -dvd-compat -Z /dev/dvd=movie.iso</b>
</p>

<p>If you just want to burn the film to a DVD you do not have to create
the movie.iso image file:<br>
<b>growisofs -speed=16 -dvd-video -dvd-compat -V "Movie Title" -Z
/dev/dvd DVD_folder/ </b>
</p>

</blockquote>        </p>
                <p>
          <b>
            <a name="39"></a>
            Why do I get errors when I try to play a movie?          </b>
                  </p>
        <p>
          <p>The best all-around, free player is <a href="http://www.videolan.org/">VLC Media Player</a> - it handles most of the movie files you will find on this site.  If you're seeing errors when you try to play movies, please try downloading VLC and using that instead.  This clears up many people's problems.</p>

<p>Here are some other possible problems:

<ol>

<li>There is heavy traffic to our site. If you experience a delay, please try again later or at a different time of day.</li>

<li>You're behind a firewall and the firewall software is attempting to modify incoming bits. Contact your network or firewall administrator.</li>

<li>Your Internet connection went down or timed out. Check with your ISP or network administrator to see if there's a special policy about keeping a connection live.</li>

<li>If your browser seems to hang after a "100% downloaded" message, check to see that you have sufficient hard-disk and TMP disk space. Rebooting the system sometimes helps.</li>

<li>You are trying to play an MPEG-2 file on a platform other than Windows or Linux. At present, you need VLC ( <a href="http://www.videolan.org">http://www.videolan.org</a> ) or the for-pay <a href="http://www.apple.com/quicktime/">quicktime6</a> add-on to play MPEG-2 files on the Macintosh.  Please contact us at info at archive dot org if you have information about other players that work on platforms other than Windows.</li>

<li>2. Your player tried to stream the movie, and it isn't streamable.  Download the movie first, and then play it.  (Right-click > Save As)</li>

<li>3. Some conflict exists between your computer's configuration and the player you're using. Unfortunately, because PCs can be set up in so many different ways and because different standards exist for playing video, finding a player that will work is a hit-and-miss process. Try Rod Hewitt's <a href=http://www.coolstf.com/mpeg/mpeg-players.html>evaluations</a> of a number of players.</li>
</ol>
</p>

<p>If you still have trouble, post your question to the  <a href="https://www.archive.org/details/movies#forum">moving images forum</a>.</p>        </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#38">Can I download files via FTP?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#154">What are some good FTP clients?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#222">How do I download files? </a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="Downloading_Content"></a>
          Downloading Content        </h1>
                <p>
          <b>
            <a name="38"></a>
            Can I download files via FTP?          </b>
                  </p>
        <p>
          <p>Update (2009April):  To allow us more flexibility on access, we are discontinuing FTP read access.  HTTP read access (as in downloading through your web browser), remains more popular with users, and shall continue.</p>

<p> For more information, please see the discussion forum:<br><br>
<a
href="https://www.archive.org/iathreads/post-view.php?id=240921">https://www.archive.org/iathreads/post-view.php?id=240921</a>
</p>
        </p>
                <p>
          <b>
            <a name="154"></a>
            What are some good FTP clients?          </b>
                  </p>
        <p>
          <p>Update (2009April):  <b>Please note that to allow more flexibility on access, we are discontinuing FTP read access.  HTTP read access (as in downloading through your web browser), remains more popular with users, and shall continue.</b></p>

<s><p>FTP can yet be very useful for your uploads.</p>


Here are a few FTP clients that users found to work well:<br><br>
</s>
<u>For Windows Users</u>
<ul>
<li><a href="http://sourceforge.net/projects/filezilla">Filezilla</a> (support open source!)</li>
<li><a href="http://www.smartftp.com">SmartFTP</a></li>
<li><a href="http://www.internet-soft.com/ftpcomm.htm">FTP Commander</a></li>
</ul>

<u>For Mac Users</u>
<ul>
<li><a href="http://sourceforge.net/projects/filezilla">Filezilla</a> (support open source!)</li>
<li><a href="http://cyberduck.ch/">Cyberduck</a> (support open source!)</li>
<li><a href="http://www.panic.com/transmit/">Transmit</a></li>
<li><a href="http://fetchsoftworks.com/">Fetch</a></li>
<li><a href="http://www.interarchy.com/main/">Interarchy</a></li>
</ul>        </p>
                <p>
          <b>
            <a name="222"></a>
            How do I download files?           </b>
                  </p>
        <p>
          <p>To download the files on a PC, right click the link to the file, and select "Save Target As" or "Save Link As" (or something similar depending on which browser you're using). </p>

<p>On the Macintosh, hold the button down while the mouse is over the link, and when the menu comes up, select "Save Target As".</p>

<p>Update (2012July): some Internet Archive items may be downloaded via the BitTorrent protocol using the link <b>Torrent</b> on the item's webpage. Download via BitTorrent requires an up-to-date BitTorrent client, see the FAQ on <a href="https://archive.org/about/faqs.php#Archive_BitTorrents">Archive BitTorrents</a> for more information.        </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#1010">What books can I borrow? How can I find them?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#1011">Which reading devices can be used to read the eBooks borrowed through archive.org?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#1012">How many books can I check out at once?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#1014">How can I see which books I've checked out?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#1015">Can I put a library book on hold?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#1016">Where do I get Adobe Digital Editions?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#1017">How do I authorize Adobe Digital Editions? Who is my ebook vendor?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#1018">What about using ereaders?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#1019">Can I read or borrow books on my Kindle?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#1008">How does borrowing a book work through archive.org?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#1020">Can I borrow books on my Ipad or Android tablet?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#1013">Can I return a library book early?</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="Borrow_from_Lending_Library"></a>
          Borrow from Lending Library        </h1>
                <p>
          <b>
            <a name="1010"></a>
            What books can I borrow? How can I find them?          </b>
                  </p>
        <p>
          The easiest way to find books to borrow is to jump straight to the <a href="https://archive.org/details/lendinglibrary" rel="nofollow">Lending Library</a> which shows works which have editions that are available through the Internet Archive.        </p>
                <p>
          <b>
            <a name="1011"></a>
            Which reading devices can be used to read the eBooks borrowed through archive.org?          </b>
                  </p>
        <p>
          Internet Archive offers borrowable books in BookReader, <a href="http://en.wikipedia.org/wiki/Pdf" rel="nofollow">PDF</a> and <a href="http://en.wikipedia.org/wiki/EPUB" rel="nofollow">ePub</a> formats. BookReader editions may be read online immediately in any web browser. Downloadable eBooks are readable in Adobe Digital Editions and some other software platforms. <a href="http://blogs.adobe.com/digitalpublishing/supported-devices" rel="nofollow">Here is a list of supported devices on Adobe's website</a>. ADE also provides support for <a href="http://en.wikipedia.org/wiki/Sony_Reader" rel="nofollow">Sony's Reader</a>.         </p>
                <p>
          <b>
            <a name="1012"></a>
            How many books can I check out at once?          </b>
                  </p>
        <p>
          You can borrow 5 books at a time from archive.org. Each loan will expire after 2 weeks and will automatically "return" at the end of that time period.        </p>
                <p>
          <b>
            <a name="1014"></a>
            How can I see which books I've checked out?          </b>
                  </p>
        <p>
          There's a page under your archive.org Account which displays all the books you've checked out at any one time - <a href="https://archive.org/account/loans.php" rel="nofollow">https://archive.org/account/loans.php</a>. Additionally you can see your loan history by going to <a href="" rel="nofollow">https://archive.org/account/your Account page</a> and clicking the "Profile" link.
        </p>
                <p>
          <b>
            <a name="1015"></a>
            Can I put a library book on hold?          </b>
                  </p>
        <p>
          Not at this time but we hope to add this feature in the near future.
        </p>
                <p>
          <b>
            <a name="1016"></a>
            Where do I get Adobe Digital Editions?          </b>
                  </p>
        <p>
          You can  <a href="http://www.adobe.com/products/digitaleditions/" rel="nofollow">download Adobe Digital Editions</a> from adobe.com. It's free. If you are using a device that can not run Adobe Digital Editions, you still need an Adobe account. <a href="https://www.adobe.com/account/sign-in.adobedotcom.html" rel="nofollow">You can get one online here</a>. <a href="http://adobedigitaleditions.gooofull.net/" rel="nofollow">An older version of Adobe Digital Editions can be found at this link</a>.        </p>
                <p>
          <b>
            <a name="1017"></a>
            How do I authorize Adobe Digital Editions? Who is my ebook vendor?          </b>
                  </p>
        <p>
          The first time you run Adobe Digital Editions, it will prompt you for authorization. This is completely optional and is not linked to your archive.org ID. If you do not want to set up an Adobe ID, check the box in the lower left where it says "I want to Authorize my computer without an ID" and click Authorize. 
<br /><br />
If you do want to set up an ID, click the "create an Adobe ID" link next to the eBook vendor line (which should remain set on "Adobe ID"). You can authorize your computer at a later date by going under the Help menu of ADE and selecting the "Authorize computer..." option.
<br /><br />
<img src="https://ia601704.us.archive.org/3/items/AdobeAuthorize/adobe-authorize.jpg" alt="screen shot of adobe digital editions authorization page" />        </p>
                <p>
          <b>
            <a name="1018"></a>
            What about using ereaders?          </b>
                  </p>
        <p>
          Regardless of which ereader you have, you can read archive.org eBooks online in your browser with our BookReader. Many devices support PDF files, which can be downloaded from archive.org. Below are some tips for using some popular ereader devices. Feel free to send your feedback and questions to info@archive.org.        </p>
                <p>
          <b>
            <a name="1019"></a>
            Can I read or borrow books on my Kindle?          </b>
                  </p>
        <p>
          The procedure varies depending what model Kindle you have. 
<br /><br />
If you have a Kindle Fire, you will need to "sideload" an Adobe Digital Editions compatible application such as Overdrive Media Console to borrow modern ebooks. <a href="http://www.sclibrary.ab.ca/docs/ebooks/Kindle%20Fire%20handout.pdf" title="how to borrow archive.org books using your Kindle Fire" rel="nofollow">Here is a handout from one of our partner libraries explaining the process</a>.
<br /><br />
For older non-Fire Kindles, you can only read Classic Ebooks not borrow Lending Library books.         </p>
                <p>
          <b>
            <a name="1008"></a>
            How does borrowing a book work through archive.org?          </b>
                  </p>
        <p>
          The Internet Archive and participating libraries have selected digitized books from their collections that are available to be borrowed by one patron at a time from anywhere in the world for free. These books are in BookReader, PDF and ePub formats (and <a href="http://www.daisy.org/" rel="nofollow">Daisy</a> for the print disabled).  You can choose which format you prefer as you complete the borrowing process.
<br /><br />
BookReader editions may be read online immediately in your web browser. No special software is required.
<br /><br />
Other Internet Archive loans are managed through <a href="http://www.adobe.com/products/digitaleditions/" rel="nofollow">Adobe Digital Editions</a>, which you may need to download to manage your library of borrowed books.
<br /><br />
<b>How do I get set up to borrow books through archive.org?</b>
<br /><br />
Follow these steps:
<br /><br />
1. <a href="https://archive.org/account/login.createaccount.php" rel="nofollow">Sign up for an archive.org account</a><br />
2. Some ebooks require <a href="http://www.adobe.com/products/digitaleditions/" rel="nofollow">Adobe Digital Editions</a> (This is where you can read the books you've borrowed, manage your current loans, or return books).<br />
3. <a href="https://www.adobe.com/cfusion/membership/index.cfm" rel="nofollow">Get an Adobe.com account</a> (If you create an Adobe account, you can access your library from a variety of locations. If not, your loans will be tethered to a specific computer or device.)<br />
4. <a href="http://archive.org/details/lendinglibrary" rel="nofollow">Find a book to borrow</a><br />
5. If a BookReader edition is available, you can read it instantly online in your web browser. Other formats will require that you download a file and open it in Adobe Digital Editions
        </p>
                <p>
          <b>
            <a name="1020"></a>
            Can I borrow books on my Ipad or Android tablet?          </b>
                  </p>
        <p>
          Yes! You can read our books using our BookReader via your browser or by using a reader app like Bluefire Reader or Overdrive Media Console (iPad) or Aldiko Book Reader or Overdrive Media Console (Android tablet). For more information on Bluefire, go to their site at <a href="http://bluefirereader.com" rel="nofollow">bluefirereader.com</a>. Before you start, register an Adobe ID. You'll need to do this once. <a href="https://www.adobe.com/account/sign-in.adobedotcom.html" rel="nofollow">If you don't have one, create one at this page</a>.
<br /><br />
Here are some step-by-step instructions on using Overdrive Media Console:<br />
 1. Make sure you have downloaded and installed the free app "Overdrive Media Console" on to your iPad<br />
 2. Find a book you'd like to borrow; feel free to try a sample book that is small <a href="https://archive.org/details/mrfunnyharg00harg" rel="nofollow">such as this one</a><br />
 3. Click on the "ebook" link under the "borrow" heading on the right<br />
 4. Log in if you have not logged in to archive.org<br />
 5. Choose one of the download options. Please note: Overdrive Media Console <em>can not read PDFs</em>.
<br /><br />
Here are step-by-step instructions for Aldiko Reader:
<br /><br />
 1. Download and install Aldiko Book Reader from <a href="https://play.google.com/" rel="nofollow">Google Play Store</a>.<br />
 2. Open Aldiko, Select Other Catalogs under the Get Books section of the menu. <br />
 3. Select My Catalogs at the top and tap New Catalog on the green bar at the top. <br />
 4. Create an entry for the archive.org using openlibrary.org for the URL. Tap on the library and sign in. <br />
 5. When you have found a book you like, check it out. When the next screen comes up, select the pdf or epub version. You will then be prompted to enter your Adobe id and password. Your book will then download into Aldiko and you can open it and read it at your leisure. 
<br /><br />
The only downside to this process is that books can not be returned early via non-Adobe applications, so you'll just have to let them expire or we can return them early if you need to free up space on your loans list.
<br /><br />
<b>How do I borrow books to read on my Nook?</b>
<br /><br />
You will need <a href="http://www.adobe.com/products/digitaleditions/">Adobe Digital Editions</a>(ADE) to use your Nook. Once you have ADE follow these instructions:
<br /><br />
1. Quit Digital Editions, if it’s running<br />
2. Plug in the Nook, and start ADE<br />
3. ADE should recognize the Nook, and offer to associate with it. Make sure you can see the Nook under ‘Bookshelves’ on the left. Ok!<br />
4. Go to the <a href="https://archive.org/details/lendinglibrary">Lending Library</a> and borrow a book in pdf or epub format.<br />
5. If ADE is working properly, you should see your book!<br />
6. Next, go to ‘Library View’ in ADE – in the upper left.<br />
7. In the Library View, drag your new book over to the Nook icon under ‘Bookshelves.’<br />
8. Quit ADE and eject your Nook.
<br /><br />
To read on the Nook:
<br /><br />
1. Go to your Library (on a Nook Color, do this by touching the bottom of the touchscreen)<br />
2. Go to ‘my files’ – at the top – and open ‘Digital Editions’<br />
3. Open your book! (if it says ‘sorry, can’t open this book’, try again.)
<br /><br />
To return your book early so that others can borrow it:
<br /><br />
1. Quit ADE if it’s running<br />
2. Plug in your Nook and start ADE<br />
3. Open ‘Library View’ and click ‘All Items’ on the left<br />
4. On your book icon, there’s a drop down menu (a little triangle) in the upper left – select ‘Return Borrowed Item’<br />
5. Open the Nook, in the bookshelf area on the left.<br />
6. On your book icon – select ‘Return Borrowed Item’.<br />
7. Your book should now be available to borrow again!
<br /><br />
If you run into trouble, <strong><a href="http://bookclubs.barnesandnoble.com/t5/NOOK-Simple-Touch-Support/Using-Adobe-Digital-Editions-ADE-Library-Books-and-Vendors-other/td-p/575651">here's a forum</a></strong> on the Barnes and Noble site about how to get ADE working with the Nook.<br />
<a href="http://bookclubs.barnesandnoble.com/t5/NOOK-E-Ink-Support/Using-Adobe-Digital-Editions-ADE-Library-Books-and-Vendors-other/td-p/575651" rel="nofollow">Here are instructions</a> on how to do this from Barnes and Noble.        </p>
                <p>
          <b>
            <a name="1013"></a>
            Can I return a library book early?          </b>
                  </p>
        <p>
          Yes, usually. If you borrowed a BookReader edition, simply return it from <a href="https://archive.org/account/loans.php" rel="nofollow">your Loans page</a>.
<br /><br />
If you downloaded another type of ebook, you'll need to do that through Adobe Digital Editions. If you checked out your book with other software like Overdrive Media Console or Bluefire Reader, you will not be able to return your book early.
<br /><br />
In Adobe Digital Editions, look for your "library". That's the book spines icon in the top left corner of the application (1). Once you're in your library, click on the menu for book you'd like to return which is behind the tiny triangle that appears by the book cover (2) and select "Return Borrowed Item" from the menu (3). This image will show you where to look.<br /><br /><img src="/download/olimages/ADE-screenshot.jpg" alt="[screenshot of Adobe Digital Editions library page]" title="[screenshot of Adobe Digital Editions library page]" /><br />
<br /><br />
You may also be able to right-click on your item and select "Return Borrowed Item" from the contextual menu. Here is a screenshot of this option.
<br /><img src="/download/olimages/ADE2.0-screenshot.jpg" border="1" alt="ADE2.0-screenshot.jpg" /><br />
<br /><br />
If you used other software to access your book, you may not be able to return it early but the item will be automatically returned at the end of the loan period. Please contact us if you are having trouble returning your items.        </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#158">Why not Squid or mod_proxy?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#157">Why FreeCache?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#159">Why not BitTorrent?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#160">What files are being served by FreeCache?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#166">What's a good download manager?</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="FreeCache"></a>
          FreeCache        </h1>
                <p>
          <b>
            <a name="158"></a>
            Why not Squid or mod_proxy?          </b>
                  </p>
        <p>
          Both Squid and mod_proxy are great for reducing the load on web servers, and we encourage everybody to use them. The disadvantage of these caching proxies are that they only work "vertically", i.e., they reduce the bandwidth downstream from the originating web site to the users' browsers. That web site still gets 1 download per (non-cascading) proxy. The FreeCache system works more "horizontally", i.e., FreeCaches fill themselves up from neighboring FreeCaches if at all possible. Hence, the load on the originating web site is much lower. 

FreeCache and caching proxies are complementary technologies. Both can be used to reduce the impact on web sites.        </p>
                <p>
          <b>
            <a name="157"></a>
            Why FreeCache?          </b>
                  </p>
        <p>
          FreeCache is a demand-driven, distributed caching system. Cooperating caches exchange files without burdening the original site too much.          </p>
                <p>
          <b>
            <a name="159"></a>
            Why not BitTorrent?          </b>
                  </p>
        <p>
          BitTorrent is good and similar to FreeCache in that it balances download "horizontally". BitTorrent uses other BitTorrent clients for this balancing; these clients often become un-available after a particular file is not popular anymore. 

The FreeCache system utilizes permanent FreeCaches that don't go away (although particular files get flushed out after a while).

Unlike BitTorrent, the FreeCache system is transparent to the end-user. No new client or server software is required, and the files do not need to be converted. To offer a file via the FreeCache system, all you need to do is prefix the URL with http://freecache.org/        </p>
                <p>
          <b>
            <a name="160"></a>
            What files are being served by FreeCache?          </b>
                  </p>
        <p>
          FreeCache can only serve files that are on a web site. If the link to a file on that web site goes away, so will the file in the FreeCaches. Also, there is a minimum size requirement. We don't bother with files smaller than 5MB, as the saved bandwidth does not outweigh the protocol overhead in those cases.        </p>
                <p>
          <b>
            <a name="166"></a>
            What's a good download manager?          </b>
                  </p>
        <p>
          We like <a href="http://www.gnu.org/software/wget/wget.html">wget</a>, because you can tell it to play nice and go slow. It's highly configurable and very powerful. Wget runs on all Unix platforms (incl. Mac OS X), and it comes standard with <a href="http://www.cygwin.com/">Cygwin</a> on Windows.

If you prefer something graphical, <a href="http://www.mozilla.org/">Mozilla</a>'s built-in download manager works fine.         </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#104">What is DocuComp?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#105">What do I need I to know to use DocuComp in the WayBack Machine?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#106">What Archive Pages are comparable?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#107">Why should I compare results of past Web pages?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#109">How are images compared?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#110">Some images are missing in my comparison?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#111">Certain links or actions are not working in the comparison results?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#112">How can I report problems?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#114">Guidelines for Press, Magazines and General Media</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#108">Where can I find out more about DocuComp?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#113">Can I copy and use my results?</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="DocuComp"></a>
          DocuComp        </h1>
                <p>
          <b>
            <a name="104"></a>
            What is DocuComp?          </b>
                  </p>
        <p>
          DocuComp is a sophisticated technology that compares inserted, deleted, replaced and moved text and content in Web pages. It's patented algorithm has been specially designed and licensed for use in the Wayback Machine.        </p>
                <p>
          <b>
            <a name="105"></a>
            What do I need I to know to use DocuComp in the WayBack Machine?          </b>
                  </p>
        <p>
          You only need to know the basic functions of the Wayback Machine. Begin by typing an URL into the Wayback Machine and hit the 'Take Me Back' button. Once you've found your choices on the results page, click the 'Compare Archive Pages' button in the upper right hand corner of the page. The reloaded page will have a series of check-boxes before each page date. Check any two dates and select the 'Compare two dates' button in the upper left-hand corner of the screen. The system is designed to automatically generate results for any URL's indexed by the Wayback Machine.         </p>
                <p>
          <b>
            <a name="106"></a>
            What Archive Pages are comparable?          </b>
                  </p>
        <p>
          You can compare any two pages from the Archive's library dating from 1996 to the present (approximately 55 billion pages).         </p>
                <p>
          <b>
            <a name="107"></a>
            Why should I compare results of past Web pages?          </b>
                  </p>
        <p>
          Access to the Archive's Collections is provided at no cost to you and is granted for scholarship and research purposes only. The DocuComp feature is intended to provide interesting insight into how content on pages in every field-- from the government to entertainment to business sites-- changes over time.        </p>
                <p>
          <b>
            <a name="109"></a>
            How are images compared?          </b>
                  </p>
        <p>
          When compared pages contain different images, only the new (or latest) set of images is shown. Images that were either changed or removed are not displayed in the comparison results.        </p>
                <p>
          <b>
            <a name="110"></a>
            Some images are missing in my comparison?          </b>
                  </p>
        <p>
          In certain cases, images within the Web pages are not available. Not all images are archived nor are retrievable from the original site. If they no longer exist on the original site then the images will not be available and not displayed within the archived pages.        </p>
                <p>
          <b>
            <a name="111"></a>
            Certain links or actions are not working in the comparison results?          </b>
                  </p>
        <p>
          Links to other pages may not be live if those pages (or links) no longer exist and are not in the archive library. Also, javascript enabled links and actions are disabled in the comparison results to prevent errant scripts from being run.        </p>
                <p>
          <b>
            <a name="112"></a>
            How can I report problems?          </b>
                  </p>
        <p>
          After comparing two pages, the upper frame on the results page includes a hyperlink to report results which return any page faults. By clicking this hyperlink, an automatic error report is generated to both the Internet Archive webmaster and DocuComp's technical team. If you wish, there is an additional help screen to describe the issue. Please keep in mind that with over two billion pages to index and compare, not all being created alike; some pages will differ greatly and not have a common frame of reference to effectively compare.          </p>
                <p>
          <b>
            <a name="114"></a>
            Guidelines for Press, Magazines and General Media          </b>
                  </p>
        <p>
          DocuComp is a registered trademark of Advanced Software, Inc. Please contact the company at (866) 329-7480 or <A HREF="mailto:info@docucomp.com">info@docucomp.com</A> for background information on the company's history, technology data, or to schedule executive interviews.        </p>
                <p>
          <b>
            <a name="108"></a>
            Where can I find out more about DocuComp?          </b>
                  </p>
        <p>
          Please visit the <a href="http://www.docucomp.com">www.docucomp.com</a> site. DocuComp is a widely-used technology that is licensed by it's parent company, Advanced Software, into many of the software products and content management systems available today. Formerly a standalone application for Advanced Software, the company now focuses exclusively on licensing the DocuComp technology and patent to software vendors.        </p>
                <p>
          <b>
            <a name="113"></a>
            Can I copy and use my results?          </b>
                  </p>
        <p>
          The results of any comparison done on the Internet Archive site are governed by the terms of use listed at: <a href="/about/terms.php">https://www.archive.org/about/terms.php</a>. Additionally, any use of the DocuComp trademark or logo without express written permission by Advanced Software, Inc and any of it's affiliates is prohibited by law.          </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#223">How can I add my music, movies, or text?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#301">How does the Share button work?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#224">I want to add LOTS of individual items to the archive, how do i do that?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#132">How can I report an error for my item?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#262">How can I make changes to my item?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#169">Can you tell me a bit more about choosing a license?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#177">How should I name the files for movies I upload?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#216">During upload, I get an error message about 'illegal characters' or 'file name prohibited.' What does this mean?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#231">What kinds of formats do you want me to use for uploading?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#239">How should I name the audio files I upload?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#264">How can I take my files off the site?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#303">What is the relationship between Internet Archive and OurMedia?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#261">What languages are supported by Archive.org?  How can I use accented or special characters in my title or description?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#267">I just uploaded my files, and I got an error message that says there's a problem with my metadata - but I haven't added any metadata yet!</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="Uploading_Content"></a>
          Uploading Content        </h1>
                <p>
          <b>
            <a name="223"></a>
            How can I add my music, movies, or text?          </b>
                  </p>
        <p>
          <p>You may contribute content to the Internet Archive if it's in the public domain or if you own the rights to it. If you own the rights, we recommend that you choose a <a href="http://creativecommons.org/license/"><b>Creative Commons license</b></a> for it so that others will know how they may (or may not) use it.  You can choose a type of Creative Commons license during your upload process.</p>

<p>Please note that if you wish to be contacted with inquiries regarding your item, you'll need to supply public contact information.  Some chose to provide a web address, mailing address, or other means of contact in the description text for the item.  

<p>See also <a href="https://www.archive.org/about/faqs.php#Rights">https://www.archive.org/about/faqs.php#Rights</a>

<p><b>You can contribute movies, audios, or books</b> to the archive through the upload tool.  Click the "Upload" button near the upper right-hand corner of the site, or click <a href="https://www.archive.org/create/">here</a>. 

<p>For <b>books</b>, please see <a href="https://www.archive.org/about/faqs.php#195">https://www.archive.org/about/faqs.php#195</a>

<br><br>
        </p>
                <p>
          <b>
            <a name="301"></a>
            How does the Share button work?          </b>
                  </p>
        <p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">This exciting new uploading feature is a beta release.  This means that it's currently under development, and we've made this early release because it improves the upload process and we can't wait for you to start working with it!  
</font></p>

<p><font face="Arial, Helvetica, sans-serif" size="2">To use the new beta uploader: 

</font></p><ul>
<font face="Arial, Helvetica, sans-serif" size="2"><li>First click the "Upload" button near the upper right-hand corner of the site, or click <a href="https://www.archive.org/create/">here</a>.</li>

<li>Now you can see the <strong>Share</strong> button.  </li>

<li>Click the <strong>Share</strong> button to browse for the media you want to upload. You can select more than one file, or you can click the Share button again to select additional files. </li>

<li>Archive.org will automatically detect which media collection (movies, audio, texts, or other) your item belongs to, according to the type of the first uploaded file. </li>

<li>You also have the option to click the link to change the file type if needed.  </li>

<li>As the file(s) upload, enter the information about your file in the given fields.</li>

<li>When everything is complete, click the <strong>"Share my File(s)"</strong> button at the bottom of the page to create your item page on Archive.org. </li>
</font></ul>


<p><font face="Arial, Helvetica, sans-serif" size="2"><b>You can track the progress of your items</b> in our <a href="https://www.archive.org/catalog.php?justme=1">catalog</a>.</font></p>

<p><font face="Arial, Helvetica, sans-serif" size="2">We accept audio, video, and text files.</font></p>

        </p>
                <p>
          <b>
            <a name="224"></a>
            I want to add LOTS of individual items to the archive, how do i do that?          </b>
                  </p>
        <p>
          <p>If you have a large collection of related items in single media type, like a radio show for example, please contact the Internet Archive.  You can email our collections staff at info at archive.org.  Please put start your subject line with "Collections:".   

<p>Be sure to include the details of your collection; we want to know how many items you have, what format they are in as well as any general information you can give us about the collection.  

<p>In general, collection pages are created once the number of uploaded items has reached 50 or more.          </p>
                <p>
          <b>
            <a name="132"></a>
            How can I report an error for my item?          </b>
                  </p>
        <p>
          <p>First, we recommend that you search the Forums.  Many common problems have already been answered there, and you'll have an answer much more quickly.</p>

<p>If that doesn't work, you can email info at archive.org.  Be sure to include a link to your item's details page.  Report the details about the problem you are experiencing - the more details you provide, the more readily we can help you.</p>

        </p>
                <p>
          <b>
            <a name="262"></a>
            How can I make changes to my item?          </b>
                  </p>
        <p>
          <p>If you want to <b>change your item's metadata</b> (like title, description, file formats and titles, running time, language, etc.), <b>or change the files in your item</b> (remove files, upload new/more files, rename files, etc.), you can do this using <b>the new "Edit Item!" link</b>.  Here's how: 
<ul>
<li>Make sure you're logged in with the account you used to upload the item<li>
<li>Go to your item's details page</li>
<li>Click the "Edit item" link in the lower left box.</li>
<li>Select the "change the information" link</li>
</ul>
Your changes will appear in 20-30 minutes.  

<p>If you have uploaded new files and you want us to make derivative files (smaller, more compressed versions), you will need to do one more thing.
<ul>
<li>Click "Edit item"</li>
<li>Select the "change the information" link</li>
<li>Click "Item Manager"</li>
<li>Click the "derive" button</li>
</ul>
</p>

<p><b>How can I take my files off the site? </b><br>
<a
href="http://www.archive.org/about/faqs.php#264">http://www.archive.org/about/faqs.php#264</a>

<p>If you would like us to take down an item you have posted, please send an email to info [AT] archive [DOT] org. Please include the exact URLs of the items. Your email must come from the same email address you used to upload the item. This is the only way we can tell that you are the owner of the item.        </p>
                <p>
          <b>
            <a name="169"></a>
            Can you tell me a bit more about choosing a license?          </b>
                  </p>
        <p>
          From the <a href="http://creativecommons.org">Creative Commons</a> website: "Creative Commons licenses help you share your work but while keeping your copyright. Other people can copy and distribute your work, but only on certain conditions."<br/><br/>

You can choose a license to associate with your contribution and this license will be linked to when users see the details page.        </p>
                <p>
          <b>
            <a name="177"></a>
            How should I name the files for movies I upload?          </b>
                  </p>
        <p>
          <p>Take for example a movie called My Home Video.  The identifier (AKA base name) for this movie should be something like MyHomeVideo.  The naming convention for the files depends on the encoding.  </p>

<p>
MPEG-2:<br/>
MyHomeVideo.mpeg
</p>
<p>
MPEG-1:<br/>
MyHomeVideo.mpg
</p>
<p>
DivX:<br/>
MyHomeVideo.avi
</p>
<p>
QuickTime:<br/>
MyHomeVideo.mov
</p>
<p>
Windows Media:<br/>
MyHomeVideo.wmv
</p>
<p>
Real Media:<br/>
MyHomeVideo.rm
</p>
<p>
MPEG-4:<br/>
MyHomeVideo.mp4
</p>

<p>If you know the bitrate of the encoding (for QuickTime, Windows Media, Real Media, or MPEG-4), please include in the file name as such (using 64 as the bitrate and QuickTime as the format, for example):</p>

<p>MyHomeVideo_64kb.mov</p>

        </p>
                <p>
          <b>
            <a name="216"></a>
            During upload, I get an error message about 'illegal characters' or 'file name prohibited.' What does this mean?          </b>
                  </p>
        <p>
          The folder or files that you are attempting to upload have characters in the name that cause problems with the system - so we have designated them "illegal".  This includes the following characters in the name:<br><br>

<b>* ( ) { } [ ] / \ $ % @ # ^ & | < > ' ~ ` ! ? +</b><br><br>

In addition, files and folders may <u>not</u> have spaces in their names.<br><br>

You will need to remove any of these illegal characters by renaming the file(s) in order for the system to accept your contribution.        </p>
                <p>
          <b>
            <a name="231"></a>
            What kinds of formats do you want me to use for uploading?          </b>
                  </p>
        <p>
          <p>The Internet Archive strives to archive content in open formats that are
friendly to long-term storage and access.  In addition to affecting
long-term storage and access, giving us media in these formats will assure
that they are accessible now, since many problems with long-term
accessibility such as DRM and propriatary codecs also cause problems
today.</p>

<p>However, if you have content that is not available in an open/recommended
format (see below), we will still happily archive it.  Our systems are not
tied to specific media formats and in fact are capable of archiving any
type of digital data that can be represented as a file.</p>

Format Recommendations: </p>

We encourage users making contributions to the Archive to create as high
quality versions of their media as possible.  As we know access is
important and not everyone has a high speed connection, we will take these
archivable copies and create much smaller version for users with slow
connections.  Remember, a WAV file may seem big, but it won't be in 5
years.  Further, you can always make lower quality files (e.g. mp3s) from
higher quality files, but cannot go the other way. </p>

For video we typically recommend MPEG2 (DVD quality), or if you do not
have MPEG2, MPEG1 or MPEG4. </p>

For audio we recommend WAV or FLAC (preferably 24 bit).  </p>

For text we recommend plain text, xml, or pdfs.
        </p>
                <p>
          <b>
            <a name="239"></a>
            How should I name the audio files I upload?          </b>
                  </p>
        <p>
          <p>Take, for example, an audio called My Music. The identifier for this audio should be something like MyMusic. The naming convention for the files depends on the encoding.</p>


<p>
MP3:<br/>
MyMusic.mp3
</p>
<p>
WAVE:<br/>
MyMusic.wav
</p>
<p>
Flac:<br/>
MyMusic.flac
</p>
<p>
Shorten:<br/>
MyMusic.shn
</p>
<p>
Ogg Vorbis:<br/>
MyMusic.ogg
</p>
<p>
Windows:<br/>
MyMusic.wma
</p>
<p>
Real Media:<br/>
MyMusic.ra
</p>

<p>If you know the bitrate of the encoding, please include it in the file name. For example:</p>

<p>MyMusic_64kb.mp3</p></p><p style="text-align:right;"><a 
        </p>
                <p>
          <b>
            <a name="264"></a>
            How can I take my files off the site?          </b>
                  </p>
        <p>
          <p>If you would like us to take down an item you have posted, please send an email to info [AT] archive [DOT] org.  Please include the exact URLs of the items.  Your email <b>must</b> come from the same email address you used to upload the item.</p>        </p>
                <p>
          <b>
            <a name="303"></a>
            What is the relationship between Internet Archive and OurMedia?          </b>
                  </p>
        <p>
          The OurMedia collection on archive.org can be found at <a href="http://www.archive.org/details/ourmedia">http://www.archive.org/details/ourmedia</a>. Users can upload to this section directly from the OurMedia site on <a href="http://www.ourmedia.org/upload">this page</a>. If you have questions or concerns about your item(s) in OurMedia, please contact them <a href="http://www.ourmedia.org/contact">directly</a>.        </p>
                <p>
          <b>
            <a name="261"></a>
            What languages are supported by Archive.org?  How can I use accented or special characters in my title or description?          </b>
                  </p>
        <p>
          <p><b>What languages are supported by Archive.org?</b>

<p>Archive.org supports all metadata about items in just about any language so long as the characters are UTF8 encoded.

<p>(1) example of language:korean  <br>
<a href="https://www.archive.org/details/Shall_We_Protest_the_Candlelight_Documentary-iso">https://www.archive.org/details/Shall_We_Protest_the_Candlelight_Documentary-iso</a>

<p>(2) example of language: Arabic  <br>
<a href="https://www.archive.org/details/ktb_tragm_rgal_pdfbook_ara">https://www.archive.org/details/ktb_tragm_rgal_pdfbook_ara</a>

<p><b>Filename support:</b>
<p><b>Support for Filenames is limited to pretty basic ASCII characters</b>, like <br>
A-Z <br>
a-z <br>
0-9 <br>
_ <br>
- <br>
.

<p>Additional character support for filenames is not an area under development at this time.
<br><br>

<p><b>How can I use accented or special characters in my title or description?</b>

<b><p>You can use accented and other special characters in your item text and file titles, but you need to make sure you use the xml-safe code for those characters instead of typing them directly into the forms.</p>

<p><i>Typing accented characters directly into forms can break the xml for your item, making your files unavailable through the site.</i>  </p>

<p>Instead, you'll want to use a special code to represent those letters.  There are some examples in the table below, but you can find a complete listing of these codes on <a href="http://en.wikipedia.org/wiki/List_of_XML_and_HTML_character_entity_references">http://en.wikipedia.org/wiki/List_of_XML_and_HTML_character_entity_references</a> - you'll use the number in parentheses in the "Unicode code point" column.</p></b>

<p>Here are some common accented and special characters and what you should replace them with:</p>

<table border="1">
<tr>
<td><b>To Make This Character...</b></td>
<td><b>Replace It With This Code</b></td>
</tr>
<tr>
<td>&</td>
<td>&amp;</td>
</tr>
<tr>
<td>à</td>
<td>&agrave;</td>
</tr>
<tr>
<td>À</td>
<td>&Agrave;</td>
</tr>
<tr>
<td>á</td>
<td>&aacute;</td>
</tr>
<tr>
<td>Á</td>
<td>&Aacute;</td>
</tr>
<tr>
<td>è</td>
<td>&egrave;</td>
</tr>
<tr>
<td>È</td>
<td>&Egrave;</td>
</tr>
<tr>
<td>é</td>
<td>&eacute;</td>
</tr>
<tr>
<td>É</td>
<td>&Eacute;</td>
</tr>
<tr>
<td>ñ</td>
<td>&ntilde;</td>
</tr>
<tr>
<td>Ñ</td>
<td>&Ntilde;</td>
</tr>
</table>

<p>So to write the word <b>cafÃ©</b> you would actually write <b>caf&eacute;</b> - you replace the letter Ã© with the code &eacute;</p>

<p>There are many, many more codes than the ones listed above, of course.  You can find more at <a href="http://en.wikipedia.org/wiki/List_of_XML_and_HTML_character_entity_references">http://en.wikipedia.org/wiki/List_of_XML_and_HTML_character_entity_references</a>.  </p>        </p>
                <p>
          <b>
            <a name="267"></a>
            I just uploaded my files, and I got an error message that says there's a problem with my metadata - but I haven't added any metadata yet!          </b>
                  </p>
        <p>
          <p>When you create an item, we "check out" a directory for you to upload files into.  When you're done uploading, you "check in" the directory (by cicking a link on the check out page, or clicking the "click here when done" icon).</p>

<p>Checking in an item lets us know you're done uploading, and the first thing we do is back up your files to a second server (so we'll have two copies of everything).  Sometimes, when it's taking longer than usual to complete this backup, you'll get an error message that says there's a problem with your metadata.  If you wait a little while (usually just a few minutes, but occasionally longer), you should be able to continue the upload process without any trouble.</p>

<p>If you uploaded metadata with your files, or you've gotten this error <b>after</b> you've added metadata (title, description, file titles, etc.) then you may have a problem.  Usually an item breaks because you used <a href="https://www.archive.org/about/faqs.php#261">special characters</a> that broke the xml files for your item.  Please feel free to use the link on the error page to report the problem to us and we'll try to help you fix it.</p>        </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#44">How did you digitize the films?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#51">Do I need to credit the Internet Archive and Prelinger Archives when I reuse these movies?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#52">Do I need to inform the Internet Archive and/or Prelinger Archives when I reuse these movies?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#53">How can I get access to these movies on videotape or film?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#45">An article on re-coding Prelinger Archive films to SVCD so you can watch them on your DVD player.</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#172">What parameters were used when making the Real Media files on the website?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#197">Are there restrictions on the use of the Prelinger Films?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#198">Can you point me to resources on the history of ephemeral films?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#199">Why are there very few post-1964 movies in the Prelinger collection?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#283">For more information...</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="Prelinger_Movies"></a>
          Prelinger Movies        </h1>
                <p>
          <b>
            <a name="44"></a>
            How did you digitize the films?          </b>
                  </p>
        <p>
          The <a href=http://www.prelinger.com>Prelinger Archives</a> films are held in original film form (35mm, 16mm, 8mm, Super 8mm, and various obsolete formats like 28mm and 9.5mm). Films were first transferred to Betacam SP videotape, a widely used analog broadcast video standard, on telecine machines manufactured by Rank Cintel or Bosch. The film-to-tape transfer process is not a real-time process: It requires inspection of the film, repair of any physical damage, and supervision by a skilled operator who manipulates color, contrast, speed, and video controls.</p><p>The videotape masters created in the film-to-tape transfer suite were digitized in 2001-2003 at Prelinger Archives in New York City using an encoding workstation built by <a href=http://www.coolstf.com/>Rod Hewitt</a>. The workstation is a 550 MHz PC with a <a href=http://www.futuretel.com>FutureTel</a> NS320 MPEG encoder card. Custom software, also written by Rod Hewitt, drove the Betacam SP playback deck and managed the encoding process. The files were uploaded to hard disk through the courtesy of <a href=http://www.flycode.com/>Flycode, Inc</a>.</p><p>More recently, Prelinger films have been digitized and uploaded by Skip Elsheimer at <a href=http://www.avgeeks.com>AV Geeks</a>.</p><p>The files were encoded at constant bitrates ranging from 2.75 Mbps to 3.5 Mbps. Most were encoded at 480 x 480 pixels (2/3 D1) or 368 x 480 (roughly 1/2 D1). The encoder drops horizontal pixels during the digitizing process, which during decoding are interpolated by the decoder to produce a 720 x 480 picture. (Rod Hewitt's site <a href=http://www.coolstf.com>Coolstf</a> shows examples of an image <a href=http://www.coolstf.com/mpeg/dn-regular.jpg>before</a> and <a href=http://www.coolstf.com/mpeg/dn-no-pan-scan.jpg>after</a> this process.) Picture quality is equal to or better than most direct broadcast satellite television. Audio </b>was encoded at MPEG-1 Level 2, generally at 112 kbps. Both the MPEG-2 and MPEG-4 movies have mono audio tracks.</p><p>To convert the MPEG-2 video to MPEG-4, we used a program called FlasK MPEG. This is an MPEG-1/2 to AVI conversion tool that reads the source MPEG-2 and outputs an AVI file containing the video in MPEG-4 format and audio in uncompressed PCM format. We then use a program called Virtual Dub that recompresses the audio using the MPEG-1 Level 3 (MP3) format. This process is automated by the software that runs the system.        </p>
                <p>
          <b>
            <a name="51"></a>
            Do I need to credit the Internet Archive and Prelinger Archives when I reuse these movies?          </b>
                  </p>
        <p>
          We ask that you credit us as a source of archival material, in order to help make others aware of this site. We suggest the following forms of credit:</p><ul><p>Archival footage supplied by the Internet Moving Images Archive (at archive.org) in association with Prelinger Archives</p></ul><p>or</p><ul><p>Archival footage supplied by the Internet Moving Images Archive (at archive.org)</p></ul><p>or</p><ul><p>"Archival footage supplied by archive.org"</p></ul>        </p>
                <p>
          <b>
            <a name="52"></a>
            Do I need to inform the Internet Archive and/or Prelinger Archives when I reuse these movies?          </b>
                  </p>
        <p>
          No. However, we would very much like to know how you have used this material, and we'd be thrilled to see what you've made with it. This may well help us improve this site. Please consider sending us a copy of your production (postal mail only), and let us know whether we can call attention to it on the site. Our address is:</p><ul><p>Rick Prelinger<br>PO Box 590622<br>San Francisco, CA 94159<br>United States</p></ul>        </p>
                <p>
          <b>
            <a name="53"></a>
            How can I get access to these movies on videotape or film?          </b>
                  </p>
        <p>
          Access to the movies stored on this site in videotape or film form is available to commercial users through <a href=http://www.archivefilms.com/>Archive Films</a>, representing Prelinger Archives for stock footage sales. Please contact Archive Films directly:</p><ul><p><a href=http://www.archivefilms.com/>Archive Films/Archive Photos</a><br>75 Varick Street<br>New York, NY 10013<br>United States<br>+1 (646) 613-4100 (voice)<br>+1 (646) 613-4140 (fax)<br>+1 (800) 876-5115 (toll free in the US)<br><a href=mailto:sales@archivefilms.com>sales@archivefilms.com</a></p></ul><p>Please visit us at <a href=http://www.prelinger.com/prelarch.html>www.prelinger.com/prelarch.html</a> for more information on access to these and similar films. Prelinger Archives regrets that it cannot generally provide access to movies stored on this Web site in other ways than through the site itself. We recognize that circumstances may arise when such access should be granted, and we welcome email requests. Please address them to <a href=mailto:footage@archive.org>Rick Prelinger</a>.</p><p>The Internet Archive does not provide access to these films other than through this site.        </p>
                <p>
          <b>
            <a name="45"></a>
            An article on re-coding Prelinger Archive films to SVCD so you can watch them on your DVD player.          </b>
                  </p>
        <p>
          See <a href=https://web.archive.org/web/20040411062949/http://www.moviebone.com/>archived version of www.moviebone.com/</a>
        </p>
                <p>
          <b>
            <a name="172"></a>
            What parameters were used when making the Real Media files on the website?          </b>
                  </p>
        <p>
          Rod Hewitt posted some very useful information <a href="/iathreads/post-view.php?id=9819">here</a>        </p>
                <p>
          <b>
            <a name="197"></a>
            Are there restrictions on the use of the Prelinger Films?          </b>
                  </p>
        <p>
          There are no restrictions.  You are warmly encouraged to download, use and reproduce these films in whole or in part, in any medium or market throughout the world. You are also warmly encouraged to share, exchange, redistribute, transfer and copy these films, and especially encouraged to do so for free.</p><p>

Any derivative works that you produce using these films are yours to perform, publish, reproduce, sell, or distribute in any way you wish without any limitations.</p><p>

Descriptions, synopses, shotlists and other metadata provided by Prelinger Archives to this site are copyrighted jointly by Prelinger Archives and Getty Images. They may be quoted, excerpted or reproduced for educational, scholarly, nonprofit or archival purposes, but may not be reproduced for commercial purposes of any kind without permission.</p><p>

If you require a written license agreement or need access to stock footage in a physical format (such as videotape or a higher-quality digital file), please contact <a href=http://www.gettyimages.com>Getty Images</a>. The Internet Archive does not furnish written license agreements, nor does it comment on the rights status of a given film above and beyond the Creative Commons license.</p><p>

We would appreciate attribution or credit whenever possible, but do not require it.</p><p>        </p>
                <p>
          <b>
            <a name="198"></a>
            Can you point me to resources on the history of ephemeral films?          </b>
                  </p>
        <p>
          See the bibliography and links to other resources at <a href=http://www.prelinger.com/ephemeral.html>www.prelinger.com/ephemeral.html</a>.
        </p>
                <p>
          <b>
            <a name="199"></a>
            Why are there very few post-1964 movies in the Prelinger collection?          </b>
                  </p>
        <p>
          Largely because of copyright law. While a high percentage of ephemeral films were never originally copyrighted or (if initially copyrighted) never had their copyrights properly renewed, copyright laws still protect most moving image works produced in the United States from 1964 to the present. Since the Prelinger collection on this site exists to supply material to users without most rights restrictions, every title has been checked for copyright status. Those titles that either are copyrighted or whose status is in question have not been made available. For information on recent changes in copyright law, see the circular <a href=http://www.loc.gov/copyright/circs/circ15a.pdf>Duration of Copyright</a> (in <a href=http://www.adobe.com/products/acrobat/readstep.html>PDF format</a>) published by the Library of Congress        </p>
                <p>
          <b>
            <a name="283"></a>
            For more information...          </b>
                  </p>
        <p>
          Check out our <a href="https://www.archive.org/iathreads/forum-display.php?forum=prelinger">Prelinger Archives Forum</a>        </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#258">Can I see a list of the most downloaded movies?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#259">Can I see a list of the most downloaded audio files?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#263">Can I search by Creative Commons License?</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="Search_Tips"></a>
          Search Tips        </h1>
                <p>
          <b>
            <a name="258"></a>
            Can I see a list of the most downloaded movies?          </b>
                  </p>
        <p>
          Every collection within Moving Images has a "Most Downloaded" list in the right-hand column of the page.

However, if you'd like to see a complete list of all of our most downloaded movies, <a href="https://www.archive.org/search.php?query=mediatype%3Amovies&sort=-%2Fadditional%2Fitem%2Fdownloads">click here</a>.        </p>
                <p>
          <b>
            <a name="259"></a>
            Can I see a list of the most downloaded audio files?          </b>
                  </p>
        <p>
          Every collection within Audio has a "Most Downloaded" list in the right-hand column of the page.

However, if you'd like to see a complete list of all of our most downloaded audio items, click one of the links below:

<ul>
<li><a href="https://www.archive.org/search.php?query=mediatype%3Aaudio&sort=-%2Fadditional%2Fitem%2Fdownloads">All Audio Items (not including Live Music Archive)</a>.</li>
<li><a href="https://www.archive.org/search.php?query=mediatype%3Aetree%20AND%20-mediatype%3Acollection&sort=-%2Fadditional%2Fitem%2Fdownloads">ALL  Live Music Archive concerts</a></li>
<li><a href="https://www.archive.org/search.php?query=-collection%3AGratefulDead%20AND%20collection%3Aetree%20AND%20-mediatype%3Acollection&sort=-%2Fadditional%2Fitem%2Fdownloads">LMA concerts (without the Grateful Dead)</a></li>
<li><a href="https://www.archive.org/search.php?query=collection%3AGratefulDead%20AND%20-mediatype%3Acollection&sort=-%2Fadditional%2Fitem%2Fdownloads">Grateful Dead only</a></li>
</ul>        </p>
                <p>
          <b>
            <a name="263"></a>
            Can I search by Creative Commons License?          </b>
                  </p>
        <p>
          <p>Yes, you can.  But it's a little complicated.</p>

<p>Here's how to break it down.  See the license types at <a href="http://creativecommons.org/about/licenses/meet-the-licenses">creative commons</a>.  When you want to find all of the items assigned a certain license by an uploading party, you'll plug their abbreviation for it into this search query:</p>

<p><span style="color: rgb(255, 0, 0);">/metadata/licenseurl:http*<b>abbreviation</b>/*</span></p>

<p>So if you're looking for Attribution Non-commercial No Derivatives (by-nc-nd), you'd put this in the search box:  <span style="color: rgb(255, 0, 0);">/metadata/licenseurl:http*<b>by-nc-nd</b>/*</span>   And you'd get about 33,000 items back.  </p>

<p>If you want to use this in combination with other queries, like "I want by-nc-nd items about dogs" you'd do this:  <span style="color: rgb(255, 0, 0);">/metadata/licenseurl:http*by-nc-nd/* <b>AND dog</b></span>  And you'd get 195 items.  The AND tells the search engine all the items returned should have that license AND they should contain the word dog.  AND has to be in all caps.</p>

<p>Just to make it easier, here are the basic searches:
<ul>
<li><a href="https://www.archive.org/search.php?query=%2Fmetadata%2Flicenseurl%3Ahttp%2Apublicdomain%2A">Public Domain</a></li>
<li><a href="https://www.archive.org/search.php?query=%2Fmetadata%2Flicenseurl%3Ahttp%2Aby-nc-nd/%2A">Attribution Non-commercial No Derivatives</a> (by-nc-nd)</li>
<li><a href="https://www.archive.org/search.php?query=%2Fmetadata%2Flicenseurl%3Ahttp%2Aby-nc-sa/%2A">Attribution Non-commercial Share Alike</a> (by-nc-sa)</li>
<li><a href="https://www.archive.org/search.php?query=%2Fmetadata%2Flicenseurl%3Ahttp%2Aby-nc/%2A">Attribution Non-commercial</a> (by-nc)</li>
<li><a href="https://www.archive.org/search.php?query=%2Fmetadata%2Flicenseurl%3Ahttp%2Aby-nd/%2A">Attribution No Derivatives</a> (by-nd)</li>
<li><a href="https://www.archive.org/search.php?query=%2Fmetadata%2Flicenseurl%3Ahttp%2Aby-sa/%2A">Attribution Share Alike</a> (by-sa)</li>
<li><a href="https://www.archive.org/search.php?query=%2Fmetadata%2Flicenseurl%3Ahttp%2Aby/%2A">Attribution</a> (by)</li>
</p>
        </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#66">How can I make links clickable in my posts?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#67">How can I format text in my posts</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#289">How do I subscribe/unsubscribe to a forum email list?</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="Forums"></a>
          Forums        </h1>
                <p>
          <b>
            <a name="66"></a>
            How can I make links clickable in my posts?          </b>
                  </p>
        <p>
          You may have noticed that some posts have highlighted links in them.  Internet Archive forums permit the use of HTML codes.  Suppose you want to make a link to the Internet Archive home page, one that looks like this: <a href="/">Internet Archive home page</a>.  To do this, you would enter the following HTML code: &lt;a href="http://www.archive.org"&gt;Internet Archive home page&lt;/a&gt;.        </p>
                <p>
          <b>
            <a name="67"></a>
            How can I format text in my posts          </b>
                  </p>
        <p>
          Since the Internet Archive forum system accepts HTML codes, you can make text bold, italic, underlined, or even colored by using normal HTML codes.  See <a href="http://www.webmonkey.com/2010/02/html_cheatsheet/">WebMonkey</a> for a list of HTML codes. 



        </p>
                <p>
          <b>
            <a name="289"></a>
            How do I subscribe/unsubscribe to a forum email list?          </b>
                  </p>
        <p>
          Next to all forums, you will see a small envelope. When logged in, you can click on this envelope which will allow you to subscribe or unsubscribe to any forum.        </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#180">How can I connect to SFLan?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#190">What about IP addresses?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#191">I still have more questions, what should I do?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#181">I live at 123 Main St at Crossing; do I have line of sight access to a node?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#182">What is the cost of a node?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#183">How can I get a node?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#184">If I get a node, can my neighbors connect also?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#185">What is included in the node?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#186">What are the power requirements of a node?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#188">What are the connection characteristics of the network?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#189">What is the percentage of uptime?</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="SFLan"></a>
          SFLan        </h1>
                <p>
          <b>
            <a name="180"></a>
            How can I connect to SFLan?          </b>
                  </p>
        <p>
          With a laptop: Be in the vicinity of a SFLan node. Associate with it: The SSID is sflanNN, where NN is the number of node, e.g. sflan13. No WEP. You'll get an IP number assigned via DHCP.

With a house: Contact us at info at archive dot org. (Please include your address and a phone number.) Find out if you have line of sight to another SFLan node, buy a node, and we'll put it on your roof.
        </p>
                <p>
          <b>
            <a name="190"></a>
            What about IP addresses?          </b>
                  </p>
        <p>
          SFLan uses real, routable IP addresses. These are usally given out dynically via DHCP. The nodes themselves use static addresses. We can also assign static addresses for servers.

For the techies: We use tunneling, layer 2 and layer 3 bridging in parts on the network to make it all appear as a "flat" LAN. There are pros and cons about this approach. It has worked best for us so far. However, it is a moving target, and might change in the future.        </p>
                <p>
          <b>
            <a name="191"></a>
            I still have more questions, what should I do?          </b>
                  </p>
        <p>
          SFLan is a work in progress. If you have more questions, try the SFLan forum.  If you still need help, write to info at archive dot org.         </p>
                <p>
          <b>
            <a name="181"></a>
            I live at 123 Main St at Crossing; do I have line of sight access to a node?          </b>
                  </p>
        <p>
          You can try netstumbler or kismet to look for a SFLan ssid.         </p>
                <p>
          <b>
            <a name="182"></a>
            What is the cost of a node?          </b>
                  </p>
        <p>
          The nodes cost $1100, which includes the price of parts and installation.  Discounts are potentially available depending on the location.         </p>
                <p>
          <b>
            <a name="183"></a>
            How can I get a node?          </b>
                  </p>
        <p>
          Send an email with your name, exact address and phone number to info at archive dot org.  Be sure to write "SFLan node" (or something similar) in the subject line.  The information will be passed on to our fantastic installation team who will contact you.        </p>
                <p>
          <b>
            <a name="184"></a>
            If I get a node, can my neighbors connect also?          </b>
                  </p>
        <p>
          Yes, a SFLan node can connect your neighbors and co-condo association members.         </p>
                <p>
          <b>
            <a name="185"></a>
            What is included in the node?          </b>
                  </p>
        <p>
          Most of our nodes are composed of two radios, but some have three.  The components are in a weather tight box with a four foot coax cable and two antennas attached.  The whole unit is mounted on your roof (generally) on a pole. There is a picture of our lovely 5'3" spokesmodel holding one here:  http://www.archive.org/iathreads/uploaded-files/AstridB-PICT0017.JPG        </p>
                <p>
          <b>
            <a name="186"></a>
            What are the power requirements of a node?          </b>
                  </p>
        <p>
          A node takes on average 5 watts.        </p>
                <p>
          <b>
            <a name="188"></a>
            What are the connection characteristics of the network?          </b>
                  </p>
        <p>
          There are no average characteristics, but 2MBs shared among 20 or so people would be an example.         </p>
                <p>
          <b>
            <a name="189"></a>
            What is the percentage of uptime?          </b>
                  </p>
        <p>
          SFLan is an experimental network, so the uptime varies.  Right now uptime averages around 90% or more.        </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#322">How do I find Torrents on the Archive?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#314">Can I download only part of an item using an Archive BitTorrent?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#315">My Torrent download never completes?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#316">My Torrent download never starts?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#317">How do I tell if a Torrent is being seeded?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#320">Does the Internet Archive run trackers?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#321">How do I use Torrents to upload to archive.org?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#312">How is the Internet Archive using BitTorrent?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#313">How to prevent an Archive Torrent from being made</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#318">Why is the Torrent link for an Item lined out (<del>Torrent</del>)?  </a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#319">What are peers, seeds, leechers, and snatches?</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="Archive_BitTorrents"></a>
          Archive BitTorrents        </h1>
                <p>
          <b>
            <a name="322"></a>
            How do I find Torrents on the Archive?          </b>
                  </p>
        <p>
          You can search and browse all our Torrents on the <a href="https://archive.org/details/bittorrent">Torrents collection homepage</a> (or one of the media-specific subcollections). 

<p>To narrow your own Search or Advanced Search query, add <i>format:"Archive BitTorrent"</i> to your search terms, e.g. <a href="https://archive.org/search.php?query=scifi%20AND%20mediatype:audio%20AND%20format:%22Archive%20BitTorrent%22">https://archive.org/search.php?query='scifi AND mediatype:audio AND format:"Archive BitTorrent"'</a>.

<p>The most popular and recent Torrents are available on each tracker's hotlists, e.g. <a href="http://bt1.archive.org/hotlist.php">bt1.archive.org Hot List</a>.        </p>
                <p>
          <b>
            <a name="314"></a>
            Can I download only part of an item using an Archive BitTorrent?          </b>
                  </p>
        <p>
          Yes, almost all contemporary BitTorrent clients allow you to select which files included in the Torrent are downloaded. And even when you download only one or some files, you get the speed advantages of using the format.

<p>Many show a list of the files contained in the Torrent, and both folders and individual files can be selected or deselected both before, and during, download. 

<p>It is recommend, in fact, that you deselect the top-level directory within the Torrent named <b>._____padding_file</b> if there is one, as this contains unnecessary (empty) Internet Archive padding files.        </p>
                <p>
          <b>
            <a name="315"></a>
            My Torrent download never completes?          </b>
                  </p>
        <p>
          Most likely, you have an out-of-date Torrent for the Item you are trying to download. The first thing to try is re-downloading the Items' Torrent, and trying again.

<p>Torrents for Items on the Internet Archive can become obsolete when the Item the Torrent is for changes. In that case, some or (more rarely) all of the files within the Torrent will fail to download completely.

<p>This is because our Torrents rely heavily on webseeding (download directly from our servers, when no peers have the files you are seeking). When files on our servers have changed since the Torrent was made, they will not match expected 'piece hashes'; <b><u>some BitTorrent clients (e.g. Transmission) will attempt to re-download file pieces from changed files over and over, forever, assuming there was an error in transmission, when in fact the file has changed</u></b>.

<p>Torrents that never download at all most likely are the result of a different problem, lack of client support for Getright-style webseeding.         </p>
                <p>
          <b>
            <a name="316"></a>
            My Torrent download never starts?          </b>
                  </p>
        <p>
          It's worth mentioning that <b><u>some BitTorrent clients take a very long time to begin downloading when relying on webseeding</u></b> (a common requirement when using Archive BitTorrents). At times downloads can take upwards of several minutes to start.

<p>We're not sure exactly why; we suspect those clients exhaust all other options, such as DHT, before falling back on webseeds. (We have observed this behavior with Transmission.) 

<p>If you download an up-to-date (current) Torrent from the Archive, and it loads into your BitTorrent client, but download <b><u>never</u></b> begins, the most likely cause is that you are using a BitTorrent client that does not support Getright-style webseeding.

<p>Our Torrents rely heavily on webseeding (download directly from our servers, when no peers have the files you are seeking). <b><u>Some BitTorrent clients (e.g. rTorrent) do not support Getright-style webseeding, and will not be able to download un-seeded Internet Archive Torrents</u></b>.

<p>At the moment, the only solution to this problem is to use a different client.

<p>Another possibility is that your Torrent file is out of date, because the Item has moved to a new server, and your client does not support redirection of our canonical webseeding URL (and no tracked or discoverable peers are seeding the Torrent). 

<p>In this case, the problem can be solved by re-downloading the Torrent file.        </p>
                <p>
          <b>
            <a name="317"></a>
            How do I tell if a Torrent is being seeded?          </b>
                  </p>
        <p>
          Current seed and leech counts are displayed for each Archive Torrent on the relevant Item details pages, in parenthesis next to the <i>Torrent</i> link. These values are cached for five minutes or so, and because clients do not always update our trackers regularly, they may be somewhat out of date.

<p>The number of seeders is shown first, and the number of leechers (downloaders without the complete Torrent) second. The seeder number includes 'webseeds,' however, which are only usable by BitTorrent clients which support Getright-style webseeding.        </p>
                <p>
          <b>
            <a name="320"></a>
            Does the Internet Archive run trackers?          </b>
                  </p>
        <p>
          Yes, Internet Archive torrents are tracked by <a href="http://bt1.archive.org/hotlist.php">bt1.archive.org</a> and <a href="http://bt2.archive.org/hotlist.php">bt2.archive.org</a>.

<p>We are using <a href="http://erdgeist.org/arts/software/opentracker/">opentracker</a>, which has proven to be highly scalable.

<p>Our trackers are closed (they track our only own torrents).        </p>
                <p>
          <b>
            <a name="321"></a>
            How do I use Torrents to upload to archive.org?          </b>
                  </p>
        <p>
          Retrieval of Torrents is not the best solution for uploading unless you already have an existing mechanism for creating and seeding Torrents.

<p>This capability is not intended as an alternative to our uploader. It merely enables the Archive to capture material already being distributed via BitTorrent.

<p>Torrent retrieval by the Archive works like this:

<p>If a valid .torrent file is uploaded (e.g. through our Uploader) into an item, when that item is derived, we will instantiate a BitTorrent client (Transmission) and attempt to retrieve the Torrent. If the Torrent is successfully retrieved, its contents will be added to the item. 'Valid' in this case means, well-formed and seeded.

<p>Our client will attempt to scrape any listed trackers to find seeding peers, but will also attempt to find peers via DHT and can fall back on Getright-style webseeding when possible.

<p>The Torrent file itself is leeched only long enough to retrieve the file; we do not seed the Torrent after retrieval.

<p>However, all items contents, including those retrieved through this method, are made available via the item's own Archive Torrent. (Because it contains additional contents, this Archive Torrent will, alas, have a different infohash from the original Torrent. So uploading a Torrent to the Archive does not make us a seeder of it.)

<p>Bonus feature: if you have only a magnet link, and not a Torrent file, you can create a dummy .torrent file by pasting that magnet link into a text file and naming it foo.torrent.

<p>If you upload this dummy Torrent file, we'll detect that you gave us a magnet link and take care of the rest.        </p>
                <p>
          <b>
            <a name="312"></a>
            How is the Internet Archive using BitTorrent?          </b>
                  </p>
        <p>
          <i>Downloading Internet Archive Content</i>
<p>As of summer 2012, the Internet Archive is beta-testing the distribution of our public collections via the BitTorrent protocol (as a supplement to traditional HTTP download). 

<p>Currently over <a href="https://archive.org/details/bittorrent">1.4 million Archive Items</a> are available via the BitTorrent protocol, comprising almost a petabyte of public domain materials.

<p> As testing continues, more and more content will be made available through Torrents. For the details, see the related FAQ, <a href="https://archive.org/about/faqs.php#313">Details of Archive-made Torrents</a>.

<p>BitTorrent download requires an up-to-date <a href="http://en.wikipedia.org/wiki/Comparison_of_BitTorrent_clients">BitTorrent client</a>.

<p>For general information on the BitTorrent protocol, see <a href="http://en.wikipedia.org/wiki/BitTorrent">Wikipedia</a> or <a href="http://www.bittorrent.com"/>BitTorrent.com</a>.

<p><i>Uploading BitTorrents to the Internet Archive</i>
<p>Starting in 2011, the Internet Archive began automatically retrieving BitTorrent files uploaded into most Community collections. 

<p>Uploading a Torrent provides a convenient way to upload many files or large contents, provided seeds (including webseeds) are available for the Torrent.        </p>
                <p>
          <b>
            <a name="313"></a>
            How to prevent an Archive Torrent from being made          </b>
                  </p>
        <p>
          Internet Archive BitTorrents are automatically made for community-contributed items in many collections, and automatically updated when item contents or metadata change.

<p>If you prefer that your item <b>not</b> have an Archive Torrent made for it; or that items within a collection you maintain do not, you can prevent Torrents from being made by including the following metadata tag in your item:

<p>   <b>noarchivetorrent=true</b>

<p>Note: adding this tag does not remove existing Torrents, those must be removed using the Item Manager item file management tool.

<p><i>For instructions on how to edit an item or collection's metadata, see the FAQ <a href="https://archive.org/about/faqs.php#262">Uploading Content</a>.</i>        </p>
                <p>
          <b>
            <a name="318"></a>
            Why is the Torrent link for an Item lined out (<del>Torrent</del>)?            </b>
                  </p>
        <p>
          While an Item is being updated, its Torrent link is <b><u>temporarily</u></b> disabled and shown as <del>Torrent</del>.
<p>Changes to an item usually render any existing Archive BitTorrent for it obsolete. Attempts to download obsolete Archive Torrents will usually fail, as described here: <a href="https://archive.org/about/faqs.php#315">My Torrent download never completes?</a>. (Technically, the problem is that when files within an Item change, they can no longer download correctly via webseeding because the piece hashes for updated files change).
<p><b><u>The Torrent link will return to normal when the Item finishes updating and the torrent is updated</u></b>. The Torrent link may be unavailable for a few minutes or a few hours depending on the size of the Item and how busy the Archive processing cluster is (in very rare cases, it might be disabled for a day or more).
<p>Note: obsolete torrents will continue to be tracked by Archive trackers for some time, but will only be retrievable when seeded by peers who have downloaded the referenced version of the item.        </p>
                <p>
          <b>
            <a name="319"></a>
            What are peers, seeds, leechers, and snatches?          </b>
                  </p>
        <p>
          BitTorrent is a peer-to-peer file-sharing protocol facilitated by centralized trackers. The Internet Archive runs several BitTorrent trackers to allow for peer discovery.
<p>Archive trackers track (<u><strong>but do not log or otherwise record</strong></u>) which peers have pieces of which Torrents; real-time statistics are summarized on <a href="http//bt1.archive.org/hotlist.php">tracker hotlists</a> for each of our Trackers.
<p><u>Internet Archive tracker statistics of interest include</u>:
<ul>
<li><strong>Peers</strong>: the total number of clients known by the tracker to have pieces of a Torrent, i.e. the sum of seeds and leechers.
<br><br></li>
<li><strong>Seeds</strong>: the number of clients known by the tracker to have <i>all</i> of the pieces of a Torrent available, i.e. those which have downloaded the entire Torrent but remain online.
<br><br></li>
<li><strong>Leechers</strong>: the number of clients known by the tracker to have <i>some</i> of the pieces of a Torrent available, i.e. those currently downloading the Torrent.
<br><br></li>
<li><strong>Snatches</strong>: the number of clients known by the tracker to have downloaded a given Torrent, but which are not currently seeding it.
</li>
</ul>
<p>Note: Internet Archive seeder and peer counts include webseeds; these seeds are available only when using clients that support Getright-style webseeding.        </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#285">What is non-Commercial Use?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#300">How can I contact the person / group who uploaded an item?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#284">Can I use this ____  for ____ ?</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="Rights"></a>
          Rights        </h1>
                <p>
          <b>
            <a name="285"></a>
            What is non-Commercial Use?          </b>
                  </p>
        <p>
          What is non-Commercial Use?  Please see 

  <a href="/iathreads/post-view.php?id=111591">
https://www.archive.org/iathreads/post-view.php?id=111591</font> 
                    </a>

<p>
<p>
A link the <b>Terms of Use</b> for Archive.org is at the bottom of each page.  
<br><p>        </p>
                <p>
          <b>
            <a name="300"></a>
            How can I contact the person / group who uploaded an item?          </b>
                  </p>
        <p>
          Internet Archive is unable to release any contact information for patrons.  However, it may be worth your while to post a review for the item in question - this automatically contacts the uploader's account, notifying them that their upload has been reviewed.  You could pose queries/requests for information therein.          </p>
                <p>
          <b>
            <a name="284"></a>
            Can I use this ____  for ____ ?          </b>
                  </p>
        <p>
          <p>
Internet Archive does not itself seek to limit use of its digital materials.  However, we cannot give ironclad guarantees as to the copyright status of items in our Collections and cannot guarantee information posted on itemsâ€™ details or collection pages regarding copyright or other intellectual property rights.  Our terms of use (https://www.archive.org/about/terms.php) require that users make use of Internet Archive's Collections at their own risk and ensure that such use is non-infringing and in accordance with all applicable laws. 
<p>
The person who uploads an item often provides information related to use rights, either by way of directly entering it in the description field or by selection of a Creative Commons license.  The latter, if included by the uploader, will be viewable via a <a href="
http://creativecommons.org/license/"><b>Creative Commons</b></a> logo on the details page, which serves as a link to a description of the specific type of license that the uploader has assigned.  
<p>
One way to attempt to contact an uploader about information that they have posted is to post a review to the item.

<p>The Internet Archive follows the <a href="http://www2.sims.berkeley.edu/research/conferences/aps/removal-policy.html">Oakland Archive Policy</a> for Managing Removal Requests And Preserving Archival Integrity.  

<p>
<p>
You may also find these resources helpful:<p>
<b>
<a href="http://creativecommons.org">CreativeCommons.org</a>
<p>
Chilling Effects Clearinghouse
<a href="http://www.chillingeffects.org">Chilling Effects Clearinghouse</a>
<p>
Electronic Frontier Foundation
<a href="http://www.eff.org">Electronic Frontier Foundation</a>
</b>
<p>
<p>
Please see also: 
<p><p>
<b>Who owns the rights to these movies?</b><p>
<a href="https://www.archive.org/about/faqs.php#49">https://www.archive.org/about/faqs.php#49</a>
<p><p>
<b>Are there restrictions on the use of the Prelinger Films?</b><p>
<a href="https://www.archive.org/about/faqs.php#197">https://www.archive.org/about/faqs.php#197</a>
<p><p>
<b>Can I search Archive.org by Creative Commons License?</b>
<p>
<a href="https://www.archive.org/about/faqs.php#263">https://www.archive.org/about/faqs.php#263</a>
<p>
<p>        </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#298">There's a problem with the item -- what next? </a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#299">How can I take my file off the site?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#296">How do I report that there's an issue with an item?</a> 
          </strong>
                  </p>
                <p>
          <strong>
            <a href="#297">How do I report that something's wrong with a book?</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="Report_Item"></a>
          Report Item        </h1>
                <p>
          <b>
            <a name="298"></a>
            There's a problem with the item -- what next?           </b>
                  </p>
        <p>
          <p>Some changes to our system, to individual items, or to collections can take a day to appear on Archive.org.  If you're experiencing a problem with an item, we recommend trying again after a day.  Often the issue will then have already been resolved.  

<br><br>        </p>
                <p>
          <b>
            <a name="299"></a>
            How can I take my file off the site?          </b>
                  </p>
        <p>
          <p>If you would like us to take down an item that you have uploaded, please send an email to info -at- archive.org

<p>Please note that you need to include the URL (web address) of the item. 

<p>Your email must come from the same email address you used to upload the item. This is the only way we can tell that you are the owner of the item.

<p>As always, if you write in, please be sure any spam filter you have is set to accept email from @archive.org.    

<p>Please see also the further resources at <a
href="https://www.archive.org/about/faqs.php#Uploading_Content">https://www.archive.org/about/faqs.php#Uploading_Content</a>

        </p>
                <p>
          <b>
            <a name="296"></a>
            How do I report that there's an issue with an item?          </b>
                  </p>
        <p>
          <p>The Internet Archive (Archive.org) is a nonprofit library that preserves digital cultural artifacts, and provides online access to over a million users a day with the goal of universal access to all knowledge.  

<p>To report an item which violates the Internet Archive's <a href="https://www.archive.org/about/terms.php">Terms of Use</a>, please send an email <b>with the URL (web address) of the item</b> to info -at- archive.org

<p><p>The Internet Archive follows the <a href="http://www2.sims.berkeley.edu/research/conferences/aps/removal-policy.html">Oakland Archive Policy</a> for Managing Removal Requests And Preserving Archival Integrity.  (When reviewing the Oakland Archive Policy, please note that information about requests coming from webmasters is information to assist with archived websites in particular.)  <br>For more information, see <a href="
https://www.archive.org/about/faqs.php#Rights">
https://www.archive.org/about/faqs.php#Rights</a>.  

<br><br>
        </p>
                <p>
          <b>
            <a name="297"></a>
            How do I report that something's wrong with a book?          </b>
                  </p>
        <p>
          <p>If you see an error with a book that the Internet Archive has digitized, we'd appreciate knowing about it!  

<p>Please send an email <b>with the URL (web address) of the book</b>, and description of the problem, to info -at- archive.org

<p>In some cases, you may know of <i>alternate</i> 
information about a book that is supplemental to the library bibliographic record.  (For example, a new, more modern transliteration of an author's name.)  

<p>To share additional information like this, you may wish to post it using the option to write a review of a book.  

<p>For more information on the Internet Archive's sponsored scanning, <br>please see <a href="https://www.archive.org/about/faqs.php#Texts_and_Books">https://www.archive.org/about/faqs.php#Texts_and_Books</a>

<p>For more information on books that users upload, or for information on how to upload your own, <br>please see <a href="https://www.archive.org/about/faqs.php#Texts_and_Books">https://www.archive.org/about/faqs.php#Texts_and_Books</a>

<br><br>        </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#293">What is Archive-It?</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="Archive-It"></a>
          Archive-It        </h1>
                <p>
          <b>
            <a name="293"></a>
            What is Archive-It?          </b>
                  </p>
        <p>
          <p><a href=http://www.archive-it.org><b>Archive-It</b></a> is a subscription service that allows institutions to build and preserve collections of born digital content. Through the user-friendly web application, <b>Archive-It partners can harvest, catalog, manage, and browse their archived collections.</b> Collections are hosted at the Internet Archive data center and are accessible to the public with full-text search.

<p><b>Why would I subscribe to Archive-It instead of using the Wayback
machine at Internet Archive?</b>
<p>Partners to this service can create distinct Web archives called
"collections", containing only the born digital content they are
interested in harvesting, at whatever frequency suits their needs. All
collections are full-text searchable. The collections created with
Archive-It can be cataloged with metadata and managed directly by the partner.
The Archive-It service maintains a minimum of two copies of each collection online, a primary and a back-up copy. 

<p><b>How frequently can I archive Web sites?</b>
<p>Archive-It is very flexible: you can harvest material from the Web
using ten different frequencies, from daily to annually.
Partners can select different crawl frequencies for each chosen
URL. Additionally, your institution can also chose to start a crawl
"on demand" in the case of an unforeseen spontaneous or historic event.

<p><b>Who gets access to the collections created in Archive-It?</b>
<p>By default, all collections are available for public access from the
main page at <a href=http://www.archive-It.org>www.archive-it.org</a>. However, a partner can choose to have their collection(s) made private by special arrangement.

<p><b>How can I search the collections?</b>
<p>Archive-It provides full text search capability for all public
collections. You can also browse by URL from the list provided for
each collection.  The public can browse and search collections by
partner type or collection from www.archive-it.org.

<p><b>What types of institutions can subscribe to Archive-It?</b>
<p>Archive-It is designed to fit the needs of many types of organizations. The 190+ partners include state archives and libraries, university libraries, federal institutions, non government non profits, museums, art libraries, and local city governments.

<p><b>Who decides which content to archive in Archive-It?</b>
<p>Partners develop their own collections and have complete control
over which content to archive within those collections.

<p><b>Where is the data stored for Archive-It collections?</b>
<p>All data created using the Archive-It service is hosted and stored by
the Internet Archive. We store two copies online and are working with
partners to have redundant copies in other locations.  Partners can also request a copy of their data for local use and preservation to be shipped either on a hard drive or over the internet. 
<br><br>        </p>
              </div>
    </td>
  </tr>
</table>
<table width="100%">
  <tr> 
    <td width="25%" valign="top">
      <div class="box">
        <h1>Questions</h1>
                <p>
          <strong>
            <a href="#291">What equipment does the Internet Archive use?  What APIs?</a> 
          </strong>
                  </p>
              </div>
    </td>
    <td width="75%" valign="top">
      <div class="box">
        <h1>
          <a name="Equipment"></a>
          Equipment        </h1>
                <p>
          <b>
            <a name="291"></a>
            What equipment does the Internet Archive use?  What APIs?          </b>
                  </p>
        <p>
          <p><b>Storage systems</b> used by the Internet Archive:<br>

<li>Large Scale Data Repository: Petabox <a href=http://www.petabox.org>http://www.petabox.org</a>

<li><a href=https://www.archive.org/iathreads/post-view.php?id=238517>Datacenter in a shipping container</a> -- Internet Archive launch with Sun


<p>Equipment and software used in the Internet Archive's scanning and OCR services for Contributing Libraries
<li><a href=
https://www.archive.org/about/faqs.php#Texts_and_Books>The Scribe system</a>

<p>Documents describing how to use Archive software and services, maintain "special" servers, and so on.  Includes our API to archive.org services using JSON format.  
<li> <a href=
https://www.archive.org/help>https://www.archive.org/help</a>

<br><br>
        </p>
              </div>
    </td>
  </tr>
</table>



<div style="width:90%; margin-left:auto; margin-right:auto;">
  <a name="forum"></a>
          <div class="box">
      <h1><span style="float:right;"><a href="/iathreads/post-new.php?forum=faqs"><span style="font-weight:bold;">New Post</span></a></span>FAQ Forum <a target="_blank" title="Subscribe to or unsubscribe from this forum" alt="Subscribe to or unsubscribe from this forum" href="/iathreads/forum-subscribe.php?forum=faqs"><img src="/images/mail.gif" style="border:0; vertical-align:bottom;"/></a> <a class="label label-success" alt="RSS feed of most recent posts to this forum" title="RSS feed of most recent posts to this forum" href="/iathreads/posts-display-new.php?forum=faqs&amp;mode=rss"><img src="/images/rss.png" style="border:0; vertical-align:bottom;"/></a></h1>
    
    <!-- NOTE: extra div required by IE bec. table width=100% is set -->
    <div style="text-align:center; width:100%">
      <table class="forumTable  table table-striped table-condensed table-hover" width="100%" border="0"
             cellpadding="5" cellspacing="0">

                <tr class="backColor1 forumRowHead">
                    <td>Subject</td>
                    <td>Poster</td>
                    <td>Replies</td>
                    <td>Date</td>
                  </tr>
                  <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1025508">Unable to download or read a borrowed book</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=durrick">durrick</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Nov 4, 2014 8:09am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1025515">Re: Unable to download or read a borrowed book</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=medcatdo">medcatdo</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Nov 4, 2014 8:42am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1025488">please remove it</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=TKAK">TKAK</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Nov 4, 2014 3:10am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1025485">please remove this item</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=phonocake.org">phonocake.org</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Nov 4, 2014 1:01am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1025465">Help me! </a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Oakley14">Oakley14</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Nov 3, 2014 4:26pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1025480">Re: Help me! </a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=warriorwolf2">warriorwolf2</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Nov 3, 2014 10:38pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1025239">Please remove this entry</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=dnimda">dnimda</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 31, 2014 3:56am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1025172">.ogg &amp; .png created automatically</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Radio%20Muslim">Radio Muslim</a>
        </td>
        
         
                <td>
          2        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 29, 2014 11:56pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1025181">Re: .ogg &amp; .png created automatically</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 30, 2014 6:52am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1025230">Re: .ogg &amp; .png created automatically</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Radio%20Muslim">Radio Muslim</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 30, 2014 11:22pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1025183">Re: .ogg &amp; .png created automatically</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=tracey%20pooh">tracey pooh</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 30, 2014 7:04am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1025231">Re: .ogg &amp; .png created automatically</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Radio%20Muslim">Radio Muslim</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 30, 2014 11:33pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1024713">Internet Archive "Save a Page" Plug-In 0.1</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=drstrange1">drstrange1</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 22, 2014 8:10am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1024712">Internet Archive "Save a Page" Plug-In 0.1</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=drstrange1">drstrange1</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 22, 2014 8:10am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1024711">Internet Archive "Save a Page" Plug-In 0.1</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=drstrange1">drstrange1</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 22, 2014 8:10am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1024693">Header on archived pages transparent in Firefox</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Dale%20Latimer">Dale Latimer</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 21, 2014 8:44pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1024538">CONSULTA </a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Colegio%20Universitario%20de%20Periodismo">Colegio Universitario de Periodismo</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 20, 2014 6:03am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1024550">Re: CONSULTA </a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 20, 2014 11:01am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1024537">CONSULTA </a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Colegio%20Universitario%20de%20Periodismo">Colegio Universitario de Periodismo</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 20, 2014 6:03am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1024470">please remove it</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=blackoops">blackoops</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 18, 2014 7:21pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1024256">Search Result Images</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=uncw_archives">uncw_archives</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 16, 2014 11:28am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1024343">Re: Search Result Images</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=tracey%20pooh">tracey pooh</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 17, 2014 8:14am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1024344">Re: Search Result Images</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=uncw_archives">uncw_archives</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 17, 2014 8:22am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1024462">Re: Search Result Images</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=tracey%20pooh">tracey pooh</a>
        </td>
        
         
                <td>
          2        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 18, 2014 3:36pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1024644">Re: Search Result Images</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=uncw_archives">uncw_archives</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 21, 2014 8:08am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1024649">Re: Search Result Images</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=uncw_archives">uncw_archives</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 21, 2014 8:24am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1024653">Re: Search Result Images</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 21, 2014 10:37am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1024666">Re: Search Result Images</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=uncw_archives">uncw_archives</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 21, 2014 10:00am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1024687">Re: Search Result Images</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 21, 2014 6:26pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1025185">Re: Search Result Images</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=uncw_archives">uncw_archives</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 30, 2014 6:59am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1024196">Moving Clips into their relevant Collection</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Beachfront%20Productions">Beachfront Productions</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 15, 2014 7:03pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1024202">Re: Moving Clips into their relevant Collection</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 15, 2014 8:16pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1024144">please remove</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=blackoops">blackoops</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 14, 2014 5:48pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1024143">please remove</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=blackoops">blackoops</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 14, 2014 5:25pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1024006">Archive.org not displaying correctly</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=jsonjamesjimenez">jsonjamesjimenez</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 11, 2014 7:32pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1024033">Re: Archive.org not displaying correctly</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 12, 2014 9:31am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1023897">download not working</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=michaelb2">michaelb2</a>
        </td>
        
         
                <td>
          2        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 9, 2014 6:30pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1023902">Re: download not working</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=msull">msull</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 9, 2014 8:37pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1024044">Re: download not working</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=theobalmuis">theobalmuis</a>
        </td>
        
         
                <td>
          2        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 12, 2014 11:23am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1024048">Re: download not working</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 12, 2014 12:11pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1024420">Re: download not working</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=P7even">P7even</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 17, 2014 10:34pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1024047">Re: download not working</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 12, 2014 12:11pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1024763">Re: download not working</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=sudyzsu">sudyzsu</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 23, 2014 4:11am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1024774">Re: download not working</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          2        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 23, 2014 7:07am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1024798">Re: download not working</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=esconset">esconset</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 23, 2014 12:21pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1024797">Re: download not working</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=esconset">esconset</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 23, 2014 12:21pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1023871">Delete item</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=mrkervic">mrkervic</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 9, 2014 8:03am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1023881">Re: Delete item</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 9, 2014 12:13pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1023657">please delete</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=johnnyb45">johnnyb45</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 4, 2014 9:00pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1023642">Thank You, Qahir Alirhab</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Harry%20Wilson">Harry Wilson</a>
        </td>
        
         
                <td>
          2        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 4, 2014 10:27am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1023655">Dank You, Qahair Airhed</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Ach%20Chew">Ach Chew</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 4, 2014 5:32pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1023753">Re: Dank You, Qahair Airhed</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          4        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 7, 2014 4:09am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1023756">Re: Dank You, Qahair Airhed</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=user001">user001</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 7, 2014 5:42am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1023808">Re: Dank You, Qahair Airhed</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 8, 2014 3:14am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1023761">Re: Dank You, Qahair Airhed</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Noah%208-%3F">Noah 8-?</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 7, 2014 5:44am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1023757">Re: Dank You, Qahair Airhed</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Noah%208-%3F">Noah 8-?</a>
        </td>
        
         
                <td>
          2        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 7, 2014 5:44am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1023759">Re: Dank You, Qahair Airhed</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=user001">user001</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 7, 2014 6:08am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1023809">Re: Dank You, Qahair Airhed</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 8, 2014 3:14am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1023780">Re: Dank You, Qahair Airhed</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Ach%20Chew">Ach Chew</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 7, 2014 1:56pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1023752">Re: Thank You, Qahir Alirhab</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 7, 2014 4:01am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1023640">Please Delete</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=SierraSaltwaterSystems">SierraSaltwaterSystems</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 4, 2014 10:14am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1023635">please deleate</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=sonvol">sonvol</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 4, 2014 4:16am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1023632">Check out my new site</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=TrungPhung">TrungPhung</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 3, 2014 6:32pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1023616">NEW NEW BEHEADING VIDEO OF UK PERSON - DELETE PLEASE</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          3        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 3, 2014 2:15pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1023620">Re: NEW NEW BEHEADING VIDEO OF UK PERSON - DELETE PLEASE</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 3, 2014 3:02pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1023627">Re: NEW NEW BEHEADING VIDEO OF UK PERSON - DELETE PLEASE</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 3, 2014 5:19pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1023630">Re: NEW NEW BEHEADING VIDEO OF UK PERSON - DELETE PLEASE</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 3, 2014 6:05pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1023631">Re: NEW NEW BEHEADING VIDEO OF UK PERSON - DELETE PLEASE</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 3, 2014 6:06pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1023623">Re: NEW NEW BEHEADING VIDEO OF UK PERSON - DELETE PLEASE</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 3, 2014 4:56pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1023626">Re: NEW NEW BEHEADING VIDEO OF UK PERSON - DELETE PLEASE</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 3, 2014 5:13pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1023599">DELETE PLEASE - EXECUTION VIDEOS AND 1 BEHEADING VIDEO</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 3, 2014 6:28am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1023594">URGENT DELETE-NEW BEHEADING VIDEOS AND MASS EXECUTION</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 3, 2014 3:53am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1023592">please delete</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=sonvol">sonvol</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 3, 2014 3:08am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1023613">Re: please delete</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 3, 2014 1:25pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1023549">eliminar archivos</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Colegio%20Universitario%20de%20Periodismo">Colegio Universitario de Periodismo</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 2, 2014 7:20am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1023419">PLEASE DELETE TERROR GROUP FILES</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 30, 2014 3:35am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1023361">Please delete</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=THKiddie">THKiddie</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 28, 2014 10:34am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1023364">Re: Please delete</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 28, 2014 11:43am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1023348">Please help to remove</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Rumahbet">Rumahbet</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 27, 2014 11:42pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1023332">displayed due to robots.txt</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=juanbue">juanbue</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 27, 2014 2:36pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1023368">Re: displayed due to robots.txt</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Dosarchiver">Dosarchiver</a>
        </td>
        
         
                <td>
          2        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 28, 2014 1:17pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1023373">Re: displayed due to robots.txt</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=juanbue">juanbue</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 28, 2014 5:29pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1023685">Re: displayed due to robots.txt</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Dosarchiver">Dosarchiver</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Oct 5, 2014 4:46pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1023372">Re: displayed due to robots.txt</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=juanbue">juanbue</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 28, 2014 5:29pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1023278">URGENT DELETE - BEHEADING VIDEO OF FRENSH MAN AND KURD MAN</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 26, 2014 4:01am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1023177">DELETE PLEASE - MORE VIDEOS FOR BEHEADING, HOSTAGES</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 24, 2014 6:03am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1023215">Re: DELETE PLEASE - MORE VIDEOS FOR BEHEADING, HOSTAGES</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 25, 2014 2:56am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1023176">DELETE PLEASE - MORE VIDEOS FOR BEHEADING, HOSTAGES</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 24, 2014 6:03am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1023152">Spellcasting SPAM</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=ScholarKris">ScholarKris</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 23, 2014 2:38pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1023094">Number of Listenings</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=newscarbaradio">newscarbaradio</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 22, 2014 10:36am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1022949">does my page removed??</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=pnumedical">pnumedical</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 18, 2014 11:42am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1022925">Item cannot be found</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Gera%C3%A7%C3%A3o%20148">Geração 148</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 18, 2014 6:45am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1022573">Spam</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Steven%20LaVey">Steven LaVey</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 11, 2014 6:59am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1022509">Problem printing paperback from PDF file</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=DogCatMouse">DogCatMouse</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 10, 2014 4:24am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1022300">Please remove BEHEADING VIDEOS of USA person</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          2        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 5, 2014 2:13am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1022612">Re: Please remove BEHEADING VIDEOS of USA person</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 12, 2014 2:44am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1022774">Re: Please remove BEHEADING VIDEOS of USA person</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 15, 2014 3:50am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1022811">Re: Please remove BEHEADING VIDEOS of USA person</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 16, 2014 3:51am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1022812">Re: Please remove BEHEADING VIDEOS of USA person</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          7        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 16, 2014 5:03am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1022815">Re: Please remove BEHEADING VIDEOS of USA person</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 16, 2014 5:33am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1022814">Re: Please remove BEHEADING VIDEOS of USA person</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 16, 2014 5:33am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1022813">Re: Please remove BEHEADING VIDEOS of USA person</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 16, 2014 5:33am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1022818">Re: Please remove BEHEADING VIDEOS of USA person</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 16, 2014 5:33am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1022819">Re: Please remove BEHEADING VIDEOS of USA person</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 16, 2014 5:33am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1022816">Re: Please remove BEHEADING VIDEOS of USA person</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 16, 2014 5:33am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1022817">Re: Please remove BEHEADING VIDEOS of USA person</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=qahir%20alirhab">qahir alirhab</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 16, 2014 5:33am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1023212">Re: Please remove BEHEADING VIDEOS of USA person</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=duhovnik">duhovnik</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 25, 2014 1:24am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1022220">Please delete this spam</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=The_Emperor_Of_Television">The_Emperor_Of_Television</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 2, 2014 10:53pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1022175">Please remove my copyright works</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jonathan%20Morris">Jonathan Morris</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 2, 2014 2:06am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1022218">Re: Please remove my copyright works</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 2, 2014 9:14pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1022171">please delete</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=jonathan%20elliott">jonathan elliott</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 1, 2014 11:23pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1022217">Re: please delete</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 2, 2014 9:13pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1022161">Please remove https://archive.org/details/Doctor_Who_-_The_Eight_058_-_History_101</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=MagsLHalliday">MagsLHalliday</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 1, 2014 4:40pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1022164">Re: Please remove https://archive.org/details/Doctor_Who_-_The_Eight_058_-_History_101</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 1, 2014 7:15pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1022114">Please remove 20140723NewWorldNextWeek199</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=media%20monarchy">media monarchy</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 31, 2014 10:51am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1022116">Re: Please remove 20140723NewWorldNextWeek199</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 31, 2014 12:11pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1022029">Deleting an archive entry - no response?</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Chaszy">Chaszy</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 28, 2014 7:14pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1021720">PDF blank</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=GSTLT">GSTLT</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 22, 2014 7:18pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1021754">Re: PDF blank</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 23, 2014 10:18am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1021759">Re: PDF blank</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=GSTLT">GSTLT</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 23, 2014 11:32am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1021790">Re: PDF blank</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=librivoxbooks">librivoxbooks</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 24, 2014 12:58am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1021564">!!!!! SPAM  SPAM  SPAM !!!!!!!</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Chicknhawk">Chicknhawk</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 19, 2014 8:18pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1021321">Very Slow Upload Speed</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Freemovie.cc">Freemovie.cc</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 15, 2014 5:16am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1022382">Re: Very Slow Upload Speed</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=rolandje">rolandje</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Sep 7, 2014 3:45am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1020995">No download button or link</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=ManYad">ManYad</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 10, 2014 4:26am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1021151">Re: No download button or link</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=librivoxbooks">librivoxbooks</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 12, 2014 7:14am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1020860">Some spammer uploaded some copyrighted TV show. Please delete</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=The_Emperor_Of_Television">The_Emperor_Of_Television</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 6, 2014 10:12pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1020852">SPAM SPAM SPAM - Date Rape Drugs</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Harry%20Wilson">Harry Wilson</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 6, 2014 4:25pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1020854">Re: SPAM SPAM SPAM - Date Rape Drugs</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Harry%20Wilson">Harry Wilson</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 6, 2014 4:48pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1020772">Delete</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Kyure6050">Kyure6050</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 5, 2014 3:14pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1020789">Re: Delete</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 5, 2014 7:55pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1020742">SPAM SPAM SPAM - Indonesian Abortion Drugs</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Harry%20Wilson">Harry Wilson</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 4, 2014 5:18pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1020718">Streetcar Named Desire Public Domain?</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=mzgaceque">mzgaceque</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 3, 2014 6:35pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1020663">Can you delete this?</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=pokemycookies">pokemycookies</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 2, 2014 2:44pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1020660">Here Today, Gone Tomorrow?</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=margotbean">margotbean</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 2, 2014 12:50pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1020642">Delete item please</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Historia%20Abierta">Historia Abierta</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 2, 2014 8:19am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1020644">Re: Delete item please</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 2, 2014 8:32am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;&nbsp;&nbsp;          <a href="/post/1020645">Re: Delete item please</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Historia%20Abierta">Historia Abierta</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 2, 2014 9:01am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1020577">Could you please move this from text to audio?</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=uair01%40xs4all.nl">uair01@xs4all.nl</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 1, 2014 12:58am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1020614">Re: Could you please move this from text to audio?</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Aug 1, 2014 2:59pm</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
                    <a href="/post/1020162">Delete 2 Entries - darn!</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Holy%20Moly">Holy Moly</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Jul 23, 2014 7:02am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="odd forumRow">
        <td>
                    <a href="/post/1020158">Delete 2 Entries - darn!</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Holy%20Moly">Holy Moly</a>
        </td>
        
         
                <td>
          1        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Jul 23, 2014 7:02am</nobr>
                  </td>
              </tr>
          <tr valign="top" class="eve forumRow">
        <td>
          &nbsp;&nbsp;          <a href="/post/1020177">Re: Delete 2 Entries - darn!</a>
                  </td>

                <td>
          <a href="/iathreads/forum-display.php?poster=Jeff%20Kaplan">Jeff Kaplan</a>
        </td>
        
         
                <td>
          0        </td>
        
        
                <td>
          <nobr class="hidden-sm hidden-xs">Jul 23, 2014 1:28pm</nobr>
                  </td>
              </tr>
          </table>
            <br/><b><a href="/iathreads/forum-display.php?forum=faqs&limit=100">View more forum posts</a></b>          </div>
        </div>
         
</div>



<div id="iafootdiv"><hr style="clear:both;"/></div>
<p id="iafoot">
  <a href="/about/terms.php">Terms of Use (10 Mar 2001)</a>
</p>

<!-- Timing ...
 rendered on: www14.us.archive.org
 seconds diff sec               message   stack(file:line:function)
=========================================================
  0.0000   0.0000         petabox start   usr/local/petabox/www/sf/about/faqs.php:1:require_once
                                             |setup.inc:26:require_once
                                             |www/common/setup.php:277:log
  0.0585   0.0585   begin session_start   usr/local/petabox/www/sf/about/faqs.php:5:head
                                             |common/Nav.inc:69:__construct
                                             |Nav.inc:183:session_start
                                             |Cookies.inc:61:log
  0.0587   0.0002    done session_start   usr/local/petabox/www/sf/about/faqs.php:5:head
                                             |common/Nav.inc:69:__construct
                                             |Nav.inc:183:session_start
                                             |Cookies.inc:67:log
  0.0679   0.0092      start db connect   usr/local/petabox/www/sf/about/faqs.php:10:query
                                             |common/DB.inc:144:query0
                                             |DB.inc:174:ping
                                             |DB.inc:832:connect
                                             |DB.inc:112:conn
                                             |DB.inc:962:log
  0.0820   0.0141                got db   usr/local/petabox/www/sf/about/faqs.php:10:query
                                             |common/DB.inc:144:query0
                                             |DB.inc:174:ping
                                             |DB.inc:832:connect
                                             |DB.inc:112:conn
                                             |DB.inc:1005:log
  0.1826   0.1006              bug dump   usr/local/petabox/www/sf/about/faqs.php:105:footer
                                             |common/setup.php:134:footer
                                             |Nav.inc:1633:dump
                                             |Bug.inc:109:log
-->
<script>if(window.archive_analytics) { window.archive_analytics.values['server_ms']=182; window.archive_analytics.values['server_name']="www14.us.archive.org"; window.archive_analytics.values['service']='ao'; } </script>
</body></html>
